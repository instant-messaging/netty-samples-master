# netty4-samples-master

## 介绍
Netty 是一个高性能、异步事件驱动的 NIO 框架，它提供了对 TCP、UDP 和文件传输的支持，作为一个异步 NIO 框架，Netty 的所有 IO 操作都是异步非阻塞的，通过 Future-Listener 机制，用户可以方便的主动获取或者通过通知机制获得 IO 操作结果。它是一个网路应用框架。

## 用户指南

[Netty权威指南 第2版](./docs/Netty权威指南%20第2版.pdf)

[Netty 4.x 用户指南](https://www.kancloud.cn/kancloud/netty-4-user-guide/62707)

[Netty 是什么？](https://my.oschina.net/u/3159571/blog/3061111)

[Netty 笔记--Netty 简介](https://my.oschina.net/RyenAng?q=netty)

[NIO、Netty（Netty基础）](https://my.oschina.net/u/3728166/blog/2986455)

[大数据、分布式都用到了的Netty，这几大核心知识你一定要看看！](https://my.oschina.net/u/4465956/blog/3217959?_from=gitee_rec)

### Channel生命周期
![](docs/images/9919411-8f71841c6d0182b4.webp)

### ChannelHandler
* `ChannelHandler`是`netty`中的核心处理部分，我们使用netty的绝大部分代码都写在这部分，
* `ChannelHandler`用于处理`Channel`对应的事件
* `ChannelHandler`接口里面只定义了三个生命周期方法，我们主要实现它的子接口 `ChannelInboundHandler` 和 `ChannelOutboundHandler`.为了便利，框架提供了`ChannelInboundHandlerAdapter`，`ChannelOutboundHandlerAdapter`和`ChannelDuplexHandler`这三个适配类，在使用的时候只需要实现你关注的方法即可

### ChannelHandler生命周期方法

![](docs/images/9919411-46dac29c85f4967c.webp)

`ChannelHandler`里面定义三个生命周期方法，分别会在当前`ChannelHander`加入`ChannelHandlerContext`中，从`ChannelHandlerContext`中移除，以及`ChannelHandler`回调方法出现异常时被回调

### ChannelInboundHandler

![](docs/images/9919411-5302e2284821e3b2.webp)

**介绍一下这些回调方法被触发的时机**

回调方法                    | 触发时机                                            | client | server
------------------------- | -------------------------------------------------- | ------ | ---
channelRegistered         | 当前channel注册到EventLoop                           | true   | true
channelUnregistered       | 当前channel从EventLoop取消注册                        | true   | true
channelActive             | 当前channel激活的时候                                 | true   | true
channelInactive           | 当前channel不活跃的时候,也就是当前channel到了它生命周期末  | true   | true
channelRead               | 当前channel从远端读取到数据                            | true   | true
channelReadComplete       | channel read消费完读取的数据的时候被触发                 | true   | true
userEventTriggered        | 用户事件触发的时候                                      
channelWritabilityChanged | channel的写状态变化的时候触发

可以注意到每个方法都带了ChannelHandlerContext作为参数，具体作用是，在每个回调事件里面，处理完成之后，使用ChannelHandlerContext的fireChannelXXX方法来传递给下个ChannelHandler，netty的codec模块和业务处理代码分离就用到了这个链路处理

### ChannelOutboundHandler

![](docs/images/9919411-d669eef52e83514b.webp)

回调方法     | 触发时机                | client | server
---------- | ---------------------- | ----- | --- 
bind       | bind操作执行前触发        | false | true
connect    | connect 操作执行前触发    | true  | false
disconnect | disconnect 操作执行前触发 | true  | false
close      | close操作执行前触发       | false | true
deregister | deregister操作执行前触发
read       | read操作执行前触发        | true  | true
write      | write操作执行前触发       | true  | true
flush      | flush操作执行前触发       | true  | true

注意到一些回调方法有ChannelPromise这个参数，我们可以调用它的addListener注册监听，当回调方法所对应的操作完成后，会触发这个监听

下面这个代码，会在写操作完成后触发，完成操作包括成功和失败
```java_holder_method_tree
public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
    ctx.write(msg,promise);
    System.out.println("out write");
    promise.addListener(new GenericFutureListener<Future<? super Void>>() {
        @Override
        public void operationComplete(Future<? super Void> future) throws Exception {
            if(future.isSuccess()){
                System.out.println("OK");
            }
        }
    });
}
```

#### ChannelInboundHandler和ChannelOutboundHandler的区别

个人感觉in和out的区别主要在于ChannelInboundHandler的channelRead和channelReadComplete回调和ChannelOutboundHandler的write和flush回调上，ChannelOutboundHandler的channelRead回调负责执行入栈数据的decode逻辑，ChannelOutboundHandler的write负责执行出站数据的encode工作。其他回调方法和具体触发逻辑有关，和in与out无关。
package com.bxs.netty.persistence.annotation;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 16:59
 */
@ConditionalOnProperty(prefix = "database",value = "type",havingValue = "sql")
public @interface SqlDao {
}

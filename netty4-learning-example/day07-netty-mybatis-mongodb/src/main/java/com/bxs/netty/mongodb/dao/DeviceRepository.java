package com.bxs.netty.mongodb.dao;

import com.bxs.netty.mongodb.entity.Device;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:41
 */
public interface DeviceRepository extends MongoRepository<Device, Long> {
}

package com.bxs.day06.secureecho;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;

/**
 * SslContext Factory.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 11:52
 */
public class SslContextFactory {
    private static final String PROTOCOL = "TLS";
    /**
     * 服务器上下文
     */
    private static SSLContext SERVER_CONTEXT;
    /**
     * 客户端上下文
     */
    private static SSLContext CLIENT_CONTEXT;

    public static SSLContext getServerContext(String pkPath, String caPath, String password) {
        if (SERVER_CONTEXT != null) {
            return SERVER_CONTEXT;
        }

        SERVER_CONTEXT = getContext(pkPath, caPath, password);

        return SERVER_CONTEXT;
    }

    public static SSLContext getClientContextgetContext(String pkPath, String caPath, String password) {
        if (CLIENT_CONTEXT != null) {
            return CLIENT_CONTEXT;
        }

        CLIENT_CONTEXT = getContext(pkPath, caPath, password);

        return CLIENT_CONTEXT;
    }

    public static SSLContext getContext(String pkPath, String caPath, String password) {
        if (CLIENT_CONTEXT != null) {
            return CLIENT_CONTEXT;
        }

        InputStream in = null;
        InputStream tIN = null;

        try {
            KeyManagerFactory kmf = null;
            if (pkPath != null) {
                KeyStore store = KeyStore.getInstance("JKS");
                in = new FileInputStream(pkPath);
                store.load(in, password.toCharArray());
                kmf = KeyManagerFactory.getInstance("SunX509");
                kmf.init(store, password.toCharArray());
            }

            TrustManagerFactory tf = null;
            if (caPath != null) {
                KeyStore tks = KeyStore.getInstance("JKS");
                tIN = new FileInputStream(caPath);
                tks.load(tIN, password.toCharArray());
                tf = TrustManagerFactory.getInstance("SunX509");
                tf.init(tks);
            }

            CLIENT_CONTEXT = SSLContext.getInstance(PROTOCOL);
            CLIENT_CONTEXT.init(kmf.getKeyManagers(), tf.getTrustManagers(), null);

        } catch (Exception e) {
            throw new Error("Failed to initialize the client-side SSLContext");
        } finally {
            try {
                in.close();
                tIN.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return CLIENT_CONTEXT;
    }

}

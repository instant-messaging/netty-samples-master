package com.bxs.day05.jackcon;

import com.bxs.day05.jackcon.codec.JacksonMapper;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * JacksonServer Handler
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 11:30
 */
public class JacksonServerHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object obj) throws Exception {
        String jsonString = "";
        if (obj instanceof JacksonBean) {
            JacksonBean user = (JacksonBean) obj;
            ctx.writeAndFlush(user);
            // 对象转为json字符串
            jsonString = JacksonMapper.getInstance().writeValueAsString(user);
            System.out.println("Client -> Server: " + jsonString);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        Channel incoming = ctx.channel();
        System.out.println("SimpleChatClient:" + incoming.remoteAddress() + "异常");
        // 当出现异常就关闭连接
        cause.printStackTrace();
        ctx.close();
    }
}

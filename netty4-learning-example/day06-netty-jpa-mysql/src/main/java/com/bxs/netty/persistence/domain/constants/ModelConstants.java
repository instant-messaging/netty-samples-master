package com.bxs.netty.persistence.domain.constants;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:02
 */
public class ModelConstants {

    public static final String ID_PROPERTY = "id";
    public static final String DEVICE_NAME_PROPERTY = "name";
    public static final String DEVICE_TEMPERATURE_PROPERTY = "temperature";
    public static final String DEVICE_TIME_PROPERTY = "time";
    public static final String DEVICE_HUMIDITY_PROPERTY = "humidity";

    public static final String DEVICE_COLUMN_FAMILY_NAME = "device";
}

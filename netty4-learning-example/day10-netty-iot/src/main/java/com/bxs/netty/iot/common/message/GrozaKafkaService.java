package com.bxs.netty.iot.common.message;

import com.bxs.netty.iot.internal.InternalMessage;

/**
 * 消息转发,基于kafka
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/8 11:18
 */
public interface GrozaKafkaService {
    void send(InternalMessage internalMessage);
}

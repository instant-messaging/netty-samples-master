package com.bxs.chat.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 用户聊天关系实体层，包括用户与还有这件关系，与群之间关系
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 12:37
 */
@Entity
@Table(name = "user_chat_relation")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserChatRelation {

    /**
     * 主键自增id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    /**
     * 个人id
     */
    @Setter
    @Getter
    @Column(name = "user_id", columnDefinition = "bigint(19) DEFAULT NULL COMMENT '个人id'")
    private Long userId;

    /**
     * 好友id
     */
    @Setter
    @Getter
    @Column(name = "friends_id", columnDefinition = "bigint(19) DEFAULT NULL COMMENT '好友id'")
    private Long friendsId;


    /**
     * 群组id
     */
    @Setter
    @Getter
    @Column(name = "group_id", columnDefinition = "bigint(19) DEFAULT NULL COMMENT '群组id'")
    private Long groupId;

    /**
     * 群组或者好友备注
     */
    @Setter
    @Getter
    @Column(name = "remarks", columnDefinition = "bigint(19) DEFAULT NULL COMMENT '群组或者好友备注'")
    private String remarks;

    /**
     * 创建时间
     */
    @Setter
    @Getter
    @Column(name = "create_date", columnDefinition = "datetime DEFAULT NULL COMMENT '创建时间'")
    private LocalDateTime createDate;

    /**
     * 修改时间
     */
    @Setter
    @Getter
    @Column(name = "last_modify_date", columnDefinition = "datetime DEFAULT NULL COMMENT '修改时间'")
    private LocalDateTime lastModifyDate;

    /**
     * 状态
     */
    @Setter
    @Getter
    @Column(name = "state", columnDefinition = "int(4) DEFAULT NULL COMMENT '删除标志位'")
    private Integer state;
}

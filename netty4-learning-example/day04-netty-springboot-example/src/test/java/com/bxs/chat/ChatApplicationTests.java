package com.bxs.chat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import lombok.Data;
import lombok.experimental.Accessors;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

//@SpringBootTest
public class ChatApplicationTests {

//    @Test
//    void contextLoads() {
//
//    }

    public static void main(String[] args) {
        Item item = new Item().setItem("柜台").setDescription("还是柜台").setImageUrl("upload/pic/case/20200902090149718-4833.png,upload/pic/case/20200902090154608-9763.png");
        String jsonString = JSON.toJSONString(item);
        System.out.println("JSON 字符串:" + jsonString);

        List<Item> items = new ArrayList();
        for (int i = 0; i < 10; i++) {
            items.add(new Item().setItem("柜台" + i).setDescription("还是柜台" + i).setImageUrl("upload/pic/case/20200902090149718-4833.png,upload/pic/case/20200902090154608-9763.png"));
        }
        String s = JSON.toJSONString(items);
        System.out.println("JSON 字符串:" + s);

        List<Item> array = JSON.parseArray(s, Item.class);
        System.out.println("JSON 字符串:" + array.toString());

        List<Item> itemss = JSONArray.parseArray(s, Item.class);
        System.out.println("JSON 字符串:" + array.toString());
    }

    @Data
    @Accessors(chain = true)
    static class Item {
        private String item;
        private String description;
        private String imageUrl;
    }

}

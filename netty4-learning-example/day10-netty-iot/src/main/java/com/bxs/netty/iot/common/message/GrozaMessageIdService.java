package com.bxs.netty.iot.common.message;

/**
 * 分布式生成报文标识符
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/8 11:36
 */
public interface GrozaMessageIdService {
    /**
     * 获取报文标识符
     */
    int getNextMessageId();
}

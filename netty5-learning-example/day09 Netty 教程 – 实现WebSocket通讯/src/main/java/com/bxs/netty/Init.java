package com.bxs.netty;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/25 14:58
 */
public interface Init {
    int PORT = 5050;
    String HOST = "localhost";
    String WEB_SOCKET_URL = String.format("ws://%s:%d/websocket", HOST, PORT);
}

package com.bxs.netty.kafka.service;

import java.util.Map;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 14:59
 */
public interface DemoService {

    Map<String, Object> run();
}

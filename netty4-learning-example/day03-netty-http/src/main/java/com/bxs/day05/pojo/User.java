package com.bxs.day05.pojo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 11:17
 */
@Data
public class User {
    private String userName;

    private String method;

    private LocalDateTime date;
}

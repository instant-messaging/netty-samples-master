package com.bxs.netty.kafka.kafka;

import com.bxs.netty.kafka.proto.Callback;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 15:08
 */
@Slf4j
@Component
public class Sender {

    @Autowired
    private KafkaTemplate<Long, Callback> kafkaTemplate;

    public void send(Callback request) {
        kafkaTemplate.send("spring-kafka-protobuf-demo", request);
    }
}

package com.bxs.netty.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:23
 */
public class ClubbedTroll implements Troll {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClubbedTroll.class);

    private Troll decorated;

    public ClubbedTroll(Troll decorated) {
        this.decorated = decorated;
    }

    @Override
    public void attack() {
        decorated.attack();
        LOGGER.info("The troll swings at you with a club!");
    }

    @Override
    public int getAttackPower() {
        return decorated.getAttackPower() + 10;
    }

    @Override
    public void fleeBattle() {
        decorated.fleeBattle();
    }
}

package com.bxs.netty.test;

import java.io.Serializable;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/25 8:36
 */
public class Order implements Serializable {

    private Integer orderId;
    private String userName;
    private String productName;
    private String phoneNumber;
    private String address;

    public Order(Integer orderId, String userName, String productName, String phoneNumber, String address) {
        this.orderId = orderId;
        this.userName = userName;
        this.productName = productName;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public Order setOrderId(Integer orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public Order setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public Order setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Order setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Order setAddress(String address) {
        this.address = address;
        return this;
    }
}

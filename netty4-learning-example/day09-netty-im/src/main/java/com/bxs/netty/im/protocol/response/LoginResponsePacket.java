package com.bxs.netty.im.protocol.response;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;

import static com.bxs.netty.im.protocol.command.Command.LOGIN_RESPONSE;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 9:01
 */
@Data
public class LoginResponsePacket extends Packet {
    private String userId;

    private String userName;

    private boolean success;

    private String reason;


    @Override
    public Byte getCommand() {
        return LOGIN_RESPONSE;
    }
}

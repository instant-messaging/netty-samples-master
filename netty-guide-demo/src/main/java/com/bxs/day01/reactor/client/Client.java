package com.bxs.day01.reactor.client;

/**
 * Client Demo
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 15:26
 */
public class Client {

    public static void main(String[] args) {
        new Thread(new NIOClient("127.0.0.1", 2333)).start();
        new Thread(new NIOClient("127.0.0.1", 2333)).start();
    }
}

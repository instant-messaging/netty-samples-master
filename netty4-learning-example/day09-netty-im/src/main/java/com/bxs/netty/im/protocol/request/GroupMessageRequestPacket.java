package com.bxs.netty.im.protocol.request;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.bxs.netty.im.protocol.command.Command.GROUP_MESSAGE_REQUEST;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:53
 */
@Data
@NoArgsConstructor
public class GroupMessageRequestPacket extends Packet {
    private String toGroupId;
    private String message;

    public GroupMessageRequestPacket(String toGroupId, String message) {
        this.toGroupId = toGroupId;
        this.message = message;
    }

    @Override
    public Byte getCommand() {
        return GROUP_MESSAGE_REQUEST;
    }
}

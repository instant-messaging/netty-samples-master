package com.bxs.netty.iot.store.kafka;

import com.bxs.netty.iot.common.message.GrozaKafkaService;
import com.bxs.netty.iot.internal.InternalMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * kafka消息生产者处理类
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/8 13:49
 */
public class KafkaServiceImpl implements GrozaKafkaService {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    private static Gson gson = new GsonBuilder().create();

    @Override
    public void send(InternalMessage internalMessage) {
        kafkaTemplate.send(internalMessage.getTopic(), gson.toJson(internalMessage));
    }
}

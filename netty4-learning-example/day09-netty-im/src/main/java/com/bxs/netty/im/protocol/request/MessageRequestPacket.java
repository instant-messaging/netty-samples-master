package com.bxs.netty.im.protocol.request;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.bxs.netty.im.protocol.command.Command.MESSAGE_REQUEST;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:51
 */
@Data
@NoArgsConstructor
public class MessageRequestPacket extends Packet {
    private String toUserId;
    private String message;

    public MessageRequestPacket(String toUserId, String message) {
        this.toUserId = toUserId;
        this.message = message;
    }

    @Override
    public Byte getCommand() {
        return MESSAGE_REQUEST;
    }
}

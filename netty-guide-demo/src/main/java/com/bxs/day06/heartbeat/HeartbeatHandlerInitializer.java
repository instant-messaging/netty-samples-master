package com.bxs.day06.heartbeat;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * Heartbeat Handler Initializer
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 10:43
 */
public class HeartbeatHandlerInitializer extends ChannelInitializer<Channel> {

    /**
     * 读超时
     */
    private static final int READ_IDEL_TIME_OUT = 4;
    /**
     * 写超时
     */
    private static final int WRITE_IDEL_TIME_OUT = 5;
    /**
     * 所有超时
     */
    private static final int ALL_IDEL_TIME_OUT = 7;

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        // （1）
        pipeline.addLast(new IdleStateHandler(READ_IDEL_TIME_OUT, WRITE_IDEL_TIME_OUT, ALL_IDEL_TIME_OUT, TimeUnit.SECONDS));
        // （2）
        pipeline.addLast(new HeartbeatServerHandler());
    }
}

package com.bxs.netty.singleton.type;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:05
 */
public class ThreadSafeDoubleCheckLocking {

    private volatile static ThreadSafeDoubleCheckLocking uniqueInstance;

    private ThreadSafeDoubleCheckLocking() {
    }

    public static ThreadSafeDoubleCheckLocking getInstance() {
        if (uniqueInstance == null) {
            synchronized (ThreadSafeDoubleCheckLocking.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new ThreadSafeDoubleCheckLocking();
                }
            }
        }
        return uniqueInstance;
    }
}

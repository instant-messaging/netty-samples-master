package com.bxs.chat.common.constant;

/**
 * 常用符号
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/17 8:49
 */
public final class SymbolConstants {
    /** 分号 */
    public static final String SEMICOLON=";";
    /** 分号 */
    public static final String SEMICOLON_REG=";|;";
    /** 冒号 */
    public static final String COLON=":";
    /** 冒号 */
    public static final String COLON_REG=":|:";
    public static final String COMMA=",";
    public static final String COMMA_REG=",|,";
    public static final String XIEGANG_REG="/";
    public static final String SHUXIAN_REG="|";
    public static final String UNDERLINE_REG="_";
    public static final String JINGHAO_REG="#";
    public static final String AT_REG="@";

    private SymbolConstants() {
    }
}

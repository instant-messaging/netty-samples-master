package com.bxs.chat.domain.enums;

/**
 * 添加好友前置状态 枚举
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/18 10:15
 */
public enum SearchFriendsStatusEnum {

    /**
     * 前置条件查询：通过
     */
    SUCCESS(0, "OK"),

    /**
     * 前置条件查询：无此用户
     */
    USER_NOT_EXIST(1, "无此用户..."),

    /**
     * 前置条件查询：不能添加你自己
     */
    NOT_YOURSELF(2, "不能添加你自己..."),

    /**
     * 前置条件查询：该用户已经是你的好友
     */
    ALREADY_FRIENDS(3, "该用户已经是你的好友...");

    /**
     * 状态
     */
    public final Integer status;

    /**
     * 提示信息
     */
    public final String message;

    SearchFriendsStatusEnum(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public static String getMsgByKey(Integer status) {
        for (SearchFriendsStatusEnum type : SearchFriendsStatusEnum.values()) {
            if (type.getStatus().equals(status)) {
                return type.message;
            }
        }
        return null;
    }
}

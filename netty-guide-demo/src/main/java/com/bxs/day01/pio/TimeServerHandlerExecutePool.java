package com.bxs.day01.pio;

import java.util.concurrent.*;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 9:03
 */
public class TimeServerHandlerExecutePool {

    /**
     * 初始化线程池执行器
     */
    private ExecutorService executorService;

    public TimeServerHandlerExecutePool(int maxPoolSize, int queueSize) {
        // 队列
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(queueSize);

        // 初始化线程池执行器
        ExecutorService executorService = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(), maxPoolSize, 4000, TimeUnit.MILLISECONDS, workQueue);
        this.executorService = executorService;
    }

    public void execute(java.lang.Runnable task) {
        executorService.execute(task);
    }
}

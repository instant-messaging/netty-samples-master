package com.bxs.netty.im.protocol.response;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;

import java.util.List;

import static com.bxs.netty.im.protocol.command.Command.CREATE_GROUP_RESPONSE;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:57
 */
@Data
public class CreateGroupResponsePacket extends Packet {
    private boolean success;

    private String groupId;

    private List<String> usernameList;

    @Override
    public Byte getCommand() {
        return CREATE_GROUP_RESPONSE;
    }
}

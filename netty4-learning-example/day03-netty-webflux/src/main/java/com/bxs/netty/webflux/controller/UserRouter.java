package com.bxs.netty.webflux.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 13:36
 */
@Configuration
public class UserRouter {
    RouterFunction<ServerResponse> getAllEmployeesRoute() {
        return route(GET("/getAllEmployees"), request -> ok().body(fromObject("Home page")));
    }
}

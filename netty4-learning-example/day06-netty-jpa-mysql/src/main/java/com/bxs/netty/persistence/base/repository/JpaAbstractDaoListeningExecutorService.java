package com.bxs.netty.persistence.base.repository;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import javax.annotation.PreDestroy;
import java.util.concurrent.Executors;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:08
 */
public abstract class JpaAbstractDaoListeningExecutorService {

    protected ListeningExecutorService service = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));

    @PreDestroy
    void onDestroy() {
        service.shutdown();
    }
}

package com.bxs.chat.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/16 15:00
 */
@Documented
@Retention(RUNTIME)
@Target({TYPE, METHOD})
public @interface ResponseResult {
}

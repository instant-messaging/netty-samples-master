package com.bxs.day05.jackcon.codec;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Jackson Encoder.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 11:23
 */
public class JacksonEncoder extends MessageToByteEncoder<Object> {

    @Override
    protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out) throws Exception {
        ObjectMapper mapper = JacksonMapper.getInstance();
        // 将对象转换为byte
        byte[] body = mapper.writeValueAsBytes(msg);
        // 消息体中包含我们要发送的数据
        out.writeBytes(body);
    }
}

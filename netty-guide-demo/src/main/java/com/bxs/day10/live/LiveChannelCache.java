package com.bxs.day10.live;

import io.netty.channel.Channel;
import io.netty.util.concurrent.ScheduledFuture;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/10 11:38
 */
public class LiveChannelCache {

    private Channel channel;
    private ScheduledFuture scheduledFuture;

    public LiveChannelCache(Channel channel, ScheduledFuture scheduledFuture) {
        this.channel = channel;
        this.scheduledFuture = scheduledFuture;
    }

    public Channel getChannel() {
        return channel;
    }

    public LiveChannelCache setChannel(Channel channel) {
        this.channel = channel;
        return this;
    }

    public ScheduledFuture getScheduledFuture() {
        return scheduledFuture;
    }

    public LiveChannelCache setScheduledFuture(ScheduledFuture scheduledFuture) {
        this.scheduledFuture = scheduledFuture;
        return this;
    }
}

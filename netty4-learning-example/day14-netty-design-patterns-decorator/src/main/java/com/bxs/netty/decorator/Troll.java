package com.bxs.netty.decorator;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:18
 */
public interface Troll {

    void attack();

    int getAttackPower();

    void fleeBattle();
}

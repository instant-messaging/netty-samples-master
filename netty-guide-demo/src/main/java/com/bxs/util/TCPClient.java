package com.bxs.util;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * TCP Client.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 10:56
 */
public class TCPClient {

    public static void main(String[] args) throws IOException {
        Socket socket = null;
        OutputStream out = null;

        try {

            socket = new Socket("localhost", 18080);
            out = socket.getOutputStream();

            // 请求服务器
            String lines = "床前明月光\r\n疑是地上霜\r\n举头望明月\r\n低头思故乡\r\n";
            byte[] outputBytes = lines.getBytes("UTF-8");
            out.write(outputBytes);
            out.flush();

        } finally {
            // 关闭连接
            out.close();
            socket.close();
        }
    }
}

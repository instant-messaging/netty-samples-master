package com.bxs.netty.springboot.websocket;

import com.bxs.netty.springboot.websocket.handler.IdleStateHandlerInitializer;
import com.bxs.netty.springboot.websocket.handler.WebSocketChatHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

/**
 * NettyServer Netty服务器配置
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/25 14:29
 */
public class WebSocketServer {

    private static Logger logger = LoggerFactory.getLogger(WebSocketServer.class);

    private final int port;
    private EventLoopGroup bossGroup = new NioEventLoopGroup();
    private EventLoopGroup group = new NioEventLoopGroup();
    private ChannelFuture future;

    public WebSocketServer(int port) {
        this.port = port;
    }

    public void start() throws Exception {
        bossGroup = new NioEventLoopGroup();
        group = new NioEventLoopGroup();
        ServerBootstrap bootstrap = new ServerBootstrap();
        // 绑定线程池
        bootstrap.group(group, bossGroup)
                // 指定使用的channel
                .channel(NioServerSocketChannel.class)

                // 绑定监听端口, 使用指定的端口设置套接字地址
                .localAddress(new InetSocketAddress(port))

                // 服务端可连接队列数,对应TCP/IP协议listen函数中backlog参数
                .option(ChannelOption.SO_BACKLOG, 1024)

                //设置TCP长连接,一般如果两个小时内没有数据的通信时,TCP会自动发送一个活动探测数据报文
                .childOption(ChannelOption.SO_KEEPALIVE, true)

                //将小的数据包包装成更大的帧进行传送，提高网络的负载,即TCP延迟传输
                .childOption(ChannelOption.TCP_NODELAY, true)

                // 绑定客户端连接时候触发操作
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        logger.info("收到新的客户端连接: {}", ch.toString());
                        // websocket协议本身是基于http协议的，所以这边也要使用http解编码器
                        ch.pipeline().addLast(new HttpServerCodec());
                        // 心跳检测,参数说明：[长时间未写:长时间未读:长时间未读写:时间单位]~[读写是对连接本生而言，写：未向服务端发送消息，读：未收到服务端的消息]
                        ch.pipeline().addLast(new IdleStateHandler(0, 5 * 60, 0, TimeUnit.SECONDS));
                        ch.pipeline().addLast(new IdleStateHandlerInitializer());
                        // 以块的方式来写的处理器
                        ch.pipeline().addLast(new ChunkedWriteHandler());
                        ch.pipeline().addLast(new HttpObjectAggregator(8192));
                        ch.pipeline().addLast(new WebSocketChatHandler());
                        // 最后一个参数为数据包大小
                        ch.pipeline().addLast(new WebSocketServerProtocolHandler("/ws", null, true, 65536 * 10));
                    }
                });

        // 服务器异步创建绑定
        future = bootstrap.bind().sync();
        logger.info(WebSocketServer.class + " 启动正在监听： " + future.channel().localAddress());
    }

    public void destroy() {
        logger.info(WebSocketServer.class + " netty服务监听关闭： " + future.channel().localAddress());
        try {
            // 关闭服务器通道
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully().syncUninterruptibly();
            group.shutdownGracefully().syncUninterruptibly();
        }
    }
}

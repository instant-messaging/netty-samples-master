package com.bxs.netty.im.client.handler;

import com.bxs.netty.im.protocol.response.MessageResponsePacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 10:08
 */
public class MessageResponseHandler extends SimpleChannelInboundHandler<MessageResponsePacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageResponsePacket responsePacket) {
        String fromUserId = responsePacket.getFromUserId();
        String fromUserName = responsePacket.getFromUserName();
        System.out.println(fromUserId + ":" + fromUserName + " -> " + responsePacket
                .getMessage());
    }
}

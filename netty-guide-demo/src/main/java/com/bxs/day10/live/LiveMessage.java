package com.bxs.day10.live;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/10 11:37
 */
public class LiveMessage {

    static final byte TYPE_HEART = 1;
    static final byte TYPE_MESSAGE = 2;

    private byte type;
    private int length;
    private String content;

    public LiveMessage() {}

    public byte getType() {
        return type;
    }

    public LiveMessage setType(byte type) {
        this.type = type;
        return this;
    }

    public int getLength() {
        return length;
    }

    public LiveMessage setLength(int length) {
        this.length = length;
        return this;
    }

    public String getContent() {
        return content;
    }

    public LiveMessage setContent(String content) {
        this.content = content;
        return this;
    }

    @Override
    public String toString() {
        return "LiveMessage{" +
                "type=" + type +
                ", length=" + length +
                ", content='" + content + '\'' +
                '}';
    }
}

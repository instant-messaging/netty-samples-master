package com.bxs.chat.common.exception;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/17 9:59
 */
public class TransactionException extends RuntimeException {

    private static final long serialVersionUID = -7019206205634720119L;

    public TransactionException(){
        super();
    }

    public TransactionException(String message){
        super(message);
    }
}

package com.bxs.netty.im.protocol.response;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;

import static com.bxs.netty.im.protocol.command.Command.JOIN_GROUP_RESPONSE;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:59
 */
@Data
public class JoinGroupResponsePacket extends Packet {
    private String groupId;

    private boolean success;

    private String reason;

    @Override
    public Byte getCommand() {
        return JOIN_GROUP_RESPONSE;
    }
}

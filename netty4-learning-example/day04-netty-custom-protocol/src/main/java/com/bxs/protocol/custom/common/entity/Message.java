package com.bxs.protocol.custom.common.entity;

import lombok.Builder;

import java.io.Serializable;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 10:43
 */
@Builder
public class Message implements Serializable {

    private Header header;
    private Body body;

    public Message() {
    }

    public Message(Header header, Body body) {
        this.header = header;
        this.body = body;
    }

    public Header getHeader() {
        return header;
    }

    public Message setHeader(Header header) {
        this.header = header;
        return this;
    }

    public Body getBody() {
        return body;
    }

    public Message setBody(Body body) {
        this.body = body;
        return this;
    }

    @Override
    public String toString() {
        return "Message{" +
                "header=" + header +
                ", body=" + body +
                '}';
    }
}

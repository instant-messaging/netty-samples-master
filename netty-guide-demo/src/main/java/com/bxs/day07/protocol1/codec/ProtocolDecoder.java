package com.bxs.day07.protocol1.codec;

import com.bxs.day07.protocol1.common.ProtocolHeader;
import com.bxs.day07.protocol1.common.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/5 18:02
 */
public class ProtocolDecoder extends LengthFieldBasedFrameDecoder {
    private static final int HEADER_SIZE = 10;

    /**
     * 魔数
     */
    private byte magic;
    /**
     * 消息类型
     */
    private byte msgType;
    /**
     * 保留字
     */
    private short reserve;
    /**
     * 序列号
     */
    private short sn;
    /**
     * 长度
     */
    private int len;

    /**
     * @param maxFrameLength
     * @param lengthFieldOffset
     * @param lengthFieldLength
     */
    public ProtocolDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip);
    }

    /**
     * @param maxFrameLength
     * @param lengthFieldOffset
     * @param lengthFieldLength
     * @param lengthAdjustment
     * @param initialBytesToStrip
     * @param failFast
     */
    public ProtocolDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip, boolean failFast) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip, failFast);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf in2) throws Exception {
        ByteBuf in = (ByteBuf) super.decode(ctx, in2);
        if (in == null) {
            return null;
        }

        if (in.readableBytes() < HEADER_SIZE) {
            // response header is 10 bytes
            return null;
        }

        magic = in.readByte();
        msgType = in.readByte();
        reserve = in.readShort();
        sn = in.readShort();
        len = in.readInt();

        if (in.readableBytes() < len) {
            // until we have the entire payload return
            return null;
        }

        ByteBuf buf = in.readBytes(len);
        byte[] req = new byte[buf.readableBytes()];
        buf.readBytes(req);

        ProtocolMessage message = new ProtocolMessage();

        ProtocolHeader protocolHeader = new ProtocolHeader(magic, msgType, reserve, sn, len);
        String body = new String(req, "UTF-8");

        message.setProtocolHeader(protocolHeader);
        message.setBody(body);

        return message;
    }
}

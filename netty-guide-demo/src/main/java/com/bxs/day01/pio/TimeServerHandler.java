package com.bxs.day01.pio;

import java.util.concurrent.TimeUnit;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 9:18
 */
public class TimeServerHandler implements Runnable {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " is working");

        try {
            // 模拟一段耗时工作
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + " done");
    }
}

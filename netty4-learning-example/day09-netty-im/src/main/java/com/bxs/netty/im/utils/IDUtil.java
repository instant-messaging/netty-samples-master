package com.bxs.netty.im.utils;

import java.util.UUID;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:58
 */
public class IDUtil {
    public static String randomId() {
        return UUID.randomUUID().toString().split("-")[0];
    }
}

package com.bxs.protocol.custom.common.codec;

import com.bxs.protocol.custom.common.constant.NettyConstant;
import com.bxs.protocol.custom.common.entity.Body;
import com.bxs.protocol.custom.common.entity.Header;
import com.bxs.protocol.custom.common.entity.Message;
import com.bxs.protocol.custom.common.util.ByteUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 10:45
 */
public class MessageDecoder extends ByteToMessageDecoder {
    private static final Logger logger = LoggerFactory.getLogger(MessageDecoder.class);

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        logger.info("解码");
        Integer length = in.readInt();
        Integer code = in.readInt();
        Long sessionId = in.readLong();
        Byte type = in.readByte();
        Byte priority = in.readByte();

        Map<String, Object> map = new HashMap<>();
        Integer temp = NettyConstant.HEADER_LENGTH;
        if (in.isReadable()) {
            Short size = in.readShort();
            temp += 2;
            while (map.size() < size) {
                int keyLen = in.readInt();
                byte[] key = new byte[keyLen];
                in.readBytes(key, 0, keyLen);
                int valueLen = in.readInt();
                byte[] value = new byte[valueLen];
                in.readBytes(value, 0, valueLen);
                map.put(new String(key, CharsetUtil.UTF_8), ByteUtil.byteToObject(value));
                temp = temp + keyLen + valueLen;
            }
        }

        Object payload = null;
        if (in.isReadable()) {
            int bodyLen = length - temp;
            byte[] body = new byte[bodyLen];
            in.readBytes(body);
            payload = ByteUtil.byteToObject(body);
        }

        Header header = Header.builder()
                .length(length)
                .code(code)
                .attachment(map)
                .priority(priority)
                .sessionId(sessionId)
                .type(type)
                .build();
        Body body = new Body(payload);
        Message message = Message.builder().header(header).body(body).build();
        logger.info("解码结束！message={}", message.toString());

        out.add(message);
    }
}

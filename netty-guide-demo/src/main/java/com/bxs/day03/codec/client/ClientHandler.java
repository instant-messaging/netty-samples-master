package com.bxs.day03.codec.client;

import com.bxs.day03.common.Message;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * My ClientHandler.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 18:11
 */
public class ClientHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object obj) throws Exception {
        Channel incoming = ctx.channel();

        if (obj instanceof Message) {
            Message message = (Message) obj;
            System.out.println("Server->Client:" + incoming.remoteAddress() + message.getBody());
        } else {
            System.out.println("Server->Client:" + incoming.remoteAddress() + obj.toString());
        }
    }
}

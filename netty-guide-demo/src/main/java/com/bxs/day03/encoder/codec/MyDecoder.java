package com.bxs.day03.encoder.codec;

import com.bxs.day03.common.Message;
import com.bxs.day03.common.MessageHeader;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.nio.charset.StandardCharsets;

/**
 * My Decoder.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 9:15
 */
public class MyDecoder extends LengthFieldBasedFrameDecoder {

    private static final int MAX_FRAME_LENGTH = 1024 * 1024;
    private static final int LENGTH_FIELD_LENGTH = 4;
    private static final int LENGTH_FIELD_OFFSET = 1;
    private static final int LENGTH_ADJUSTMENT = 0;
    private static final int INITIAL_BYTES_TO_STRIP = 0;

    private static final int HEADER_SIZE = 5;
    /**
     * 消息类型
     */
    private byte msgType;
    /**
     * 长度
     */
    private int len;

    public MyDecoder() {
        super(MAX_FRAME_LENGTH, LENGTH_FIELD_OFFSET, LENGTH_FIELD_LENGTH, LENGTH_ADJUSTMENT, INITIAL_BYTES_TO_STRIP);
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf in2) throws Exception {
        ByteBuf in = (ByteBuf) super.decode(ctx, in2);
        if (in == null) {
            return null;
        }

        // 校验长度
        if (in.readableBytes() < HEADER_SIZE) {
            return null;
        }

        msgType = in.readByte();
        len = in.readInt();

        // 校验消息体长度
        if (in.readableBytes() < len) {
            return null;
        }

        ByteBuf buf = in.readBytes(len);
        byte[] req = new byte[buf.readableBytes()];
        buf.readBytes(req);
        String body = new String(req, StandardCharsets.UTF_8);

        // ByteBuf 转为 Msg 类型
        Message msg = new Message();
        MessageHeader header = new MessageHeader(msgType, len);
        msg.setMessageHeader(header);
        msg.setBody(body);
        return msg;
    }
}

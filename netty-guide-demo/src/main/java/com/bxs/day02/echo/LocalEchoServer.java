package com.bxs.day02.echo;

import com.bxs.day02.echo.tcp.server.EchoServerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.local.LocalEventLoopGroup;
import io.netty.channel.local.LocalServerChannel;

/**
 * Local Echo Server.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 17:19
 */
public class LocalEchoServer {

    public static int DEFAULT_PORT = 7;

    public static void main(String[] args) throws Exception {
        int port;

        try {
            port = Integer.parseInt(args[0]);
        } catch (RuntimeException ex) {
            port = DEFAULT_PORT;
        }

        // 多线程事件循环器
        // 主线程
        EventLoopGroup bossGroup = new LocalEventLoopGroup();
        // 从线程
        EventLoopGroup workerGroup = new LocalEventLoopGroup();

        try {
            // 启动NIO服务的引导程序类
            ServerBootstrap bootstrap = new ServerBootstrap();

            // 设置EventLoopGroup
            bootstrap.group(bossGroup, workerGroup)
                    // 指明新的Channel的类型
                    .channel(LocalServerChannel.class)
                    // 设置的ServerChannel的一些选项
                    .option(ChannelOption.SO_BACKLOG, 128)
                    // 设置的ServerChannel的子Channel的选项
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    // 指定ChannelHandler
                    .childHandler(new EchoServerHandler());

            // 绑定端口，开始接收进来的连接
            ChannelFuture future = bootstrap.bind(port).sync();

            System.out.println("EchoServer已启动，端口：" + port);

            // 等待服务器 socket 关闭 。
            // 在这个例子中，这不会发生，但你可以优雅地关闭你的服务器。
            future.channel().closeFuture().sync();
        } finally {

            // 优雅的关闭
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }

    }
}

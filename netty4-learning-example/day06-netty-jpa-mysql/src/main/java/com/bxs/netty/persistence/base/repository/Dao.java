package com.bxs.netty.persistence.base.repository;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.List;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:09
 */
public interface Dao<T> {

    List<T> find();

    T findById(Long id);

    ListenableFuture<T> findByIdAsync(Long id);

    T save(T t);

    boolean removeById(Long id);
}

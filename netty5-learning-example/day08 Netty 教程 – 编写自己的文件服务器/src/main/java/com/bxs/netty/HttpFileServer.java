package com.bxs.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.stream.ChunkedWriteHandler;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/25 10:43
 */
public class HttpFileServer {
    private static final int DEFAULT_PORT = 4040;
    private static final String DEFAULT_IP = "192.168.101.209";
    /**
     * 对应的映射目录
     */
    private static final String DEFAULT_HOME = "D:\\Downloads";

    static void run(final int port, final String path) throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
                            channel.pipeline().addLast("http-decoder", new HttpRequestDecoder());
                            channel.pipeline().addLast("http-aggregator", new HttpObjectAggregator(8 * 1024));
                            channel.pipeline().addLast("http-encoder", new HttpResponseEncoder());
                            channel.pipeline().addLast("http-chunked", new ChunkedWriteHandler());
                            channel.pipeline().addLast("fileServerHandler", new HttpFileServerHandler(path));
                        }
                    });
            ChannelFuture future = bootstrap.bind(DEFAULT_IP, port).sync();
            System.out.println("HTTP文件目录服务器启动，网址是 : " + "http://" + DEFAULT_IP + ":" + port);
            future.channel().closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        HttpFileServer.run(DEFAULT_PORT, DEFAULT_HOME);
    }
}

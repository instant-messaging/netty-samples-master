package com.bxs.chat.common;

/**
 * 游戏的 update 接口
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/17 8:46
 */
public interface IUpdatable {
    public boolean update();
}

package com.bxs.protocol.custom.common.enums;

/**
 * 消息类型枚举
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/25 10:50
 */
public enum MessageType {

    BIZ_REQUEST(0,"业务请求"),
    BIZ_RESPONSE(1,"业务相应"),
    BIZ_ONEWAY(2,"即是请求也是响应"),
    HANDSHAKE_REQUEST(3,"握手请求"),
    HANDSHAKE_RESPONSE(4,"握手响应"),
    HEARTBEAT_REQUEST(5,"心跳请求"),
    HEARTBEAT_RESPONSE(6,"心跳响应"),
    ;

    private Integer type;
    private String name;

    MessageType(Integer type,String name){
        this.name = name;
        this.type = type;
    }

    public Integer getType() {
        return type;
    }
    public String getName(){
        return name;
    }
}

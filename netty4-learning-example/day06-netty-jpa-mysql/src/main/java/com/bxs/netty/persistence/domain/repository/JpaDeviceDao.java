package com.bxs.netty.persistence.domain.repository;

import com.bxs.netty.persistence.annotation.SqlDao;
import com.bxs.netty.persistence.base.repository.JpaAbstractDao;
import com.bxs.netty.persistence.domain.vo.Device;
import com.bxs.netty.persistence.domain.entity.DeviceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:16
 */
@SqlDao
@Component
public class JpaDeviceDao extends JpaAbstractDao<DeviceEntity, Device> implements DeviceDao {

    @Autowired
    private DeviceRepository deviceRepository;

    @Override
    protected Class<DeviceEntity> getEntityClass() {
        return DeviceEntity.class;
    }

    @Override
    protected CrudRepository<DeviceEntity, Long> getCrudRepository() {
        return deviceRepository;
    }
}

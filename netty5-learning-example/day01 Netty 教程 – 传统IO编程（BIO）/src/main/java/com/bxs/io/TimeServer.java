package com.bxs.io;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * 同步阻塞方式的I/O创建
 *
 * @Author: Mr.Lu
 * @Date: 2020/8/22 16:29
 */
public class TimeServer {

    public static void main(String[] args) {
        int port = 4040;

        System.out.println("start server......" + port);
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                new TimeServerHandler(serverSocket.accept()).run();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

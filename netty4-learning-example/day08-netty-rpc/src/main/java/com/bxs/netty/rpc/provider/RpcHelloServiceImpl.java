package com.bxs.netty.rpc.provider;

import com.bxs.netty.rpc.api.IRpcHelloService;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/9 11:57
 */
public class RpcHelloServiceImpl implements IRpcHelloService {
    @Override
    public String hello(String name) {
        return "Hello " + name + "!";
    }
}

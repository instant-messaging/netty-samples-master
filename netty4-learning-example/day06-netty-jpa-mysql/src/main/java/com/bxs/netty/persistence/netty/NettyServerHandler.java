package com.bxs.netty.persistence.netty;

import com.alibaba.fastjson.JSONObject;
import com.bxs.netty.persistence.domain.vo.Device;
import com.bxs.netty.persistence.service.DeviceService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.net.InetAddress;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:23
 */
@ChannelHandler.Sharable
public class NettyServerHandler extends SimpleChannelInboundHandler<String> {

    private final DeviceService deviceService;

    public NettyServerHandler(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // Send greeting for a new connection.
        ctx.write("Welcome to " + InetAddress.getLocalHost().getHostName() + "!\r\n");
        ctx.write("It is " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + " now.\r\n");
        ctx.flush();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String request) throws Exception {
        try {
            JSONObject jsonObject = JSONObject.parseObject(request);
            Device device = jsonObject.toJavaObject(Device.class);
            deviceService.saveDevice(device);
            ctx.write("Successfully saved!\r\n");
        } catch (Exception e) {
            ctx.write("error Json format!\r\n");
            e.printStackTrace();
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}

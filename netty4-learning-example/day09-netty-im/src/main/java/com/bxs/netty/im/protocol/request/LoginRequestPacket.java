package com.bxs.netty.im.protocol.request;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;

import static com.bxs.netty.im.protocol.command.Command.LOGIN_REQUEST;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:48
 */
@Data
public class LoginRequestPacket extends Packet {
    private String username;

    private String password;

    @Override
    public Byte getCommand() {
        return LOGIN_REQUEST;
    }
}

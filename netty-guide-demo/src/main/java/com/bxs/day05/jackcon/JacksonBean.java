package com.bxs.day05.jackcon;

import java.util.List;
import java.util.Map;

/**
 * POJO: Jackson Bean.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 11:20
 */
public class JacksonBean {

    private int age;
    private String name;
    private List<String> sons;
    private Map<String, String> addrs;

    public int getAge() {
        return age;
    }

    public JacksonBean setAge(int age) {
        this.age = age;
        return this;
    }

    public String getName() {
        return name;
    }

    public JacksonBean setName(String name) {
        this.name = name;
        return this;
    }

    public List<String> getSons() {
        return sons;
    }

    public JacksonBean setSons(List<String> sons) {
        this.sons = sons;
        return this;
    }

    public Map<String, String> getAddrs() {
        return addrs;
    }

    public JacksonBean setAddrs(Map<String, String> addrs) {
        this.addrs = addrs;
        return this;
    }
}

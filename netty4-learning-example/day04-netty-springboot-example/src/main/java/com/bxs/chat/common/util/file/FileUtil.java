package com.bxs.chat.common.util.file;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.Base64Utils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/17 9:19
 */
public final class FileUtil {

    private FileUtil() {
    }

    public static FileInputStream openInputStream(File file) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
                throw new IOException("File '" + file + "' exists but is a directory");
            } else if (!file.canRead()) {
                throw new IOException("File '" + file + "' cannot be read");
            } else {
                return new FileInputStream(file);
            }
        } else {
            throw new FileNotFoundException("File '" + file + "' does not exist");
        }
    }

    /**
     * 创建一个目录
     *
     * @param dir
     * @throws RuntimeException ,创建目录失败会抛出此异常
     */
    public static void createDir(File dir) {
        if (!dir.exists() && !dir.mkdirs()) {
            throw new RuntimeException("Can't create the dir [" + dir + ']');
        }
    }

    /**
     * 删除一个文件或者目录
     *
     * @param file
     */
    public static void delete(File file) {
        if (file.isFile()) {
            file.delete();
        } else if (file.isDirectory()) {
            File[] _files = file.listFiles();
            for (File _f : _files) {
                delete(_f);
            }
            file.delete();
        }
    }

    /**
     * 删除一个目录下的除exculde指定的后缀名外的所有子文件或子目录
     *
     * @param file
     */
    public static void cleanFolder(File file, String exculde) {
        if (!file.isDirectory()) {
            return;
        }

        File[] _files = file.listFiles();
        for (File _f : _files) {
            if (_f.getName().endsWith(exculde)) {
                continue;
            }
            delete(_f);
        }
    }

    public static String readFileToString(File file, String encoding) throws IOException {
        FileInputStream in = null;

        String var3;
        try {
            in = openInputStream(file);
            var3 = IOUtil.toString(in, encoding);
        } finally {
            IOUtil.closeQuietly(in);
        }

        return var3;
    }

    /**
     * 压缩zip文件
     *
     * @param file
     * @param dest
     * @throws IOException
     */
    public static void zip(File file, File dest) throws IOException {
        ZipOutputStream _zip1 = new ZipOutputStream(new FileOutputStream(dest));
        zipFiles(file, _zip1, file.getAbsolutePath());
        _zip1.close();
    }

    private static void zipFiles(File file, ZipOutputStream out, String root) throws IOException {
        String _entryName = file.getAbsolutePath();
        if (_entryName.equals(root)) {
            //
            _entryName = "";
        } else {
            int _ri = _entryName.indexOf(root);
            _entryName = _entryName.substring(_ri + root.length() + 1);
            _entryName = _entryName.replace('\\', '/');
        }
        if (file.isFile()) {
            out.putNextEntry(new ZipEntry(_entryName));
            FileInputStream _fin = new FileInputStream(file);
            byte[] _b = new byte[1024];
            int len;
            while ((len = _fin.read(_b)) != -1) {
                out.write(_b, 0, len);
            }
            _fin.close();
        } else {
            out.putNextEntry(new ZipEntry(_entryName + '/'));
            File[] _files = file.listFiles();
            for (File _f : _files) {
                zipFiles(_f, out, root);
            }
        }
    }

    public static URL getConfigURL(String fileName) {
        ClassLoader classLoader = Thread.currentThread()
                .getContextClassLoader();
        return classLoader.getResource(fileName);
    }

    public static String getConfigPath(String fileName) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return classLoader.getResource(fileName).getPath();
    }

    public static File getFile(String filePath) {
        URL url = getConfigURL(filePath);
        if (url != null) {
            return new File(url.getFile());
        }
        return null;
    }

    //===============================================================================================

    /**
     * 根据url拿取file
     *
     * @param url    url地址
     * @param suffix 文件后缀名
     */
    public static File createFileByUrl(String url, String suffix) {
        byte[] byteFile = getImageFromNetByUrl(url);
        if (byteFile != null) {
            return getFileFromBytes(byteFile, suffix);
        } else {
            return null;
        }
    }

    /**
     * 根据地址获得数据的字节流
     *
     * @param strUrl 网络连接地址
     * @return 字节流信息
     */
    private static byte[] getImageFromNetByUrl(String strUrl) {
        try {
            URL url = new URL(strUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5 * 1000);
            // 通过输入流获取图片数据
            InputStream inStream = conn.getInputStream();
            // 得到图片的二进制数据
            return readInputStream(inStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 从输入流中获取数据
     *
     * @param inStream 输入流
     * @return 字节信息流
     * @throws Exception 向上抛出异常
     */
    private static byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }

    /**
     * 创建临时文件
     *
     * @param b      字节流
     * @param suffix 后缀
     * @return File文件
     */
    private static File getFileFromBytes(byte[] b, String suffix) {
        BufferedOutputStream stream = null;
        File file = null;
        try {
            file = File.createTempFile("pattern", "." + suffix);
            System.out.println("临时文件位置：" + file.getCanonicalPath());
            FileOutputStream fstream = new FileOutputStream(file);
            stream = new BufferedOutputStream(fstream);
            stream.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    /**
     * 创建一个.img图片文件
     *
     * @param url 文件路径
     * @return MultipartFile对象
     */
    public static MultipartFile createImg(String url) {
        try {
            // File转换成MultipartFile
            File file = FileUtil.createFileByUrl(url, "jpg");
            assert file != null;
            FileInputStream inputStream = new FileInputStream(file);
            return new MockMultipartFile(file.getName(), inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将文件转换为MultipartFile
     *
     * @param filePath 文件路径
     * @return 转换完成的MultipartFile对象
     */
    public static MultipartFile fileToMultipart(String filePath) {
        try {
            // File转换成MultipartFile
            File file = new File(filePath);
            FileInputStream inputStream = new FileInputStream(file);
            return new CommonsMultipartFile(new DiskFileItem(file.getParentFile().getName(), "image/png", false, file.getName(), Integer.MAX_VALUE, file));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将base64字符串转换为文件
     *
     * @param filePath   保存路径
     * @param base64Data base64数据
     * @return 转换完成的文件对象
     * @throws Exception 向上抛出异常
     */
    public static boolean base64ToFile(String filePath, String base64Data) throws Exception {
        String data;

        if (base64Data == null || "".equals(base64Data)) {
            return false;
        } else {
            String[] d = base64Data.split("base64,");
            if (d.length == 2) {
                data = d[1];
            } else {
                return false;
            }
        }

        // 因为BASE64Decoder的jar问题，此处使用spring框架提供的工具包
        byte[] bs = Base64Utils.decodeFromString(data);
        // 使用apache提供的工具类操作流
        org.apache.commons.io.FileUtils.writeByteArrayToFile(new File(filePath), bs);

        return true;
    }
}

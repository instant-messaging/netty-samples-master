package com.bxs.netty.iterator;

import com.bxs.netty.iterator.bst.BstIterator;
import com.bxs.netty.iterator.bst.TreeNode;
import com.bxs.netty.iterator.list.Item;
import com.bxs.netty.iterator.list.ItemType;
import com.bxs.netty.iterator.list.TreasureChest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.bxs.netty.iterator.list.ItemType.*;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:15
 */
public class App {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    private static final TreasureChest TREASURE_CHEST = new TreasureChest();

    private static void demonstrateTreasureChestIteratorForType(ItemType itemType) {
        LOGGER.info("------------------------");
        LOGGER.info("Item Iterator for ItemType " + itemType + ": ");
        Iterator<Item> itemIterator = TREASURE_CHEST.iterator(itemType);
        while (itemIterator.hasNext()) {
            LOGGER.info(itemIterator.next().toString());
        }
    }

    private static void demonstrateBstIterator() {
        LOGGER.info("------------------------");
        LOGGER.info("BST Iterator: ");
        TreeNode<Integer> root = buildIntegerBst();
        BstIterator<Integer> bstIterator = new BstIterator<Integer>(root);
        while (bstIterator.hasNext()) {
            LOGGER.info("Next node: " + bstIterator.next().getVal());
        }
    }

    private static TreeNode<Integer> buildIntegerBst() {
        TreeNode<Integer> root = new TreeNode<Integer>(8);

        root.insert(3);
        root.insert(10);
        root.insert(1);
        root.insert(6);
        root.insert(14);
        root.insert(4);
        root.insert(7);
        root.insert(13);

        return root;
    }

    /**
     * Program entry point.
     *
     * @param args command line args
     */
    public static void main(String[] args) {
        demonstrateTreasureChestIteratorForType(RING);
        demonstrateTreasureChestIteratorForType(POTION);
        demonstrateTreasureChestIteratorForType(WEAPON);
        demonstrateTreasureChestIteratorForType(ANY);

        demonstrateBstIterator();
    }
}

package com.bxs.netty.singleton.type;

/**
 * 基于枚举的单例实现, Effective Java 中文第二版(Joshua Bloch) p.15
 * 此实现是线程安全的，但是添加任何其他方法及其线程安全是开发人员的责任
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:02
 */
public enum EnumMouseDriver {
    INSTANCE;

    @Override
    public String toString(){
        return getDeclaringClass().getCanonicalName() + "@" + hashCode();
    }
}

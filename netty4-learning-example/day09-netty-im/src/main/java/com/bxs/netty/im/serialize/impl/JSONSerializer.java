package com.bxs.netty.im.serialize.impl;

import com.alibaba.fastjson.JSON;
import com.bxs.netty.im.serialize.Serializer;
import com.bxs.netty.im.serialize.SerializerAlgorithm;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 18:01
 */
public class JSONSerializer implements Serializer {

    @Override
    public byte getSerializerAlgorithm() {
        return SerializerAlgorithm.JSON;
    }

    @Override
    public byte[] serialize(Object object) {
        return JSON.toJSONBytes(object);
    }

    @Override
    public <T> T deserialize(Class<T> clazz, byte[] bytes) {
        return JSON.parseObject(bytes, clazz);
    }
}

package com.bxs.netty.persistence.domain.repository;

import com.bxs.netty.persistence.annotation.SqlDao;
import com.bxs.netty.persistence.domain.entity.DeviceEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:15
 */
@SqlDao
public interface DeviceRepository extends CrudRepository<DeviceEntity, Long> {
}

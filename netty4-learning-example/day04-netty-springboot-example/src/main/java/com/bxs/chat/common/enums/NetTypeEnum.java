package com.bxs.chat.common.enums;

/**
 * 网络类型
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/17 8:48
 */
public enum NetTypeEnum {
    HTTP,
    WEBSOCKET,
    TCP,
    UDP
    ;
}

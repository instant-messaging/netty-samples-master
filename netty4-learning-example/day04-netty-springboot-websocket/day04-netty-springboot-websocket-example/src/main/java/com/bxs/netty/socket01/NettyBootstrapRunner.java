package com.bxs.netty.socket01;

import com.bxs.netty.socket01.handler.HeartBeatServerHandler;
import com.bxs.netty.socket01.handler.WebsocketMessageHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 初始化Netty服务
 * <p>
 * 大概流程：
 * 消息发送 ：用户1先连接通道，然后发送消息给用户2，用户2若是在线直接可以发送给用户，若没在线可以将消息暂存在redis或者通道里，用户2链接通道的话，两者可以直接通讯。
 * 消息推送 ：用户1连接通道，根据通道id查询要推送的人是否在线，或者推送给所有人，这里我只推送给指定的人。
 * <p>
 * 服务端
 * 1.创建一个ServerBootstrap的实例引导和绑定服务器。
 * 2.创建并分配一个NioEventLoopGroup实例以进行事件的处理，比如接受连接以及读写数据。
 * 3.指定服务器绑定的本地的InetSocketAddress。
 * 4.使用一个EchoServerHandler的实例初始化每一个新的Channel。
 * 5.调用ServerBootstrap.bind()方法以绑定服务器。
 * </p>
 *
 * @Author: Mr.Lu
 * @Date: 2020/10/17 15:49
 */
@Component
public class NettyBootstrapRunner implements ApplicationRunner, ApplicationListener<ContextClosedEvent>, ApplicationContextAware {
    private static final Logger LOGGER = LoggerFactory.getLogger(NettyBootstrapRunner.class);

    private static final int RECVBYTE_ALLOCATOR_SIZE = 592048;
    private static final int maxContentLength = 65536;

    @Value("${netty.tcp.backlog}")
    private int backlog;

    @Value("${netty.tcp.keepalive}")
    private boolean keepalive;

    @Value("${netty.tcp.noDelay}")
    private boolean noDelay;

    @Value("${netty.websocket.port}")
    private int port;

    @Value("${netty.websocket.ip}")
    private String ip;

    @Value("${netty.websocket.path}")
    private String path;

    @Value("${netty.websocket.max-frame-size}")
    private long maxFrameSize;

    /**
     * NioEventLoop并不是一个纯粹的I/O线程，它除了负责I/O的读写之外, 创建了两个NioEventLoopGroup，它们实际是两个独立的Reactor线程池。
     * 一个用于接收客户端的TCP连接，
     * 另一个用于处理I/O相关的读写操作，或者执行系统Task、定时任务Task等。
     * <p>
     * 创建 boss 线程组 用于服务端接受客户端的连接
     */
    private final EventLoopGroup bossGroup = new NioEventLoopGroup();
    /**
     * 创建 worker 线程组 用于进行 SocketChannel 的数据读写
     */
    private final EventLoopGroup workerGroup = new NioEventLoopGroup();

    private ApplicationContext applicationContext;

    private Channel serverChannel;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * 启动服务
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 创建 ServerBootstrap 对象
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        // 绑定线程池, 设置使用的EventLoopGroup
        serverBootstrap.group(bossGroup, workerGroup);
        // 指定使用的channel
        serverBootstrap.channel(NioServerSocketChannel.class);
        // 使用指定的端口设置套接字地址
        serverBootstrap.localAddress(new InetSocketAddress(this.ip, this.port));

        // 服务端可连接队列数,对应TCP/IP协议listen函数中backlog参数
        serverBootstrap.option(ChannelOption.SO_BACKLOG, backlog);

        // 设置TCP长连接,一般如果两个小时内没有数据的通信时,TCP会自动发送一个活动探测数据报文
        serverBootstrap.childOption(ChannelOption.SO_KEEPALIVE, keepalive);

        // 将小的数据包包装成更大的帧进行传送，提高网络的负载,即TCP延迟传输
        serverBootstrap.childOption(ChannelOption.TCP_NODELAY, noDelay);

        // 配置固定长度接收缓存内存分配
        serverBootstrap.childOption(ChannelOption.RCVBUF_ALLOCATOR, new FixedRecvByteBufAllocator(RECVBYTE_ALLOCATOR_SIZE));

        // 设置 NioServerSocketChannel 的处理器
        serverBootstrap.handler(new LoggingHandler(LogLevel.INFO));

        // 设置连入服务端的 Client 的 SocketChannel 的处理器
        serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                LOGGER.info("收到新的客户端连接: {}", ch.toString());

                ChannelPipeline pipeline = ch.pipeline();

                // ------------------
                // 用于支持Http协议
                // ------------------
                // websocket协议本身是基于http协议的，所以这边也要使用http解编码器
                pipeline.addLast(new HttpServerCodec());

                // ------------------
                // 以块的方式来写的处理器
                // ------------------
                // 主要用于处理大数据流,比如一个1G大小的文件如果你直接传输肯定会撑暴jvm内存的 ,加上这个handler我们就不用考虑这个问题了
                pipeline.addLast(new ChunkedWriteHandler());
                // 添加对HTTP请求和响应的聚合器:只要使用Netty进行Http编程都需要使用
                // 主要是将同一个http请求或响应的多个消息对象变成一个 fullHttpRequest完整的消息对象
                // 设置单次请求的文件的大小
                pipeline.addLast(new HttpObjectAggregator(maxContentLength));

                // ------------------
                // 心跳处理
                // ------------------
                // 添加Netty空闲超时检查的支持
                // 1. 读空闲超时（超过一定的时间会发送对应的事件消息）
                // 2. 写空闲超时
                // 3. 读写空闲超时
                // 心跳检测,参数说明：[长时间未读:长时间未写:长时间未读写:时间单位]~[读写是对连接本生而言，写：未向服务端发送消息，读：未收到服务端的消息]
                // 15 秒客户端没有向服务器发送心跳则关闭连接
                pipeline.addLast(new IdleStateHandler(8, 12, 60, TimeUnit.SECONDS));
                // 添加心跳处理
                pipeline.addLast(new HeartBeatServerHandler());

                // ------------------
                // 用于支持WebSocket协议
                // ------------------
                pipeline.addLast(new WebSocketServerCompressionHandler());
                pipeline.addLast(new WebSocketServerProtocolHandler(path, null, true, maxFrameSize));

                // 消息入站处理器
                pipeline.addLast(new ChannelInboundHandlerAdapter() {
                    @Override
                    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                        if (msg instanceof FullHttpRequest) {
                            FullHttpRequest fullHttpRequest = (FullHttpRequest) msg;
                            String uri = fullHttpRequest.uri();
                            if (!uri.equals(path)) {
                                // 访问的路径不是 websocket的端点地址，响应404
                                ctx.channel().writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND)).addListener(ChannelFutureListener.CLOSE);
                                return;
                            }
                        }
                        super.channelRead(ctx, msg);
                    }
                });

                /**
                 * 从IOC中获取到Handler
                 */
                pipeline.addLast(applicationContext.getBean(WebsocketMessageHandler.class));
            }
        });

        // 同步等待创建完成
        ChannelFuture future = serverBootstrap.bind().sync();
        if (future.isSuccess()) {
            serverChannel = future.channel();
            LOGGER.info("websocket 服务启动，ip={},port={}", this.ip, this.port);

            // 服务端管道关闭的监听器并同步阻塞,直到channel关闭,线程才会往下执行,结束进程
            serverChannel.closeFuture().syncUninterruptibly();
        } else {
            LOGGER.error("Netty server start up Error!");
            throw new Exception(future.cause());
        }
    }

    /**
     * 停止服务
     *
     * @param event
     */
    @SneakyThrows
    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        LOGGER.info("Netty server listening {} ...", serverChannel.localAddress());
        LOGGER.info("Shutdown Netty Server...");

        if (serverChannel != null) {
            // 关闭服务器通道
            serverChannel.close();
        }

        // 优雅关闭两个 EventLoopGroup 对象
        if (workerGroup != null) {
            workerGroup.shutdownGracefully().sync();
        }
        if (bossGroup != null) {
            bossGroup.shutdownGracefully().sync();
        }

        LOGGER.info("Shutdown Netty Server Success!");
    }

    public void run(String... args) throws Exception {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {

            }
        });
    }
}

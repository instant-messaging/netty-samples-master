package com.bxs.day02.discard.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

/**
 * Discard Server Handler.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 17:29
 */
public class DiscardServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        ByteBuf in = (ByteBuf) msg;
        try {
            // (1)
            while (in.isReadable()) {

                // 打印消息内容
                System.out.print((char) in.readByte());
                System.out.flush();
            }
        } finally {

            // 释放消息
            ReferenceCountUtil.release(msg);
            // 默默地丢弃收到的数据
            //in.release();
        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)
        // 当出现异常就关闭连接
        cause.printStackTrace();
        ctx.close();
    }
}

package com.bxs.day03.encoder.codec;

import com.bxs.day03.common.Message;
import com.bxs.day03.common.MessageHeader;
import com.bxs.day03.common.MessageType;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.CharsetUtil;

/**
 * My Encoder.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 9:04
 */
public class MyEncoder extends MessageToByteEncoder<Message> {

    @Override
    protected void encode(ChannelHandlerContext ctx, Message msg, ByteBuf out) throws Exception {
        if (msg == null | msg.getMessageHeader() == null) {
            throw new Exception("The encode message is null");
        }

        // 获取消息头
        MessageHeader header = msg.getMessageHeader();

        // 获取消息体
        String body = msg.getBody();
        byte[] bodyBytes = body.getBytes(CharsetUtil.UTF_8);

        // 计算消息体的长度
        int bodySize = bodyBytes.length;

        System.out.printf("MyEncoder header: %s, body: %s", header.getMessageType(), body);

        out.writeByte(MessageType.EMGW_LOGIN_RES.getValue());
        out.writeInt(bodySize);
        out.writeBytes(bodyBytes);
    }
}

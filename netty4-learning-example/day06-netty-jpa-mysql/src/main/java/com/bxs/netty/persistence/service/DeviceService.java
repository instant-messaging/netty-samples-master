package com.bxs.netty.persistence.service;

import com.bxs.netty.persistence.domain.vo.Device;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:13
 */
public interface DeviceService {

    Device saveDevice(Device device);
}

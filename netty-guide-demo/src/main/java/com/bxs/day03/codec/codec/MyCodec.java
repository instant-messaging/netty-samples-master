package com.bxs.day03.codec.codec;

import io.netty.channel.CombinedChannelDuplexHandler;

/**
 * My Codec.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 18:04
 */
public class MyCodec extends CombinedChannelDuplexHandler<MyDecoder, MyEncoder> {
    public MyCodec() {
        super(new MyDecoder(), new MyEncoder());
    }
}

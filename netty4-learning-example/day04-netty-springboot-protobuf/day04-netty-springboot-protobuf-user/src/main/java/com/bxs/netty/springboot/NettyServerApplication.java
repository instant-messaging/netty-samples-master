package com.bxs.netty.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/9 13:07
 */
@SpringBootApplication
@ComponentScan({"com.bxs.netty.springboot.server"})
public class NettyServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NettyServerApplication.class);
    }
}

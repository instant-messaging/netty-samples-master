package com.bxs.day05.serialization;

import java.io.Serializable;

/**
 * POJO: a Serialization Bean
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 11:02
 */
public class SerializationBean implements Serializable {
    private static final long serialVersionUID = 3235432002462705915L;

    private int age;
    private String name;

    public int getAge() {
        return age;
    }

    public SerializationBean setAge(int age) {
        this.age = age;
        return this;
    }

    public String getName() {
        return name;
    }

    public SerializationBean setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "SerializationBean{" + "age=" + age + ", name='" + name + '\'' + '}';
    }
}

package com.bxs.chat.service;

import com.alibaba.fastjson.JSONObject;
import io.netty.channel.ChannelHandlerContext;

/**
 * 领域服务-聊天服务
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 11:54
 */
public interface ChatService {

    /**
     * 用户注册登录
     * @param param 参数类型
     * @param ctx 处理上下午文
     */
    void register(JSONObject param, ChannelHandlerContext ctx);

    /**
     * 用户-用户之间相互之间发送消息
     * @param param 参数类型
     * @param ctx 处理上下午文
     */
    void sendOne(JSONObject param, ChannelHandlerContext ctx);

    /**
     * 用户-群之间相互发送消息
     * @param param 参数类型
     * @param ctx 处理上下午文
     */
    void sendGroup(JSONObject param, ChannelHandlerContext ctx);

    /**
     * 用户-用户互发文件
     * @param param 参数类型
     * @param ctx 处理上下午文
     */
    void sendOneFile(JSONObject param, ChannelHandlerContext ctx);

    /**
     * 用户向群发送文件
     * @param param 参数类型
     * @param ctx 处理上下午文
     */
    void sendGroupsFile(JSONObject param, ChannelHandlerContext ctx);

    /**
     * 下线移除
     * @param ctx 处理上下文
     */
    void remove(ChannelHandlerContext ctx);

    /**
     * 不存在该类型业务
     * @param ctx 处理上下文
     */
    void typeError(ChannelHandlerContext ctx);
}

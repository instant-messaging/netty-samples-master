package com.bxs.day03.encoder.client;

import com.bxs.day03.common.Message;
import com.bxs.day03.common.MessageHeader;
import com.bxs.day03.common.MessageType;
import com.bxs.day03.encoder.codec.MyDecoder;
import com.bxs.day03.encoder.codec.MyEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.nio.charset.Charset;

/**
 * My Client.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 9:55
 */
public class Client {

    private String host;
    private int port;

    public Client(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public static void main(String[] args) throws InterruptedException {
        new Client("localhost", 8082).run();
    }

    public void run() throws InterruptedException {

        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(workerGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast("decoder", new MyDecoder());
                            pipeline.addLast("encoder", new MyEncoder());
                            pipeline.addLast(new ClientHandler());

                        }
                    });

            // 启动客户端
            ChannelFuture future = bootstrap.connect(host, port).sync();

            while (true) {

                // 发送消息给服务器
                Message msg = new Message();
                MessageHeader header = new MessageHeader();
                header.setMessageType(MessageType.EMGW_LOGIN_REQ.getValue());
                String body = "床前明月光，疑是地上霜。举头望明月，低头思故乡。";

                byte[] bodyBytes = body.getBytes(Charset.forName("utf-8"));
                int    bodySize  = bodyBytes.length;
                header.setLen(bodySize);
                msg.setMessageHeader(header);
                msg.setBody(body);

                future.channel().writeAndFlush(msg);
                Thread.sleep(2000);
            }
        } finally {
            workerGroup.shutdownGracefully();
        }
    }
}

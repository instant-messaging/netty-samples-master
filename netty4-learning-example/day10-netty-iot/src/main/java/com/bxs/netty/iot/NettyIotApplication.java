package com.bxs.netty.iot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 11:14
 */
@SpringBootApplication
public class NettyIotApplication {

    public static void main(String[] args) {
        SpringApplication.run(NettyIotApplication.class, args);
    }
}

package com.bxs.netty.kafka.kafka;

import java.util.Map;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 15:09
 */
public abstract class Adapter {

    public void close() {}
    public void configure(Map<String, ?> configs, boolean isKey) {}
}

package com.bxs.day06.secureecho;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.ssl.SslHandler;

import javax.net.ssl.SSLEngine;

/**
 * Echo Client ChannelInitializer.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 13:38
 */
public class EchoClientChannelInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        // 先添加SslHandler
        String pkPath = System.getProperties().getProperty("user.dir") + "/netty-user-guide-demos/src/main/resources/ssl/nettyClient.jks";
        String password = "defaultPass";
        SSLEngine engine = SslContextFactory.getServerContext(pkPath, pkPath, password).createSSLEngine();
        // 设置为服务器模式
        engine.setUseClientMode(true);
        // 需要客户端认证
        engine.setNeedClientAuth(true);

        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new SslHandler(engine));
        // 再添加其他ChannelHandler
        pipeline.addLast(new EchoClientHandler());
    }
}

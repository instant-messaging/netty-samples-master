package com.bxs.netty.socket01.handler;

import com.alibaba.fastjson.JSON;
import com.bxs.netty.service.DiscardService;
import com.bxs.netty.utils.NettyAttrUtil;
import com.bxs.netty.utils.RequestURIUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.util.CharsetUtil;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static io.netty.handler.codec.http.HttpUtil.isKeepAlive;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Date: 2020/10/17 16:00
 */
@ChannelHandler.Sharable
@Component
public class WebsocketMessageHandler extends SimpleChannelInboundHandler<WebSocketFrame> {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebsocketMessageHandler.class);

    @Autowired
    private DiscardService discardService;

    @Setter
    private String webSocketURL;

    private WebSocketServerHandshaker handshaker;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        LOGGER.info("链接创建：{}", ctx.channel().remoteAddress());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        LOGGER.info("链接断开：{}", ctx.channel().remoteAddress());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame frame) throws Exception {
        /// 判断是否关闭链路的指令
        if (frame instanceof CloseWebSocketFrame) {
            //关闭握手
            if (handshaker != null){
                handshaker.close(ctx.channel(), (CloseWebSocketFrame) frame.retain());
            } else {
                ctx.writeAndFlush(frame.retainedDuplicate()).addListener(ChannelFutureListener.CLOSE);
            }
            SessionManager.removeChannel(ctx.channel());
            return;
        }
        // 判断是否ping消息
        if (frame instanceof PingWebSocketFrame) {
            ctx.channel().write(new PongWebSocketFrame(frame.content().retain()));
            return;
        }
        // 判断是否Pong消息
        if (frame instanceof PongWebSocketFrame) {
            ctx.writeAndFlush(new PongWebSocketFrame(frame.content().retain()));
            return;
        }
        // 二进制消息处理
        if (frame instanceof BinaryWebSocketFrame) {
            LOGGER.info("服务器接收到二进制消息. [{}]", frame.toString());

            ByteBuf content = frame.content();
            content.markReaderIndex();
            int flag = content.readInt();
            LOGGER.info("标志位:[{}]", flag);
            content.resetReaderIndex();

            ByteBuf byteBuf = Unpooled.directBuffer(frame.content().capacity());
            byteBuf.writeBytes(frame.content());

            // 转成byte
            byte [] bytes = new byte[frame.content().capacity()];
            byteBuf.readBytes(bytes);
            // byte转ByteBuf
            ByteBuf byteBuf2 = Unpooled.directBuffer(bytes.length);
            byteBuf2.writeBytes(bytes);

            LOGGER.info("JSON.toJSONString(byteBuf) [ {} ]", JSON.toJSONString(byteBuf));
            //TODO 这是发给自己
            ctx.writeAndFlush(new BinaryWebSocketFrame(byteBuf));
            return;
        }
        // 获取客户端发送过来的文本消息
        if (frame instanceof TextWebSocketFrame) {
            TextWebSocketFrame textWebSocketFrame = (TextWebSocketFrame) frame;
            // 业务层处理数据
            this.discardService.discard(textWebSocketFrame.text());
            // 响应客户端
            ctx.channel().writeAndFlush(new TextWebSocketFrame("我收到了你的消息：" + System.currentTimeMillis()));
        } else {
            // 不接受文本以外的数据帧类型
            ctx.channel().writeAndFlush(WebSocketCloseStatus.INVALID_MESSAGE_TYPE).addListener(ChannelFutureListener.CLOSE);
        }
    }
}

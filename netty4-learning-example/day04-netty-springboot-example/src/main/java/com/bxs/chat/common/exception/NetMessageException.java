package com.bxs.chat.common.exception;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/17 10:00
 */
public class NetMessageException extends Exception {

    private static final long serialVersionUID = 1L;

    public NetMessageException(String name){
        super(name);
    }
    public NetMessageException(String name, Throwable t){
        super(name,t);
    }

    public NetMessageException(Throwable t){
        super(t);
    }
}

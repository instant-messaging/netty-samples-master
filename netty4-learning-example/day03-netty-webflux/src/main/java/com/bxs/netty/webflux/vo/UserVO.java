package com.bxs.netty.webflux.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 13:35
 */
@Data
@ToString
public class UserVO implements Serializable {

    private Integer id;

    private String username;
}

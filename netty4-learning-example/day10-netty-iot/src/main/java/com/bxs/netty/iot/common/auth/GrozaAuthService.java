package com.bxs.netty.iot.common.auth;

/**
 * 用户和密码认证服务接口
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/8 11:16
 */
public interface GrozaAuthService {

    /**
     * 验证用户名和密码是否正确
     */
    boolean checkValid(String username, String password);
}

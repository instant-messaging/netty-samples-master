package com.bxs.netty.singleton.type;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:05
 */
public final class ThreadSafeLazyLoadedMouseDriver {

    private static ThreadSafeLazyLoadedMouseDriver instance;

    private ThreadSafeLazyLoadedMouseDriver() {
        // protect against instantiation via reflection
        if (instance == null) {
            instance = this;
        } else {
            throw new IllegalStateException("Already initialized.");
        }
    }

    /**
     * The instance gets created only when it is called for first time. Lazy-loading
     */
    public static synchronized ThreadSafeLazyLoadedMouseDriver getInstance() {
        if (instance == null) {
            instance = new ThreadSafeLazyLoadedMouseDriver();
        }

        return instance;
    }
}

package com.bxs.netty.iot.web;

import cn.hutool.core.io.IoUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.interfaces.RSAPrivateKey;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 13:57
 */
@Slf4j
@RestController
@RequestMapping("/groza/v1")
public class AuthApiController {

    @RequestMapping(value = "/{username}/auth",method = RequestMethod.GET,produces = "application/json")
    public String getPwd(@PathVariable("username") String username){
        RSAPrivateKey privateKey = IoUtil.readObj(AuthApiController.class.getClassLoader().getResourceAsStream("keystore/auth-private.key"));
        RSA rsa = new RSA(privateKey, null);
        return rsa.encryptBcd(username, KeyType.PrivateKey);
    }
}

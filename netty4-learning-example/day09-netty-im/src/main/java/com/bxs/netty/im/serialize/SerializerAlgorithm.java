package com.bxs.netty.im.serialize;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 18:01
 */
public interface SerializerAlgorithm {
    /**
     * json 序列化
     */
    byte JSON = 1;
}

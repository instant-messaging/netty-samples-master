package com.bxs.day08.http.base;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.util.AsciiString;
import io.netty.util.CharsetUtil;

/**
 * HTTP Server Handler.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 14:49
 */
public class HttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    private AsciiString contentType = HttpHeaderValues.TEXT_PLAIN;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {
        readRequest(request);

        String sendMsg;
        String uri = request.uri();

        switch (uri) {
            case "/":
                sendMsg = "<h3>Netty HTTP Server</h3><p>Welcome to <a href=\"https://waylau.com\">waylau.com</a>!</p>";
                break;
            case "/hi":
                sendMsg = "<h3>Netty HTTP Server</h3><p>Hello Word!</p>";
                break;
            case "/love":
                sendMsg = "<h3>Netty HTTP Server</h3><p>I Love You!</p>";
                break;
            default:
                sendMsg = "<h3>Netty HTTP Server</h3><p>I was lost!</p>";
                break;
        }
        sendMsg = "<!DOCTYPE html><html><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'></head><body>" + sendMsg + "<body></html>";
        writeResponse(ctx, sendMsg);
    }

    private void readRequest(FullHttpRequest request) {
        System.out.println("======请求行======");
        System.out.println(request.method() + " " + request.uri() + " " + request.protocolVersion());

        System.out.println("======请求头======");
        for (String name : request.headers().names()) {
            System.out.println(name + ": " + request.headers().get(name));
        }

        System.out.println("======消息体======");
        System.out.println(request.content().toString(CharsetUtil.UTF_8));

        System.out.println("======Class======");
        System.out.println(request.getClass().getName());
    }

    private void writeResponse(ChannelHandlerContext ctx, String message) {
        ByteBuf byteBuf = Unpooled.copiedBuffer(message, CharsetUtil.UTF_8);

        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, byteBuf);
        HttpHeaders headers = response.headers();
        headers.add(HttpHeaderNames.CONTENT_TYPE, contentType + "; charset=UTF-8");
        headers.set(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());
        headers.add(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);

        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE_ON_FAILURE);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        System.out.println("channelReadComplete");
        super.channelReadComplete(ctx);
//        ctx.flush(); // 4
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println("exceptionCaught");
        if(null != cause) cause.printStackTrace();
        if(null != ctx) ctx.close();
    }
}

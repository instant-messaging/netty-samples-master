package com.bxs.netty;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/24 16:30
 */
public interface EchoConstant {
    /**
     * 特殊分割符号,DelimiterBasedFrameDecoder使用
     */
    String SEPARATOR = "$_";
    Integer ECHO_DELIMITER_PORT = 4040;
    Integer ECHO_LENGTH_PORT = 5050;
    String HOST = "127.0.0.1";
    /**
     * 固定消息长度,FixedLengthFrameDecoder使用
     */
    Integer FRAME_LENGTH = 10;
}

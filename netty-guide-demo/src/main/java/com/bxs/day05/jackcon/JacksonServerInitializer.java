package com.bxs.day05.jackcon;

import com.bxs.day05.jackcon.codec.JacksonDecoder;
import com.bxs.day05.jackcon.codec.JacksonEncoder;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;

/**
 * JacksonServer ChannelInitializer.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 11:30
 */
public class JacksonServerInitializer extends ChannelInitializer<Channel> {

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new JacksonDecoder<JacksonBean>(JacksonBean.class));
        pipeline.addLast(new JacksonEncoder());
        pipeline.addLast(new JacksonServerHandler());
    }
}

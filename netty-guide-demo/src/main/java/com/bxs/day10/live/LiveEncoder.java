package com.bxs.day10.live;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.internal.StringUtil;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/10 11:45
 */
public class LiveEncoder extends MessageToByteEncoder<LiveMessage> {

    @Override
    protected void encode(ChannelHandlerContext ctx, LiveMessage message, ByteBuf out) throws Exception {
        out.writeByte(message.getType());
        out.writeInt(message.getLength());
        if (!StringUtil.isNullOrEmpty(message.getContent())) {
            out.writeBytes(message.getContent().getBytes());
        }
    }
}

package com.bxs.netty.im.protocol.response;

import com.bxs.netty.im.protocol.Packet;
import com.bxs.netty.im.session.Session;
import lombok.Data;

import java.util.List;

import static com.bxs.netty.im.protocol.command.Command.LIST_GROUP_MEMBERS_RESPONSE;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 9:00
 */
@Data
public class ListGroupMembersResponsePacket extends Packet {

    private String groupId;

    private List<Session> sessionList;

    @Override
    public Byte getCommand() {
        return LIST_GROUP_MEMBERS_RESPONSE;
    }
}

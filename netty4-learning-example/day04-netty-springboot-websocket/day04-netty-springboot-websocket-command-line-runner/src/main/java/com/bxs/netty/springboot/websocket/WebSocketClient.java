package com.bxs.netty.springboot.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 15:59
 */
@Component
@Order(1)
public class WebSocketClient implements CommandLineRunner {
    private static Logger logger = LoggerFactory.getLogger(WebSocketServer.class);

    @Override
    public void run(String... args) throws Exception {
        logger.info("我是第一个启动的方法");
    }
}

package com.bxs.netty.webflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 13:34
 */
@SpringBootApplication(scanBasePackages = {"com.bxs.netty.webflux"})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

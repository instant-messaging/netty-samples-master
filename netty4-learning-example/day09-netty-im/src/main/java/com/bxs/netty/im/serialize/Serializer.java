package com.bxs.netty.im.serialize;


import com.bxs.netty.im.serialize.impl.JSONSerializer;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 18:00
 */
public interface Serializer {
    Serializer DEFAULT = new JSONSerializer();

    /**
     * 序列化算法
     * @return
     */
    byte getSerializerAlgorithm();

    /**
     * java 对象转换成二进制
     */
    byte[] serialize(Object object);

    /**
     * 二进制转换成 java 对象
     */
    <T> T deserialize(Class<T> clazz, byte[] bytes);
}

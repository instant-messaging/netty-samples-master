package com.bxs.day07.protocol1.common;

/**
 * 说明：消息对象
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 17:58
 */
public class ProtocolMessage {

    private ProtocolHeader protocolHeader = new ProtocolHeader();
    private String body;

    public ProtocolHeader getProtocolHeader() {
        return protocolHeader;
    }

    public ProtocolMessage setProtocolHeader(ProtocolHeader protocolHeader) {
        this.protocolHeader = protocolHeader;
        return this;
    }

    public String getBody() {
        return body;
    }

    public ProtocolMessage setBody(String body) {
        this.body = body;
        return this;
    }
}

package com.bxs.netty.iot;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 14:12
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTests {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void HyperLogLogTest(){
        for (int i = 0;i<100000;i++){
            redisTemplate.opsForHyperLogLog().add("codehole","user"+i);
        }
    }
}

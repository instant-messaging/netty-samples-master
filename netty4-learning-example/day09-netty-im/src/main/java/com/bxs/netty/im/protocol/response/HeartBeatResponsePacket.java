package com.bxs.netty.im.protocol.response;

import com.bxs.netty.im.protocol.Packet;

import static com.bxs.netty.im.protocol.command.Command.HEARTBEAT_RESPONSE;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:59
 */
public class HeartBeatResponsePacket extends Packet {
    @Override
    public Byte getCommand() {
        return HEARTBEAT_RESPONSE;
    }
}

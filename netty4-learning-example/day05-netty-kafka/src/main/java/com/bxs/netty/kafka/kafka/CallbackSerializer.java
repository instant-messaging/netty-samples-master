package com.bxs.netty.kafka.kafka;

import com.bxs.netty.kafka.proto.Callback;
import org.apache.kafka.common.serialization.Serializer;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 15:13
 */
public class CallbackSerializer extends Adapter implements Serializer<Callback> {

    @Override
    public byte[] serialize(final String topic, final Callback data) {
        return data.toByteArray();
    }
}

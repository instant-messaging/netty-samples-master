package com.bxs.chat.domain.enums;

/**
 * 操作好友请求的枚举
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/18 10:13
 */
public enum OperatorFriendRequestTypeEnum {
    /**
     * 通过好友请求
     */
    PASS(1, "通过"),

    /**
     * 忽略好友请求
     */
    IGNORE(0, "忽略"),
    ;

    /**
     * 操作状态
     */
    public final Integer status;

    /**
     * 操作描述
     */
    public final String message;

    OperatorFriendRequestTypeEnum(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * 获得指定status的msg描述信息
     *
     * @param status 状态码
     * @return msg描述信息
     */
    public static String getMessageType(Integer status) {
        for (OperatorFriendRequestTypeEnum typeEnum : OperatorFriendRequestTypeEnum.values()) {
            if (typeEnum.status.equals(status)) {
                return typeEnum.message;
            }
        }
        return null;
    }
}

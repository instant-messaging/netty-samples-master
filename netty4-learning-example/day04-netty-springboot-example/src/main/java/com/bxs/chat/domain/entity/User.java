package com.bxs.chat.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 用户实体层
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 11:34
 */
@Entity
@Table(name = "user")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    /**
     * phone(用户手机号)
     */
    @Setter
    @Getter
    @Column(name = "phone", columnDefinition = "varchar(64) DEFAULT NULL COMMENT '用户手机号'")
    private String phone;

    /**
     * username(用户账号)
     */
    @Setter
    @Getter
    @Column(name = "username", columnDefinition = "varchar(64) DEFAULT NULL COMMENT '用户账号'")
    private String username;

    /**
     * password(用户密码)
     */
    @Setter
    @Getter
    @Column(name = "password", columnDefinition = "varchar(64) DEFAULT NULL COMMENT '用户密码'")
    private String password;

    /**
     * gender(用户性别)
     */
    @Setter
    @Getter
    @Column(name = "gender", columnDefinition = "int(4) DEFAULT NULL COMMENT '用户性别'")
    private String gender;

    /**
     * nickname(用户昵称)
     */
    @Setter
    @Getter
    @Column(name = "nickname", columnDefinition = "varchar(45) DEFAULT NULL COMMENT '用户昵称'")
    private String nickname;

    /**
     * avatar(用户头像)
     */
    @Setter
    @Getter
    @Column(name = "avatar_url", columnDefinition = "varchar(255) DEFAULT NULL COMMENT '用户头像'")
    private String avatarUrl;


    /**
     * 创建时间
     */
    @Setter
    @Getter
    @Column(name = "create_date", columnDefinition = "datetime DEFAULT NULL COMMENT '创建时间'")
    private LocalDateTime createDate;

    /**
     * 修改时间
     */
    @Setter
    @Getter
    @Column(name = "last_modify_date", columnDefinition = "datetime DEFAULT NULL COMMENT '修改时间'")
    private LocalDateTime lastModifyDate;


    /**
     * 状态
     */
    @Setter
    @Getter
    @Column(name = "state", columnDefinition = "datetime DEFAULT NULL COMMENT '删除标志位'")
    private Integer state;


    /**
     * 用户状态枚举
     */
    public enum UserState {

        /**
         * GENERAL：正常
         * FREEZE： 冻结
         */
        GENERAL(10),
        FREEZE(20);

        private Integer status;

        UserState(Integer status) {
            this.status = status;
        }

        public Integer getStatus() {
            return status;
        }
    }

    /**
     * 冻结用户
     */
    public void freeze() {
        Integer status = UserState.FREEZE.getStatus();
        this.state = status.equals(this.state) ? UserState.GENERAL.getStatus() : status;
        //this.addEvent(new StudentFreezeEvent(this.getId(),this.accountId));
    }
}

package com.bxs.netty.iot.store.message;

import com.bxs.netty.iot.common.message.GrozaMessageIdService;
import org.springframework.stereotype.Service;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 13:51
 */
@Service
public class MessageIdServiceImpl implements GrozaMessageIdService {
    @Override
    public int getNextMessageId() {
        return 0;
    }
}

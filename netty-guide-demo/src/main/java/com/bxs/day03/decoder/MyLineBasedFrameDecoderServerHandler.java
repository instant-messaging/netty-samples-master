package com.bxs.day03.decoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * My LineBasedFrameDecoder ServerHandler。
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 10:16
 */
public class MyLineBasedFrameDecoderServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        // 接收msg消息，此处已经无需解码了
        System.out.println("Client -> Server: " + msg);
    }
}

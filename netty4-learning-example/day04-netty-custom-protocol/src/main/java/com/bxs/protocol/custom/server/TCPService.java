package com.bxs.protocol.custom.server;

import com.bxs.protocol.custom.common.codec.MessageDecoder;
import com.bxs.protocol.custom.common.codec.MessageEncoder;
import com.bxs.protocol.custom.server.handler.HeartBeatResponseHandler;
import com.bxs.protocol.custom.server.handler.MessageHandler;
import com.bxs.protocol.custom.server.handler.ResponseBiz;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


/**
 * TCPService Netty服务器配置
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/25 11:12
 */
@Component
@Order(1)
public class TCPService implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(TCPService.class);

    @Value("${netty.port}")
    private int port;

    @Override
    public void run(String... args) throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            // 绑定线程池
            bootstrap.group(bossGroup, workerGroup)
                    // 指定使用的channel
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 20)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    // 绑定客户端连接时候触发操作
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            logger.info("收到新的客户端连接: {}", ch.toString());
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast(new MessageDecoder());
                            pipeline.addLast(new MessageEncoder());
                            pipeline.addLast(new HeartBeatResponseHandler());
                            //pipeline.addLast(new IdleStateHandler(3,5,7, TimeUnit.SECONDS))
                            pipeline.addLast(new MessageHandler());
                            pipeline.addLast(new ResponseBiz());
                        }
                    });

            // 服务器异步创建绑定
            ChannelFuture future = bootstrap.bind(port).sync();
            if (future.isSuccess()) {
                logger.info("TCP 服务器启动成功, 监听端口：{}", port);
            }

            logger.info(TCPService.class + " TCP服务监听关闭： " + future.channel().localAddress());
            // 关闭服务器通道 ~~~~~会阻塞主进程，一般用于单元测试
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully().syncUninterruptibly();
            workerGroup.shutdownGracefully().syncUninterruptibly();
        }
    }
}

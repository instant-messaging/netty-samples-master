package com.bxs.day08.websocket01.business;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/28 13:37
 */
public interface Constant {

    /**
     * 存放在浏览器Cookie中的关键字
     */
    String TOKEN = "TOKEN";

    /**
     * 存放在浏览器UserInfo中的关键字
     */
    String USERINFO = "USERINFO";
}

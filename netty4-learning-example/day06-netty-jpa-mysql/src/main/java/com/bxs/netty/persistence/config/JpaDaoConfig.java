package com.bxs.netty.persistence.config;

import com.bxs.netty.persistence.annotation.SqlDao;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 16:58
 */
@Configuration
@EnableAutoConfiguration
@SqlDao
@EnableTransactionManagement
@EnableJpaRepositories("com.bxs.netty.persistence.domain.repository")
@EntityScan("com.bxs.netty.persistence.domain.entity")
public class JpaDaoConfig {
}

package com.bxs.day03.common;

/**
 * Message Type.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 18:00
 */
public enum MessageType {
    EMGW_LOGIN_REQ((byte) 0x00),
    EMGW_LOGIN_RES((byte) 0x01);

    private byte value;

    public byte getValue() {
        return value;
    }

    private MessageType(byte value) {
        this.value = value;
    }
}

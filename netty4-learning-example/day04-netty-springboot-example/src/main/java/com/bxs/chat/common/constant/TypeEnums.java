package com.bxs.chat.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 发送消息枚举类型
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 11:57
 */
@AllArgsConstructor
@Getter
public enum TypeEnums {

    REGISTER("注册"),
    SINGLE_SENDING("给单个人发送消息"),
    GROUP_SENDING("群发"),
    FILE_MSG_SINGLE_SENDING("给单个人发送文件"),
    FILE_MSG_GROUP_SENDING("群发文件");

    private String type;
}

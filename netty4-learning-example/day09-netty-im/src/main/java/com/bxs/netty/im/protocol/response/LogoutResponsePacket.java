package com.bxs.netty.im.protocol.response;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;

import static com.bxs.netty.im.protocol.command.Command.LOGOUT_RESPONSE;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 9:02
 */
@Data
public class LogoutResponsePacket extends Packet {

    private boolean success;

    private String reason;


    @Override
    public Byte getCommand() {
        return LOGOUT_RESPONSE;
    }
}

package com.bxs.netty.rpc.api;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/9 11:48
 */
public interface IRpcHelloService {

    String hello(String name);
}

package com.bxs.netty.rpc.protocol;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/9 11:46
 */
@Data
public class InvokerProtocol implements Serializable {
    /**
     * 类名
     */
    private String className;
    /**
     * 函数名称
     */
    private String methodName;
    /**
     * 形参列表
     */
    private Class<?>[] parames;
    /**
     * 实参列表
     */
    private Object[] values;
}

package com.bxs.protocol.custom.common.entity;

import lombok.Builder;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 10:41
 */
@Builder
public class Header implements Serializable {

    private Integer length;
    private Integer code;
    private Long sessionId;
    private Byte type;
    private Byte priority;
    private Map<String,Object> attachment;

    public Header() {
    }

    public Header(Integer length, Integer code, Long sessionId, Byte type, Byte priority, Map<String, Object> attachment) {
        this.length = length;
        this.code = code;
        this.sessionId = sessionId;
        this.type = type;
        this.priority = priority;
        this.attachment = attachment;
    }

    public Integer getLength() {
        return length;
    }

    public Header setLength(Integer length) {
        this.length = length;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public Header setCode(Integer code) {
        this.code = code;
        return this;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public Header setSessionId(Long sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public Byte getType() {
        return type;
    }

    public Header setType(Byte type) {
        this.type = type;
        return this;
    }

    public Byte getPriority() {
        return priority;
    }

    public Header setPriority(Byte priority) {
        this.priority = priority;
        return this;
    }

    public Map<String, Object> getAttachment() {
        return attachment;
    }

    public Header setAttachment(Map<String, Object> attachment) {
        this.attachment = attachment;
        return this;
    }

    @Override
    public String toString() {
        return "Header{" +
                "length=" + length +
                ", code=" + code +
                ", sessionId=" + sessionId +
                ", type=" + type +
                ", priority=" + priority +
                ", attachment=" + attachment +
                '}';
    }
}

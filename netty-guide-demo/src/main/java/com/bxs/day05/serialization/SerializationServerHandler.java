package com.bxs.day05.serialization;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * SerializationServer Handler.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 11:07
 */
public class SerializationServerHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object obj) throws Exception {
        if (obj instanceof SerializationBean) {
            SerializationBean user = (SerializationBean) obj;
            ctx.writeAndFlush(user);
            System.out.println("Client -> Server: " + user);
        }
    }
}

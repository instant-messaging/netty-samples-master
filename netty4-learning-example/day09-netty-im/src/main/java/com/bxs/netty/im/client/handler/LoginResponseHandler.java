package com.bxs.netty.im.client.handler;

import com.bxs.netty.im.protocol.response.LoginResponsePacket;
import com.bxs.netty.im.session.Session;
import com.bxs.netty.im.utils.SessionUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 10:06
 */
public class LoginResponseHandler extends SimpleChannelInboundHandler<LoginResponsePacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LoginResponsePacket responsePacket) {
        String userId = responsePacket.getUserId();
        String userName = responsePacket.getUserName();

        if (responsePacket.isSuccess()) {
            System.out.println("[" + userName + "]登录成功，userId 为: " + responsePacket.getUserId());
            SessionUtil.bindSession(new Session(userId, userName), ctx.channel());
        } else {
            System.out.println("[" + userName + "]登录失败，原因：" + responsePacket.getReason());
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        System.out.println("客户端连接被关闭!");
    }
}

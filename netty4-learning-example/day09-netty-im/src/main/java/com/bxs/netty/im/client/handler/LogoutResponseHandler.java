package com.bxs.netty.im.client.handler;

import com.bxs.netty.im.protocol.response.LogoutResponsePacket;
import com.bxs.netty.im.utils.SessionUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 10:07
 */
public class LogoutResponseHandler extends SimpleChannelInboundHandler<LogoutResponsePacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LogoutResponsePacket responsePacket) {
        SessionUtil.unbindSession(ctx.channel());
    }
}

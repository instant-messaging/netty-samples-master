package com.bxs.netty.rpc.api;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/9 11:48
 */
public interface IRpcService {

    /** 加 */
    public int add(int a,int b);

    /** 减 */
    public int sub(int a,int b);

    /** 乘 */
    public int mult(int a,int b);

    /** 除 */
    public int div(int a,int b);
}

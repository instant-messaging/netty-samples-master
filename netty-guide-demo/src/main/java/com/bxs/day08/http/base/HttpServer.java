package com.bxs.day08.http.base;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * HTTP Server.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 14:55
 */
public class HttpServer {
    private static int DEFAULT_PORT = 8080;

    private final int port;

    public HttpServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) {
        int port;

//        if (args.length != 1) {
//            System.err.println("Usage: " + HttpServer.class.getSimpleName() + " <port>");
//            return;
//        }

        try {
            port = Integer.parseInt(args[0]);
            System.out.println("start server with port:" + port);
        } catch (RuntimeException ex) {
            port = DEFAULT_PORT;
        }

        new HttpServer(port).run();
    }

    public void run() {
        // 多线程事件循环器
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            // 启动NIO服务的引导程序类
            ServerBootstrap bootstrap = new ServerBootstrap();
            // 设置EventLoopGroup
            bootstrap.group(bossGroup, workerGroup)
                    // 指明新的Channel的类型
                    .channel(NioServerSocketChannel.class)
                    // 指定ChannelHandler
                    .childHandler(new HttpServerChannelInitializer())
                    // 设置的ServerChannel的一些选项
                    .option(ChannelOption.SO_BACKLOG, 128)
                    // 设置的ServerChannel的子Channel的选项
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            // 绑定端口，开始接收进来的连接
            ChannelFuture future = bootstrap.bind(port).sync();

            System.out.println("HttpServer已启动，端口：" + port);

            // 等待服务器 socket 关闭 。
            // 在这个例子中，这不会发生，但你可以优雅地关闭你的服务器。
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // 优雅的关闭
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}

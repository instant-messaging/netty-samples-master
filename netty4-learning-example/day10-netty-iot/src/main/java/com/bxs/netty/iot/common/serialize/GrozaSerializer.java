package com.bxs.netty.iot.common.serialize;

import com.bxs.netty.iot.common.session.SessionStore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * 继承RedisTemplate序列化接口,自定义序列化类
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/8 11:18
 */
public class GrozaSerializer implements RedisSerializer<Object> {
    @Override
    public byte[] serialize(Object o) throws SerializationException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.toJson(o).getBytes();
    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializationException {
        String s = new String(bytes);
        Gson gson = new Gson();
        return gson.fromJson(s, SessionStore.class);
    }
}

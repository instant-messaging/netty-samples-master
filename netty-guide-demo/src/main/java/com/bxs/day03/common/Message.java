package com.bxs.day03.common;

/**
 * Message.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 17:57
 */
public class Message {

    private MessageHeader messageHeader = new MessageHeader();
    
    private String body;

    public MessageHeader getMessageHeader() {
        return messageHeader;
    }

    public Message setMessageHeader(MessageHeader messageHeader) {
        this.messageHeader = messageHeader;
        return this;
    }

    public String getBody() {
        return body;
    }

    public Message setBody(String body) {
        this.body = body;
        return this;
    }
}

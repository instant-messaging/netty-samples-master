package com.bxs.protocol.custom.client.handler;

import com.bxs.protocol.custom.common.entity.Body;
import com.bxs.protocol.custom.common.entity.Header;
import com.bxs.protocol.custom.common.entity.Message;
import com.bxs.protocol.custom.common.enums.MessageType;
import com.bxs.protocol.custom.common.util.ByteUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 12:34
 */
public class LoginRequestHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Header header = Header.builder().priority((byte) 1).sessionId(23L).code(1).type(MessageType.HANDSHAKE_REQUEST.getType().byteValue()).build();
        Map<String, Object> attachment = new HashMap<>();
        attachment.put("dev", "2534");
        attachment.put("name", "张三");
        header.setAttachment(attachment);

        Body body = new Body();
        body.setPayload("welcom to shenzhen");

        Message message = Message.builder().header(header).body(body).build();

        int len = ByteUtil.calculateLenth(message);
        message.getHeader().setLength(len);

        ctx.writeAndFlush(message);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Message message = (Message) msg;
        if (message != null && message.getHeader().getType() == MessageType.HANDSHAKE_RESPONSE.getType().byteValue()) {
            System.out.println("kkkkkkkkkkkkkkk");
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
}

package com.bxs.day07.protocol1.codec;

import com.bxs.day07.protocol1.common.ProtocolHeader;
import com.bxs.day07.protocol1.common.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.nio.charset.Charset;

/**
 * 说明：编码器
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 18:07
 */
public class ProtocolEncoder extends MessageToByteEncoder<ProtocolMessage> {

    /**
     *
     */
    public ProtocolEncoder() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param outboundMessageType
     */
    public ProtocolEncoder(Class<? extends ProtocolMessage> outboundMessageType) {
        super(outboundMessageType);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param preferDirect
     */
    public ProtocolEncoder(boolean preferDirect) {
        super(preferDirect);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param outboundMessageType
     * @param preferDirect
     */
    public ProtocolEncoder(Class<? extends ProtocolMessage> outboundMessageType, boolean preferDirect) {
        super(outboundMessageType, preferDirect);
        // TODO Auto-generated constructor stub
    }

    /** (non-Javadoc)
     * @see io.netty.handler.codec.MessageToByteEncoder#encode(io.netty.channel.ChannelHandlerContext, java.lang.Object, io.netty.buffer.ByteBuf)
     */
    @Override
    protected void encode(ChannelHandlerContext ctx, ProtocolMessage message, ByteBuf out) throws Exception {
        if (message == null | message.getProtocolHeader() == null) {
            throw new Exception("The encode message is null");
        }
        ProtocolHeader header = message.getProtocolHeader();
        String body = message.getBody();
        byte[] bodyBytes = body.getBytes(Charset.forName("utf-8"));
        int bodySize = bodyBytes.length;

        out.writeByte(header.getMagic());
        out.writeByte(header.getType());
        out.writeShort(header.getReserve());
        out.writeShort(header.getSn());
        out.writeInt(bodySize);
        out.writeBytes(bodyBytes);
    }
}

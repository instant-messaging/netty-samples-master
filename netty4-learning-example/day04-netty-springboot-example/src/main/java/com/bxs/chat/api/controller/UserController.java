package com.bxs.chat.api.controller;

import com.bxs.chat.common.annotation.ResponseResult;
import com.bxs.chat.common.constant.Constant;
import com.bxs.chat.service.ChatService;
import com.bxs.chat.service.UserService;
import com.bxs.chat.common.exception.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户前端展示层Controller
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 14:38
 */
@Slf4j
@RestController
@RequestMapping("/user")
@Api(value = "用户模块")
public class UserController {

    private final UserService userService;
    private final ChatService chatService;

    public UserController(UserService userService, ChatService chatService) {
        this.userService = userService;
        this.chatService = chatService;
    }

    @ApiOperation(value = "接口测试")
    @GetMapping(path = "/test")
    public String test(){
        return "isOk";
    }

    @ApiOperation(value = "用户登录")
    @PostMapping(path = "/login")
    public Result login(@RequestParam(value = "username") String username, @RequestParam(value = "password")String password, HttpServletRequest request) {
        log.info("用户:[{}]正在登录聊天室",username);
        return userService.login(username,password,request);
    }

    @ApiOperation(value = "通过id获取用户信息")
    @GetMapping(path = "/userInfo")
    public Result getUserInfoById(HttpServletRequest request){
        Long userId = (Long) request.getSession().getAttribute(Constant.USER_TOKEN);
        log.info("用户:[{}]正在获取用户信息",userId);
        return userService.getUserInfoById(userId);
    }
}

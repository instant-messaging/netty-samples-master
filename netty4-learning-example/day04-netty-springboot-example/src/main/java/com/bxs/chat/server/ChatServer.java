package com.bxs.chat.server;

import com.bxs.chat.server.init.WSServerInitialer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;


/**
 * 使用 Netty 作为通信基础的 Websocket 长连接示例
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 10:08
 */
@Component
public class ChatServer {
    private static Logger logger = LoggerFactory.getLogger(ChatServer.class);

    private static final int RECVBYTE_ALLOCATOR_SIZE = 592048;

    /**
     * 通过springboot读取静态资源,实现netty配置文件的读写
     */
    @Value("${server.websocket.port:18080}")
    private Integer port;
    /**
     * 主线程组
     */
    private EventLoopGroup bossGroup;
    /**
     * 从线程组
     */
    private EventLoopGroup workerGroup;

    private ChannelFuture future;

//    public NettyServer(Integer port) {
//        this.port = port;
//    }

    @PostConstruct
    public void start() throws InterruptedException {
        logger.info("Starting Server");
        // 创建 boss 线程组 用于服务端接受客户端的连接
        bossGroup = new NioEventLoopGroup();
        // 创建 worker 线程组 用于进行 SocketChannel 的数据读写
        workerGroup = new NioEventLoopGroup();
        // 创建 ServerBootstrap 对象
        ServerBootstrap bootstrap = new ServerBootstrap();
        // 绑定线程池, 设置使用的EventLoopGroup
        bootstrap.group(bossGroup, workerGroup)
                // 指定使用的channel, 设置要被实例化的为 NioServerSocketChannel 类
                .channel(NioServerSocketChannel.class)
                // TCP参数配置 握手字符串长度设置
                .option(ChannelOption.SO_BACKLOG, 1024)
                // 设置TCP NO_DELAY 算法 尽量发送大文件包
                .option(ChannelOption.TCP_NODELAY, true)
                // 开启心跳模式
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                // 配置固定长度接收缓存内存分配
                .childOption(ChannelOption.RCVBUF_ALLOCATOR, new FixedRecvByteBufAllocator(RECVBYTE_ALLOCATOR_SIZE))
                // 设置 NioServerSocketChannel 的处理器
                .handler(new LoggingHandler(LogLevel.INFO))
                // 设置连入服务端的 Client 的 SocketChannel 的处理器
                .childHandler(new WSServerInitialer());

        logger.info("Netty WebSocket Server 服务启动完成: ws://localhost:{}/websocket", port);
        Long beginTime = System.currentTimeMillis();

        // 绑定端口，并同步等待成功，即启动服务端
        future = bootstrap.bind(port).sync();

        Long endTime = System.currentTimeMillis();
        logger.info("服务器启动完成，耗时:[{}]毫秒,已经在端口：[{}]进行阻塞等待", endTime - beginTime, port);

        logger.info("Server started!");
        logger.info(ChatServer.class + " 启动正在监听： " + future.channel().localAddress());
    }

    @PreDestroy
    public void shutdown() {
        logger.info(ChatServer.class + " netty服务监听关闭： " + future.channel().localAddress());
        try {
            // 监听服务端关闭，并阻塞等待
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // 优雅关闭两个 EventLoopGroup 对象
            workerGroup.shutdownGracefully().syncUninterruptibly();
            bossGroup.shutdownGracefully().syncUninterruptibly();
        }
        logger.info("server stopped!");
    }
}

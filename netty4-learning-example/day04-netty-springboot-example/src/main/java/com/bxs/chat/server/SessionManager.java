package com.bxs.chat.server;

import com.bxs.chat.common.util.StringUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelId;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户会话管理
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/18 9:40
 */
@Component
public class SessionManager {

    private static final Map<String, Channel> userIdChannelMap = new ConcurrentHashMap<>();

    private static final Map<String, ChannelGroup> groupIdChannelGroupMap = new ConcurrentHashMap<>();

    /**
     * 对于每一个会话将用户id作为一个key
     */
    private static final AttributeKey<String> USER_ID = AttributeKey.newInstance("phone");

    /**
     * channel连接池
     */
    private static ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /**
     * 用户连接信息保存池
     */
    private final Map<String, ChannelId> channelMap = new ConcurrentHashMap<>();

    /**
     * 用户离线或者主动断开时出发此操作
     */
    private ChannelFutureListener remover = (future) -> {
        String userId = future.channel().attr(USER_ID).get();
        if (!StringUtil.isEmpty(userId)) {
            System.out.println("用户: " + userId + "断开连接");
            channelMap.remove(userId);
        }
    };

    /**
     * 缓存用户连接信息
     *
     * @param senderId 发送人的id
     * @param channel  连接信息
     */
    public void cacheSession(String senderId, Channel channel) {
        boolean add = channelGroup.add(channel);
        if (add) {
            channel.closeFuture().addListener(remover);
            channelMap.put(senderId, channel.id());
        }
    }

    /**
     * 根据id获得该用户的channel连接信息
     *
     * @param senderId 用户id
     * @return channel连接信息
     */
    public Channel getSession(String senderId) {
        ChannelId channelId = channelMap.get(senderId);
        return channelId == null ? null : channelGroup.find(channelId);
    }
}

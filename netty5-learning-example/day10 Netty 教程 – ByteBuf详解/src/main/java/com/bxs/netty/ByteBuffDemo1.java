package com.bxs.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteBuffer;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/25 16:40
 */
public class ByteBuffDemo1 {

    public static void main(String[] args) {
        ByteBuf buf = Unpooled.buffer(10);
        // 扩容算法稍后讲解
        buf.writeBytes("鏖战八方QQ群391619659".getBytes());
        System.out.println(buf);
        System.out.println("//////////////////////////////////////////无耻的分割线//////////////////////////////////////////");
        ByteBuffer buffer = ByteBuffer.allocate(10);

        buffer.put("鏖战八方QQ群391619659".getBytes());
        System.out.println(buffer);

    }
}

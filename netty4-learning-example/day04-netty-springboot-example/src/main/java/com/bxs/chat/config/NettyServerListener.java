//package com.bxs.chat.config;
//
//import com.bxs.chat.service.netty.NettyServer;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//
//import javax.servlet.ServletContextEvent;
//import javax.servlet.ServletContextListener;
//import java.util.Calendar;
//
///**
// * @Author: Mr.Lu
// * @Date: 2020/9/17 18:04
// */
//public class NettyServerListener implements ServletContextListener {
//
//    private static final Logger logger = LoggerFactory.getLogger(NettyServerListener.class);
//    private NettyServer nettyServer;
//
//    /**
//     * 通过springboot读取静态资源,实现netty配置文件的读写
//     */
//    @Value("${server.websocket.port:18080}")
//    private Integer port;
//
//    @Override
//    public void contextInitialized(ServletContextEvent sce) {
//        nettyServer =  new NettyServer(8091);
//        try {
//            nettyServer.start();
//        } catch (Exception e) {
//            logger.error("netty server abnormal startup {netty服务启动异常}", e);
//        }
//    }
//
//    /**
//     * 销毁
//     *
//     * @param sce
//     */
//    @Override
//    public void contextDestroyed(ServletContextEvent sce) {
//        logger.info("destroy() of netty"+ Calendar.getInstance().getTime()+"  flag:"+(null==nettyServer));
//        nettyServer.shutdown();
//    }
//}

package com.bxs.netty.tomcat.servlet;

import com.bxs.netty.tomcat.http.GPRequest;
import com.bxs.netty.tomcat.http.GPResponse;
import com.bxs.netty.tomcat.http.GPServlet;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/9 11:39
 */
public class FirstServlet extends GPServlet {
    @Override
    public void doGet(GPRequest request, GPResponse response) throws Exception {
        this.doPost(request, response);
    }

    @Override
    public void doPost(GPRequest request, GPResponse response) throws Exception {
        response.write("This is First Serlvet");
    }
}

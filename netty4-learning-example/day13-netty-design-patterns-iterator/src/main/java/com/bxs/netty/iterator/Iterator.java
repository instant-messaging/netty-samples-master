package com.bxs.netty.iterator;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:08
 */
public interface Iterator<T> {

    boolean hasNext();

    T next();
}

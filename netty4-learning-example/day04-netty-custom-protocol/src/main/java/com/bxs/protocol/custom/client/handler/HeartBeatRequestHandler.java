package com.bxs.protocol.custom.client.handler;

import com.bxs.protocol.custom.common.constant.NettyConstant;
import com.bxs.protocol.custom.common.entity.Header;
import com.bxs.protocol.custom.common.entity.Message;
import com.bxs.protocol.custom.common.enums.MessageType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 12:33
 */
public class HeartBeatRequestHandler extends ChannelInboundHandlerAdapter {
    private static final Logger log = LoggerFactory.getLogger(HeartBeatRequestHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Message message = (Message) msg;
        log.debug("heartbeatReqhandler receive msg:{}", message.toString());
        if (message != null && message.getHeader().getType() == MessageType.HANDSHAKE_RESPONSE.getType().byteValue()) {
            ctx.executor().scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    Header header = Header.builder()
                            .sessionId(NettyConstant.LOGIN_SESSION_ID)
                            .priority(NettyConstant.PRIORITY)
                            .code(NettyConstant.CODE)
                            .type(MessageType.HEARTBEAT_REQUEST.getType().byteValue())
                            .length(NettyConstant.HEADER_LENGTH)
                            .build();

                    Message message = Message.builder().header(header).build();
                    log.debug("clent send heartbeat***");
                    ctx.channel().writeAndFlush(message);
                }
            }, 0, 50, TimeUnit.SECONDS);
        } else if (message != null && message.getHeader().getType() == MessageType.HEARTBEAT_RESPONSE.getType().byteValue()) {
            log.debug("client receive heartbeat msg:{}", message.toString());
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println(cause);
        ctx.close();
    }
}

package com.bxs.netty.im.protocol.request;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;

import static com.bxs.netty.im.protocol.command.Command.QUIT_GROUP_REQUEST;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:52
 */
@Data
public class QuitGroupRequestPacket extends Packet {

    private String groupId;

    @Override
    public Byte getCommand() {
        return QUIT_GROUP_REQUEST;
    }
}

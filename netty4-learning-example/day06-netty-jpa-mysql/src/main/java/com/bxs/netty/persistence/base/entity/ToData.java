package com.bxs.netty.persistence.base.entity;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:02
 */
public interface ToData<T> {

    T toData();
}

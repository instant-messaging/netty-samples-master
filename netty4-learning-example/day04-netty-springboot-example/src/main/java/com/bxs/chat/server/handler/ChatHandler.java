package com.bxs.chat.server.handler;

import com.alibaba.fastjson.JSONObject;
import com.bxs.chat.common.constant.Constant;
import com.bxs.chat.common.exception.Result;
import com.bxs.chat.server.SessionManager;
import com.bxs.chat.service.ChatService;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import java.util.Arrays;

import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

/**
 * 实际进行处理消息的Handler 发送消息
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 11:37
 */
@Slf4j
@Component
@ChannelHandler.Sharable
public class ChatHandler extends SimpleChannelInboundHandler<Object> {

    @Resource
    private ChatService chatService;

    @Resource
    private SessionManager sessionManager;

    /**
     * 读取连接消息并对消息进行处理
     * @param ctx 处理上下文
     * @param message WebSocket组件
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object message) throws Exception {
        // 传统的HTTP接入 需要进行协议升级
        if (message instanceof FullHttpRequest) {
            handleHttpRequest(ctx, (FullHttpRequest) message);
        }
        // WebSocket接入
        if (message instanceof WebSocketFrame) {
            handleWebSocketFrame(ctx, (WebSocketFrame) message);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    /**
     * 客户端断开连接之后触发
     * @param ctx 处理和上下文
     * @throws Exception 捕获异常
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        chatService.remove(ctx);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;

            if (event.state() == IdleState.READER_IDLE) {
                System.out.println("读空闲");
            } else if (event.state() == IdleState.WRITER_IDLE) {
                System.out.println("写空闲");
            } else if (event.state() == IdleState.ALL_IDLE) {
                System.out.println("等待超时，清理服务器无用资源");
                ctx.channel().closeFuture().sync();
            }
        }
    }

    /**
     * 出现移异常后触发
     * @param ctx 处理上下文
     * @param cause 异常类
     * @throws Exception 异常
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
        System.out.println(cause.getMessage() + "===" + cause.getLocalizedMessage());
        System.out.println(Arrays.toString(cause.getStackTrace()));
    }


    /**
     * 消息处理失败 发送一个失败请求 应答客户端
     * @param context 处理上下文
     * @param request 请求
     * @param defaultFullHttpResponse 默认的Http响应
     */
    private void sendHttpResponse(ChannelHandlerContext context, FullHttpRequest request, DefaultFullHttpResponse defaultFullHttpResponse) {
        if (defaultFullHttpResponse.status().code() != HttpResponseStatus.OK.code()) {
            ByteBuf buf = Unpooled.copiedBuffer(defaultFullHttpResponse.status().toString(), CharsetUtil.UTF_8);
            defaultFullHttpResponse.content().writeBytes(buf);
            buf.release();
        }
        // 如果长连接好存在 关闭长连接
        boolean keepLive = HttpUtil.isKeepAlive(request);
        ChannelFuture future = context.channel().writeAndFlush(request);
        if (!keepLive) {
            future.addListener(ChannelFutureListener.CLOSE);
        }
    }

    /**
     * Http协议和转换
     * @param context 处理上下文
     * @param request 消息请求
     */
    private void handleHttpRequest(ChannelHandlerContext context, FullHttpRequest request) {
        // 如果HTTP解码失败，返回HHTP异常
        if (!request.decoderResult().isSuccess()) {
            sendHttpResponse(context, request, new DefaultFullHttpResponse(HTTP_1_1, BAD_REQUEST));
            return;
        }
        // 协议升级, 构造握手响应返回
        WebSocketServerHandshakerFactory factory = new WebSocketServerHandshakerFactory("ws:/" + context.channel() + "/websocket", null, false);
        WebSocketServerHandshaker handsShaker = factory.newHandshaker(request);
        // 存储握手信息
        Constant.webSocketHandshakerMap.put(context.channel().id().asLongText(), handsShaker);
        if (handsShaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(context.channel());
        }else {
            // 表示握手成功
            handsShaker.handshake(context.channel(), request);
            log.info("Http-websocket握手协议升级成功啦");
        }
    }


    private void handleWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame frame) {
        // 判断是否是关闭链路的指令
        if (frame instanceof CloseWebSocketFrame) {
            WebSocketServerHandshaker handsShaker = Constant.webSocketHandshakerMap.get(ctx.channel().id().asLongText());
            if (null == handsShaker) {
                sendErrorMessage(ctx,"该用户已经离线或者不存在该连接");
            }else {
                handsShaker.close(ctx.channel(), ((CloseWebSocketFrame) frame).retain());
            }
            return;
        }
        // 判断是否是Ping消息
        if (frame instanceof PingWebSocketFrame) {
            ctx.channel().write(new PongWebSocketFrame(frame.content().retain()));
            return;
        }
        // 本例程仅支持文本消息，不支持二进制消息
        if (!(frame instanceof TextWebSocketFrame)) {
            sendErrorMessage(ctx,"不支持二进制文件");
        }
        // 返回应答消息
        String request = ((TextWebSocketFrame) frame).text();
        JSONObject params = null;
        try {
            params = JSONObject.parseObject(request);
            log.info("收到服务器消息：[{}]",params);
        }catch (Exception e){
            sendErrorMessage(ctx, "JSON字符串转换出错！");
            log.error("参数转换异常");
        }
        if (null == params) {
            sendErrorMessage(ctx, "参数为空！");
            log.warn("参数为空");
            return;
        }
        String messageType = (String) params.get("type");
        switch (messageType) {
            // 注册
            case "REGISTER":
                chatService.register(params,ctx);
                break;
            // 发送消息给单个人
            case "SINGLE_SENDING":
                chatService.sendOne(params, ctx);
                break;
            // 群发消息
            case "GROUP_SENDING":
                chatService.sendGroup(params, ctx);
                break;
            // 发送文件给单个人
            case "FILE_MSG_SINGLE_SENDING":
                //chatService.FileMsgSingleSend(param, ctx);
                break;
            // 群发文件
            case "FILE_MSG_GROUP_SENDING":
                //chatService.FileMsgGroupSend(param, ctx);
                break;
            default:
                //chatService.typeError(ctx);
                break;
        }
    }

    /**
     * 出现不可抗拒因素发送错误消息给客户端
     * @param context 处理上下文
     * @param message 消息文字
     */
    public void sendErrorMessage(ChannelHandlerContext context,String message){
        String result = Result.failure().setMessage(message).toString();
        context.channel().writeAndFlush(new TextWebSocketFrame(result));
    }
}

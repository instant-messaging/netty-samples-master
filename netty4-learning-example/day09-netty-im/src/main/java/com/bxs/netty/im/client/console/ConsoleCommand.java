package com.bxs.netty.im.client.console;

import io.netty.channel.Channel;

import java.util.Scanner;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 9:52
 */
public interface ConsoleCommand {
    void exec(Scanner scanner, Channel channel);
}

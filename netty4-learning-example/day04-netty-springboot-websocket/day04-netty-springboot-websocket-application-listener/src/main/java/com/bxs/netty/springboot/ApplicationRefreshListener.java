package com.bxs.netty.springboot;

import com.bxs.netty.springboot.websocket.runner.WebSocketServer;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;

/**
 * ApplicationEvent相当于一个事件，所有自定义事件都需要继承这个抽象类。在Eclipse中Ctrl+Shift+H调用类的层次结构列表，可以看到如下
 * Application下抽象子类ApplicationContextEvent的下面有4个已经实现好的事件
 * ContextClosedEvent（容器关闭时）
 * ContextRefreshedEvent（容器刷新是）
 * ContextStartedEvent（容器启动时候）
 * ContextStoppedEvent（容器停止的时候）
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/25 15:40
 */
@Component
public class ApplicationRefreshListener implements ApplicationListener<ApplicationContextEvent> {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationRefreshListener.class);

    @SneakyThrows
    @Override
    public void onApplicationEvent(ApplicationContextEvent event) {
        if (event instanceof ContextRefreshedEvent) {
            logger.debug("WebSocket Server 启动 .......");
            // ApplicationContext初始化或者刷新时触发该事件
            // 获取server实例
            WebSocketServer.getInstance().start();
        } else if (event instanceof ContextClosedEvent) {
            logger.debug("WebSocket Server 停止 .......");
            // ApplicationContext关闭时触发该事件
            // 销毁netty线程
            WebSocketServer.getInstance().destory();
        } else {
            logger.debug("有其它事件发生:" + event.getClass().getName());
        }
    }
}

package com.bxs.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteBuffer;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/25 16:25
 */
public class ByteBuffDemo {

    public static void main(String[] args) {
        ByteBuf buf = Unpooled.buffer(10);
        // 扩容算法稍后讲解
        buf.writeBytes("鏖战八方QQ群391619659".getBytes());
        System.out.println("Netty：" + buf);
        byte[] by = new byte[buf.readableBytes()];
        buf.readBytes(by);
        System.out.println("Netty：" + new String(by));

        System.out.println("//////////////////////////////////////////无耻的分割线//////////////////////////////////////////");
        ByteBuffer bf1 = ByteBuffer.allocate(100);
        bf1.put("鏖战八方QQ群391619659".getBytes());
        System.out.println("JDK："+bf1);
        System.out.println("当前指针：" + bf1.position());
        byte[] by1 = new byte[bf1.remaining()];
        // What's 居然是74
        System.out.println(by1.length);
        bf1.get(by1);
        // 居然是空的
        System.out.println("未使用flip："+new String(by1));
        System.out.println("//////////////////////////////////////////无耻的分割线//////////////////////////////////////////");
        ByteBuffer bf2 = ByteBuffer.allocate(100);
        bf2.put("鏖战八方QQ群391619659".getBytes());
        System.out.println("JDK："+bf2);
        System.out.println("当前指针：" + bf2.position());
        bf2.flip();
        byte[]  by2 = new byte[bf2.remaining()];
        // 是26了
        System.out.println(by2.length);
        bf2.get(by2);
        // 拿到了
        System.out.println("使用flip："+new String(by2));
    }
}

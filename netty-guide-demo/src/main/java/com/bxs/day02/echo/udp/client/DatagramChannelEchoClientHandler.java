package com.bxs.day02.echo.udp.client;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.socket.DatagramPacket;
import io.netty.util.CharsetUtil;

/**
 * DatagramChannel Echo Client Handler.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 16:39
 */
public class DatagramChannelEchoClientHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        // 从管道读消息
        // 转为DatagramPacket类型
        DatagramPacket packet = (DatagramPacket) msg;
        // 转为字符串
        String m = packet.content().toString(CharsetUtil.UTF_8);
        System.out.println( "echo :" + m);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {

        // 当出现异常就关闭连接
        cause.printStackTrace();
        ctx.close();
    }
}

package com.bxs.netty.chain.quest;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 14:26
 */
public enum RequestType {
    PRODUCT_MANGER, SOFTWARE_ENGINNER, QA_ENGINNER
}

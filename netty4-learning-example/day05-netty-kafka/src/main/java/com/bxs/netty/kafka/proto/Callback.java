// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: helloworld.proto

package com.bxs.netty.kafka.proto;

/**
 * Protobuf type {@code helloworld.Callback}
 */
public  final class Callback extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:helloworld.Callback)
    CallbackOrBuilder {
private static final long serialVersionUID = 0L;
  // Use Callback.newBuilder() to construct.
  private Callback(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private Callback() {
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private Callback(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!parseUnknownFieldProto3(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            com.bxs.netty.kafka.proto.KafkaEventOne.Builder subBuilder = null;
            if (eventTypeCase_ == 1) {
              subBuilder = ((com.bxs.netty.kafka.proto.KafkaEventOne) eventType_).toBuilder();
            }
            eventType_ =
                input.readMessage(com.bxs.netty.kafka.proto.KafkaEventOne.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom((com.bxs.netty.kafka.proto.KafkaEventOne) eventType_);
              eventType_ = subBuilder.buildPartial();
            }
            eventTypeCase_ = 1;
            break;
          }
          case 18: {
            com.bxs.netty.kafka.proto.KafkaEventTwo.Builder subBuilder = null;
            if (eventTypeCase_ == 2) {
              subBuilder = ((com.bxs.netty.kafka.proto.KafkaEventTwo) eventType_).toBuilder();
            }
            eventType_ =
                input.readMessage(com.bxs.netty.kafka.proto.KafkaEventTwo.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom((com.bxs.netty.kafka.proto.KafkaEventTwo) eventType_);
              eventType_ = subBuilder.buildPartial();
            }
            eventTypeCase_ = 2;
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.bxs.netty.kafka.proto.HelloWorldProto.internal_static_helloworld_Callback_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.bxs.netty.kafka.proto.HelloWorldProto.internal_static_helloworld_Callback_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.bxs.netty.kafka.proto.Callback.class, com.bxs.netty.kafka.proto.Callback.Builder.class);
  }

  private int eventTypeCase_ = 0;
  private java.lang.Object eventType_;
  public enum EventTypeCase
      implements com.google.protobuf.Internal.EnumLite {
    ONE_EVENT(1),
    TWO_EVENT(2),
    EVENTTYPE_NOT_SET(0);
    private final int value;
    private EventTypeCase(int value) {
      this.value = value;
    }
    /**
     * @deprecated Use {@link #forNumber(int)} instead.
     */
    @java.lang.Deprecated
    public static EventTypeCase valueOf(int value) {
      return forNumber(value);
    }

    public static EventTypeCase forNumber(int value) {
      switch (value) {
        case 1: return ONE_EVENT;
        case 2: return TWO_EVENT;
        case 0: return EVENTTYPE_NOT_SET;
        default: return null;
      }
    }
    public int getNumber() {
      return this.value;
    }
  };

  public EventTypeCase
  getEventTypeCase() {
    return EventTypeCase.forNumber(
        eventTypeCase_);
  }

  public static final int ONE_EVENT_FIELD_NUMBER = 1;
  /**
   * <code>.helloworld.KafkaEventOne one_event = 1;</code>
   */
  public boolean hasOneEvent() {
    return eventTypeCase_ == 1;
  }
  /**
   * <code>.helloworld.KafkaEventOne one_event = 1;</code>
   */
  public com.bxs.netty.kafka.proto.KafkaEventOne getOneEvent() {
    if (eventTypeCase_ == 1) {
       return (com.bxs.netty.kafka.proto.KafkaEventOne) eventType_;
    }
    return com.bxs.netty.kafka.proto.KafkaEventOne.getDefaultInstance();
  }
  /**
   * <code>.helloworld.KafkaEventOne one_event = 1;</code>
   */
  public com.bxs.netty.kafka.proto.KafkaEventOneOrBuilder getOneEventOrBuilder() {
    if (eventTypeCase_ == 1) {
       return (com.bxs.netty.kafka.proto.KafkaEventOne) eventType_;
    }
    return com.bxs.netty.kafka.proto.KafkaEventOne.getDefaultInstance();
  }

  public static final int TWO_EVENT_FIELD_NUMBER = 2;
  /**
   * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
   */
  public boolean hasTwoEvent() {
    return eventTypeCase_ == 2;
  }
  /**
   * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
   */
  public com.bxs.netty.kafka.proto.KafkaEventTwo getTwoEvent() {
    if (eventTypeCase_ == 2) {
       return (com.bxs.netty.kafka.proto.KafkaEventTwo) eventType_;
    }
    return com.bxs.netty.kafka.proto.KafkaEventTwo.getDefaultInstance();
  }
  /**
   * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
   */
  public com.bxs.netty.kafka.proto.KafkaEventTwoOrBuilder getTwoEventOrBuilder() {
    if (eventTypeCase_ == 2) {
       return (com.bxs.netty.kafka.proto.KafkaEventTwo) eventType_;
    }
    return com.bxs.netty.kafka.proto.KafkaEventTwo.getDefaultInstance();
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (eventTypeCase_ == 1) {
      output.writeMessage(1, (com.bxs.netty.kafka.proto.KafkaEventOne) eventType_);
    }
    if (eventTypeCase_ == 2) {
      output.writeMessage(2, (com.bxs.netty.kafka.proto.KafkaEventTwo) eventType_);
    }
    unknownFields.writeTo(output);
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (eventTypeCase_ == 1) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, (com.bxs.netty.kafka.proto.KafkaEventOne) eventType_);
    }
    if (eventTypeCase_ == 2) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, (com.bxs.netty.kafka.proto.KafkaEventTwo) eventType_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.bxs.netty.kafka.proto.Callback)) {
      return super.equals(obj);
    }
    com.bxs.netty.kafka.proto.Callback other = (com.bxs.netty.kafka.proto.Callback) obj;

    boolean result = true;
    result = result && getEventTypeCase().equals(
        other.getEventTypeCase());
    if (!result) return false;
    switch (eventTypeCase_) {
      case 1:
        result = result && getOneEvent()
            .equals(other.getOneEvent());
        break;
      case 2:
        result = result && getTwoEvent()
            .equals(other.getTwoEvent());
        break;
      case 0:
      default:
    }
    result = result && unknownFields.equals(other.unknownFields);
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    switch (eventTypeCase_) {
      case 1:
        hash = (37 * hash) + ONE_EVENT_FIELD_NUMBER;
        hash = (53 * hash) + getOneEvent().hashCode();
        break;
      case 2:
        hash = (37 * hash) + TWO_EVENT_FIELD_NUMBER;
        hash = (53 * hash) + getTwoEvent().hashCode();
        break;
      case 0:
      default:
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.bxs.netty.kafka.proto.Callback parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.bxs.netty.kafka.proto.Callback parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.bxs.netty.kafka.proto.Callback parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.bxs.netty.kafka.proto.Callback parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.bxs.netty.kafka.proto.Callback parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.bxs.netty.kafka.proto.Callback parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.bxs.netty.kafka.proto.Callback parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.bxs.netty.kafka.proto.Callback parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.bxs.netty.kafka.proto.Callback parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.bxs.netty.kafka.proto.Callback parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.bxs.netty.kafka.proto.Callback parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.bxs.netty.kafka.proto.Callback parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.bxs.netty.kafka.proto.Callback prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code helloworld.Callback}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:helloworld.Callback)
      com.bxs.netty.kafka.proto.CallbackOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.bxs.netty.kafka.proto.HelloWorldProto.internal_static_helloworld_Callback_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.bxs.netty.kafka.proto.HelloWorldProto.internal_static_helloworld_Callback_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.bxs.netty.kafka.proto.Callback.class, com.bxs.netty.kafka.proto.Callback.Builder.class);
    }

    // Construct using com.bxs.netty.kafka.proto.Callback.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      eventTypeCase_ = 0;
      eventType_ = null;
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.bxs.netty.kafka.proto.HelloWorldProto.internal_static_helloworld_Callback_descriptor;
    }

    public com.bxs.netty.kafka.proto.Callback getDefaultInstanceForType() {
      return com.bxs.netty.kafka.proto.Callback.getDefaultInstance();
    }

    public com.bxs.netty.kafka.proto.Callback build() {
      com.bxs.netty.kafka.proto.Callback result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.bxs.netty.kafka.proto.Callback buildPartial() {
      com.bxs.netty.kafka.proto.Callback result = new com.bxs.netty.kafka.proto.Callback(this);
      if (eventTypeCase_ == 1) {
        if (oneEventBuilder_ == null) {
          result.eventType_ = eventType_;
        } else {
          result.eventType_ = oneEventBuilder_.build();
        }
      }
      if (eventTypeCase_ == 2) {
        if (twoEventBuilder_ == null) {
          result.eventType_ = eventType_;
        } else {
          result.eventType_ = twoEventBuilder_.build();
        }
      }
      result.eventTypeCase_ = eventTypeCase_;
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.bxs.netty.kafka.proto.Callback) {
        return mergeFrom((com.bxs.netty.kafka.proto.Callback)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.bxs.netty.kafka.proto.Callback other) {
      if (other == com.bxs.netty.kafka.proto.Callback.getDefaultInstance()) return this;
      switch (other.getEventTypeCase()) {
        case ONE_EVENT: {
          mergeOneEvent(other.getOneEvent());
          break;
        }
        case TWO_EVENT: {
          mergeTwoEvent(other.getTwoEvent());
          break;
        }
        case EVENTTYPE_NOT_SET: {
          break;
        }
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.bxs.netty.kafka.proto.Callback parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.bxs.netty.kafka.proto.Callback) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int eventTypeCase_ = 0;
    private java.lang.Object eventType_;
    public EventTypeCase
        getEventTypeCase() {
      return EventTypeCase.forNumber(
          eventTypeCase_);
    }

    public Builder clearEventType() {
      eventTypeCase_ = 0;
      eventType_ = null;
      onChanged();
      return this;
    }


    private com.google.protobuf.SingleFieldBuilderV3<
        com.bxs.netty.kafka.proto.KafkaEventOne, com.bxs.netty.kafka.proto.KafkaEventOne.Builder, com.bxs.netty.kafka.proto.KafkaEventOneOrBuilder> oneEventBuilder_;
    /**
     * <code>.helloworld.KafkaEventOne one_event = 1;</code>
     */
    public boolean hasOneEvent() {
      return eventTypeCase_ == 1;
    }
    /**
     * <code>.helloworld.KafkaEventOne one_event = 1;</code>
     */
    public com.bxs.netty.kafka.proto.KafkaEventOne getOneEvent() {
      if (oneEventBuilder_ == null) {
        if (eventTypeCase_ == 1) {
          return (com.bxs.netty.kafka.proto.KafkaEventOne) eventType_;
        }
        return com.bxs.netty.kafka.proto.KafkaEventOne.getDefaultInstance();
      } else {
        if (eventTypeCase_ == 1) {
          return oneEventBuilder_.getMessage();
        }
        return com.bxs.netty.kafka.proto.KafkaEventOne.getDefaultInstance();
      }
    }
    /**
     * <code>.helloworld.KafkaEventOne one_event = 1;</code>
     */
    public Builder setOneEvent(com.bxs.netty.kafka.proto.KafkaEventOne value) {
      if (oneEventBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        eventType_ = value;
        onChanged();
      } else {
        oneEventBuilder_.setMessage(value);
      }
      eventTypeCase_ = 1;
      return this;
    }
    /**
     * <code>.helloworld.KafkaEventOne one_event = 1;</code>
     */
    public Builder setOneEvent(
        com.bxs.netty.kafka.proto.KafkaEventOne.Builder builderForValue) {
      if (oneEventBuilder_ == null) {
        eventType_ = builderForValue.build();
        onChanged();
      } else {
        oneEventBuilder_.setMessage(builderForValue.build());
      }
      eventTypeCase_ = 1;
      return this;
    }
    /**
     * <code>.helloworld.KafkaEventOne one_event = 1;</code>
     */
    public Builder mergeOneEvent(com.bxs.netty.kafka.proto.KafkaEventOne value) {
      if (oneEventBuilder_ == null) {
        if (eventTypeCase_ == 1 &&
            eventType_ != com.bxs.netty.kafka.proto.KafkaEventOne.getDefaultInstance()) {
          eventType_ = com.bxs.netty.kafka.proto.KafkaEventOne.newBuilder((com.bxs.netty.kafka.proto.KafkaEventOne) eventType_)
              .mergeFrom(value).buildPartial();
        } else {
          eventType_ = value;
        }
        onChanged();
      } else {
        if (eventTypeCase_ == 1) {
          oneEventBuilder_.mergeFrom(value);
        }
        oneEventBuilder_.setMessage(value);
      }
      eventTypeCase_ = 1;
      return this;
    }
    /**
     * <code>.helloworld.KafkaEventOne one_event = 1;</code>
     */
    public Builder clearOneEvent() {
      if (oneEventBuilder_ == null) {
        if (eventTypeCase_ == 1) {
          eventTypeCase_ = 0;
          eventType_ = null;
          onChanged();
        }
      } else {
        if (eventTypeCase_ == 1) {
          eventTypeCase_ = 0;
          eventType_ = null;
        }
        oneEventBuilder_.clear();
      }
      return this;
    }
    /**
     * <code>.helloworld.KafkaEventOne one_event = 1;</code>
     */
    public com.bxs.netty.kafka.proto.KafkaEventOne.Builder getOneEventBuilder() {
      return getOneEventFieldBuilder().getBuilder();
    }
    /**
     * <code>.helloworld.KafkaEventOne one_event = 1;</code>
     */
    public com.bxs.netty.kafka.proto.KafkaEventOneOrBuilder getOneEventOrBuilder() {
      if ((eventTypeCase_ == 1) && (oneEventBuilder_ != null)) {
        return oneEventBuilder_.getMessageOrBuilder();
      } else {
        if (eventTypeCase_ == 1) {
          return (com.bxs.netty.kafka.proto.KafkaEventOne) eventType_;
        }
        return com.bxs.netty.kafka.proto.KafkaEventOne.getDefaultInstance();
      }
    }
    /**
     * <code>.helloworld.KafkaEventOne one_event = 1;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        com.bxs.netty.kafka.proto.KafkaEventOne, com.bxs.netty.kafka.proto.KafkaEventOne.Builder, com.bxs.netty.kafka.proto.KafkaEventOneOrBuilder> 
        getOneEventFieldBuilder() {
      if (oneEventBuilder_ == null) {
        if (!(eventTypeCase_ == 1)) {
          eventType_ = com.bxs.netty.kafka.proto.KafkaEventOne.getDefaultInstance();
        }
        oneEventBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            com.bxs.netty.kafka.proto.KafkaEventOne, com.bxs.netty.kafka.proto.KafkaEventOne.Builder, com.bxs.netty.kafka.proto.KafkaEventOneOrBuilder>(
                (com.bxs.netty.kafka.proto.KafkaEventOne) eventType_,
                getParentForChildren(),
                isClean());
        eventType_ = null;
      }
      eventTypeCase_ = 1;
      onChanged();;
      return oneEventBuilder_;
    }

    private com.google.protobuf.SingleFieldBuilderV3<
        com.bxs.netty.kafka.proto.KafkaEventTwo, com.bxs.netty.kafka.proto.KafkaEventTwo.Builder, com.bxs.netty.kafka.proto.KafkaEventTwoOrBuilder> twoEventBuilder_;
    /**
     * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
     */
    public boolean hasTwoEvent() {
      return eventTypeCase_ == 2;
    }
    /**
     * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
     */
    public com.bxs.netty.kafka.proto.KafkaEventTwo getTwoEvent() {
      if (twoEventBuilder_ == null) {
        if (eventTypeCase_ == 2) {
          return (com.bxs.netty.kafka.proto.KafkaEventTwo) eventType_;
        }
        return com.bxs.netty.kafka.proto.KafkaEventTwo.getDefaultInstance();
      } else {
        if (eventTypeCase_ == 2) {
          return twoEventBuilder_.getMessage();
        }
        return com.bxs.netty.kafka.proto.KafkaEventTwo.getDefaultInstance();
      }
    }
    /**
     * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
     */
    public Builder setTwoEvent(com.bxs.netty.kafka.proto.KafkaEventTwo value) {
      if (twoEventBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        eventType_ = value;
        onChanged();
      } else {
        twoEventBuilder_.setMessage(value);
      }
      eventTypeCase_ = 2;
      return this;
    }
    /**
     * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
     */
    public Builder setTwoEvent(
        com.bxs.netty.kafka.proto.KafkaEventTwo.Builder builderForValue) {
      if (twoEventBuilder_ == null) {
        eventType_ = builderForValue.build();
        onChanged();
      } else {
        twoEventBuilder_.setMessage(builderForValue.build());
      }
      eventTypeCase_ = 2;
      return this;
    }
    /**
     * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
     */
    public Builder mergeTwoEvent(com.bxs.netty.kafka.proto.KafkaEventTwo value) {
      if (twoEventBuilder_ == null) {
        if (eventTypeCase_ == 2 &&
            eventType_ != com.bxs.netty.kafka.proto.KafkaEventTwo.getDefaultInstance()) {
          eventType_ = com.bxs.netty.kafka.proto.KafkaEventTwo.newBuilder((com.bxs.netty.kafka.proto.KafkaEventTwo) eventType_)
              .mergeFrom(value).buildPartial();
        } else {
          eventType_ = value;
        }
        onChanged();
      } else {
        if (eventTypeCase_ == 2) {
          twoEventBuilder_.mergeFrom(value);
        }
        twoEventBuilder_.setMessage(value);
      }
      eventTypeCase_ = 2;
      return this;
    }
    /**
     * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
     */
    public Builder clearTwoEvent() {
      if (twoEventBuilder_ == null) {
        if (eventTypeCase_ == 2) {
          eventTypeCase_ = 0;
          eventType_ = null;
          onChanged();
        }
      } else {
        if (eventTypeCase_ == 2) {
          eventTypeCase_ = 0;
          eventType_ = null;
        }
        twoEventBuilder_.clear();
      }
      return this;
    }
    /**
     * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
     */
    public com.bxs.netty.kafka.proto.KafkaEventTwo.Builder getTwoEventBuilder() {
      return getTwoEventFieldBuilder().getBuilder();
    }
    /**
     * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
     */
    public com.bxs.netty.kafka.proto.KafkaEventTwoOrBuilder getTwoEventOrBuilder() {
      if ((eventTypeCase_ == 2) && (twoEventBuilder_ != null)) {
        return twoEventBuilder_.getMessageOrBuilder();
      } else {
        if (eventTypeCase_ == 2) {
          return (com.bxs.netty.kafka.proto.KafkaEventTwo) eventType_;
        }
        return com.bxs.netty.kafka.proto.KafkaEventTwo.getDefaultInstance();
      }
    }
    /**
     * <code>.helloworld.KafkaEventTwo two_event = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        com.bxs.netty.kafka.proto.KafkaEventTwo, com.bxs.netty.kafka.proto.KafkaEventTwo.Builder, com.bxs.netty.kafka.proto.KafkaEventTwoOrBuilder> 
        getTwoEventFieldBuilder() {
      if (twoEventBuilder_ == null) {
        if (!(eventTypeCase_ == 2)) {
          eventType_ = com.bxs.netty.kafka.proto.KafkaEventTwo.getDefaultInstance();
        }
        twoEventBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            com.bxs.netty.kafka.proto.KafkaEventTwo, com.bxs.netty.kafka.proto.KafkaEventTwo.Builder, com.bxs.netty.kafka.proto.KafkaEventTwoOrBuilder>(
                (com.bxs.netty.kafka.proto.KafkaEventTwo) eventType_,
                getParentForChildren(),
                isClean());
        eventType_ = null;
      }
      eventTypeCase_ = 2;
      onChanged();;
      return twoEventBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFieldsProto3(unknownFields);
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:helloworld.Callback)
  }

  // @@protoc_insertion_point(class_scope:helloworld.Callback)
  private static final com.bxs.netty.kafka.proto.Callback DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.bxs.netty.kafka.proto.Callback();
  }

  public static com.bxs.netty.kafka.proto.Callback getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<Callback>
      PARSER = new com.google.protobuf.AbstractParser<Callback>() {
    public Callback parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new Callback(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<Callback> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<Callback> getParserForType() {
    return PARSER;
  }

  public com.bxs.netty.kafka.proto.Callback getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}


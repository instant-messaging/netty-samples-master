package com.bxs.netty.socket00.handler;

import com.alibaba.fastjson.JSON;
import com.bxs.netty.service.DiscardService;
import com.bxs.netty.utils.NettyAttrUtil;
import com.bxs.netty.utils.RequestURIUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.util.CharsetUtil;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static io.netty.handler.codec.http.HttpUtil.isKeepAlive;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Date: 2020/10/17 16:00
 */
@ChannelHandler.Sharable
@Component
public class WebsocketMessageHandler extends SimpleChannelInboundHandler<Object> {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebsocketMessageHandler.class);

    @Autowired
    private DiscardService discardService;

    @Setter
    private String webSocketURL;

    private WebSocketServerHandshaker handshaker;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        LOGGER.info("链接创建：{}", ctx.channel().remoteAddress());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        LOGGER.info("链接断开：{}", ctx.channel().remoteAddress());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof FullHttpRequest) {
            handleFullHttpRequest(ctx, (FullHttpRequest) msg);
        } else if (msg instanceof WebSocketFrame) {
            handleWebSocketFrame(ctx, (WebSocketFrame) msg);
        }
    }

    /**
     * 处理客户端向服务端发起 http 握手请求的业务
     * WebSocket在建立握手时，数据是通过HTTP传输的。但是建立之后，在真正传输时候是不需要HTTP协议的。
     * <p>
     * WebSocket 连接过程：
     * 首先，客户端发起http请求，经过3次握手后，建立起TCP连接；http请求里存放WebSocket支持的版本号等信息，如：Upgrade、Connection、WebSocket-Version等；
     * 然后，服务器收到客户端的握手请求后，同样采用HTTP协议回馈数据；
     * 最后，客户端收到连接成功的消息后，开始借助于TCP传输信道进行全双工通信。
     */
    private void handleFullHttpRequest(ChannelHandlerContext ctx, FullHttpRequest request) {
        // 如果请求失败或者该请求不是客户端向服务端发起的 http 请求，则响应错误信息
        if (!request.decoderResult().isSuccess() || (!"websocket".equals(request.headers().get("Upgrade")))) {
            // 若不是websocket方式，则创建BAD_REQUEST的req，返回给客户端
            sendHttpResponse(ctx, request, new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST));
            return;
        }

        // WebSocket 握手工厂类
        WebSocketServerHandshakerFactory factory = new WebSocketServerHandshakerFactory(webSocketURL, null, false);
        // 新建一个握手
        handshaker = factory.newHandshaker(request);
        if (handshaker == null) {
            // 如果为空，返回响应：不受支持的 websocket 版本
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            // 否则，执行握手
            Map<String, String> params = RequestURIUtil.getParams(request.uri());
            String userId = params.get("userId");
            Channel channel = ctx.channel();

            NettyAttrUtil.setUserId(channel, userId);
            NettyAttrUtil.refreshLastHeartBeatTime(channel);
            handshaker.handshake(ctx.channel(), request);
            SessionManager.addChannel(userId, channel);
            SessionManager.addChannel(channel);
            LOGGER.info("握手成功，客户端请求uri：{}", request.uri());

        }
    }

    /**
     * 服务端向客户端响应消息
     */
    private static void sendHttpResponse(ChannelHandlerContext ctx, FullHttpRequest request, DefaultFullHttpResponse response) {
        if (response.status().code() != 200) {
            // 创建源缓冲区
            ByteBuf byteBuf = Unpooled.copiedBuffer(response.status().toString(), CharsetUtil.UTF_8);
            // 将源缓冲区的数据传送到此缓冲区
            response.content().writeBytes(byteBuf);
            // 释放源缓冲区
            byteBuf.release();
        }
        // 写入请求，服务端向客户端发送数据
        ChannelFuture channelFuture = ctx.channel().writeAndFlush(response);
        // 如果是非Keep-Alive，关闭连接
        if (!isKeepAlive(request) || response.status().code() != 200) {
            // 如果请求失败，关闭 ChannelFuture
            // ChannelFutureListener.CLOSE 源码：future.channel().close();
            channelFuture.addListener(ChannelFutureListener.CLOSE);
        }
    }


    private void handleWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame frame) {
        /// 判断是否关闭链路的指令
        if (frame instanceof CloseWebSocketFrame) {
            //关闭握手
            if (handshaker != null){
                handshaker.close(ctx.channel(), (CloseWebSocketFrame) frame.retain());
            } else {
                ctx.writeAndFlush(frame.retainedDuplicate()).addListener(ChannelFutureListener.CLOSE);
            }
            SessionManager.removeChannel(ctx.channel());
            return;
        }
        // 判断是否ping消息
        if (frame instanceof PingWebSocketFrame) {
            ctx.channel().write(new PongWebSocketFrame(frame.content().retain()));
            return;
        }
        // 判断是否Pong消息
        if (frame instanceof PongWebSocketFrame) {
            ctx.writeAndFlush(new PongWebSocketFrame(frame.content().retain()));
            return;
        }
        // 二进制消息处理
        if (frame instanceof BinaryWebSocketFrame) {
            LOGGER.info("服务器接收到二进制消息. [{}]", frame.toString());

            ByteBuf content = frame.content();
            content.markReaderIndex();
            int flag = content.readInt();
            LOGGER.info("标志位:[{}]", flag);
            content.resetReaderIndex();

            ByteBuf byteBuf = Unpooled.directBuffer(frame.content().capacity());
            byteBuf.writeBytes(frame.content());

            // 转成byte
            byte [] bytes = new byte[frame.content().capacity()];
            byteBuf.readBytes(bytes);
            // byte转ByteBuf
            ByteBuf byteBuf2 = Unpooled.directBuffer(bytes.length);
            byteBuf2.writeBytes(bytes);

            LOGGER.info("JSON.toJSONString(byteBuf) [ {} ]", JSON.toJSONString(byteBuf));
            //TODO 这是发给自己
            ctx.writeAndFlush(new BinaryWebSocketFrame(byteBuf));
            return;
        }
        // 获取客户端发送过来的文本消息
        if (frame instanceof TextWebSocketFrame) {
            TextWebSocketFrame textWebSocketFrame = (TextWebSocketFrame) frame;
            // 业务层处理数据
            this.discardService.discard(textWebSocketFrame.text());
            // 响应客户端
            ctx.channel().writeAndFlush(new TextWebSocketFrame("我收到了你的消息：" + System.currentTimeMillis()));
        } else {
            // 不接受文本以外的数据帧类型
            ctx.channel().writeAndFlush(WebSocketCloseStatus.INVALID_MESSAGE_TYPE).addListener(ChannelFutureListener.CLOSE);
        }
    }
}

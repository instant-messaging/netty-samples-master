package com.bxs.netty.chain.handler;

import com.bxs.netty.chain.quest.Request;
import com.bxs.netty.chain.quest.RequestType;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 14:30
 */
public class SoftwareEngineer extends RequestHandler {


    public SoftwareEngineer(RequestHandler handler) {
        super(handler);
    }

    @Override
    public void handleRequest(Request req) {
        if (RequestType.SOFTWARE_ENGINNER == req.getRequestType()) {
            printHandling(req);
            req.markHandled();
        } else {
            super.handleRequest(req);
        }
    }

    @Override
    public String toString() {
        return "软件工程师";
    }
}

package com.bxs.netty.persistence.domain.vo;

import lombok.Data;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 16:56
 */
@Data
public class Device {

    private Long id;
    private String name;
    private Double temperature;
    private Double humidity;
    private Long createdTime;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Device [id=");
        builder.append(id);
        builder.append(", name=");
        builder.append(name);
        builder.append(", temperature=");
        builder.append(temperature);
        builder.append(", createdTime=");
        builder.append(createdTime);
        builder.append(", humidity=");
        builder.append(humidity);
        builder.append("]");
        return builder.toString();
    }
}

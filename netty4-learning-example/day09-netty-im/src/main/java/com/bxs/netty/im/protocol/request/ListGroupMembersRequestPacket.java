package com.bxs.netty.im.protocol.request;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;

import static com.bxs.netty.im.protocol.command.Command.LIST_GROUP_MEMBERS_REQUEST;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:55
 */
@Data
public class ListGroupMembersRequestPacket extends Packet {

    private String groupId;

    @Override
    public Byte getCommand() {
        return LIST_GROUP_MEMBERS_REQUEST;
    }
}

package com.bxs.day08.websocket01;

import com.bxs.day08.websocket01.business.Constant;
import com.bxs.day08.websocket01.business.UserInfo;
import com.bxs.day08.websocket01.business.UserService;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.cookie.Cookie;
import io.netty.handler.codec.http.cookie.ServerCookieDecoder;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.AttributeKey;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/24 11:47
 */
@ChannelHandler.Sharable
public class WebSocketServerHandshakeHandler extends SimpleChannelInboundHandler<Object> {
    private Logger logger = LoggerFactory.getLogger(WebSocketServerHandshakeHandler.class);

    /**
     * 请求类型常量
     */
    private static final String WEBSOCKET_UPGRADE = "websocket";
    private static final String WEBSOCKET_HEAD = "Upgrade";

    private String path = "/websocket";

    private UserService userService;

    /**
     * 读取连接消息并对消息进行处理
     * @param ctx 处理上下文
     * @param msg WebSocket组件
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        // 处理WebSocket协议的信息
        if (msg instanceof WebSocketFrame) {
            handleWebSocketFrame(ctx, (WebSocketFrame) msg);
        }
        // 处理HTTP协议升级为WebSocket
        else if (msg instanceof FullHttpRequest) {
            handleHttpRequest(ctx, (FullHttpRequest) msg);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // 不自动执行
    }

    /**
     * 客户端断开连接之后触发
     * @param ctx 处理和上下文
     * @throws Exception 捕获异常
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {

    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;

            if (event.state() == IdleState.READER_IDLE) {
                logger.debug("读空闲 ......");
            } else if (event.state() == IdleState.WRITER_IDLE) {
                logger.debug("写空闲 ......");
            } else if (event.state() == IdleState.ALL_IDLE) {
                logger.debug("等待超时，清理服务器无用资源 ......");
                ctx.channel().closeFuture().sync();
            }
        }
    }

    /**
     * 获取UserInfo
     *
     * @return 如果存在则返回，不存在返回null
     */
    private UserInfo checkLogin(FullHttpRequest req) {
        try {
            if (req.headers() != null) {
                Set<Cookie> cookies = ServerCookieDecoder.LAX.decode(req.headers().get("Cookie"));
                for (Cookie cookie : cookies) {
                    if (Constant.TOKEN.equals(cookie.name())) {
                        if (userService.loginCheck(cookie.value())){
                            return userService.getUserInfo(cookie.value());
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * 完整发送完消息后关闭通道
     */
    private void closeFuture(ChannelHandlerContext ctx, Object msg) {
        ctx.writeAndFlush(msg).addListener(ChannelFutureListener.CLOSE);
    }

    /**
     * 发送400 Bad Request
     */
    private void sendBadRequest(ChannelHandlerContext ctx) {
        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, BAD_REQUEST);
        ByteBuf buf = Unpooled.copiedBuffer(response.status().toString(), CharsetUtil.UTF_8);
        response.content().writeBytes(buf);
        buf.release();
        HttpUtil.setContentLength(response, response.content().readableBytes());
        closeFuture(ctx, response);
    }

    /**
     * Http协议和转换
     * @param ctx 处理上下文
     * @param request 消息请求
     */
    private void handleHttpRequest(ChannelHandlerContext ctx, FullHttpRequest request) {
        // 如果HTTP解码失败，返回HTTP异常
        boolean success = request.decoderResult().isSuccess();
        String header = request.headers().get(WEBSOCKET_HEAD);
        String uri = request.getUri();
        if (!request.decoderResult().isSuccess() || !WEBSOCKET_UPGRADE.equals(request.headers().get(WEBSOCKET_HEAD)) || !path.equals(request.uri())) {
            sendBadRequest(ctx);
            return;
        }
        /**
         * 握手工厂
         * 协议升级, 构造握手响应返回
         */
        WebSocketServerHandshakerFactory handshakerFactory = new WebSocketServerHandshakerFactory(getWebSocketLocation(request), null, true, 5 * 1024 * 1024);
        WebSocketServerHandshaker handshaker = handshakerFactory.newHandshaker(request);
        if (handshaker != null) {
            // 表示握手成功
            handshaker.handshake(ctx.channel(), request);
            // 设置 UserInfo
            ctx.channel().attr(AttributeKey.valueOf(Constant.USERINFO)).set(checkLogin(request));
            // 握手成功才添加到ChannelGroup
            ctx.fireChannelActive();

            logger.info("Http-websocket握手协议升级成功啦");
        } else {
            sendBadRequest(ctx);
        }
    }

    private void handleWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame frame){
        // 判断是否是关闭链路的指令
        if (frame instanceof CloseWebSocketFrame) {
            // 将其移出ChannelGroup
            closeFuture(ctx, frame.retain());
        }
        // 判断是否是Ping消息
        else if (frame instanceof PingWebSocketFrame) {
            ctx.channel().write(new PongWebSocketFrame(frame.content().retain()));
        } else {
            ctx.fireChannelRead(frame.retain());
        }
    }

    private String getWebSocketLocation(FullHttpRequest req) {
        String location =  req.headers().get(HttpHeaderNames.HOST) + path;
        return "ws://" + location;
    }
}

package com.bxs.netty.springboot.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Calendar;

/**
 * 使用监听器启动
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/25 14:37
 */
public class WebSocketServerrListener implements ServletContextListener {
    private static final Logger logger = LoggerFactory.getLogger(WebSocketServerrListener.class);

    private WebSocketServer nettyServer;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        nettyServer = new WebSocketServer(8091);
        try {
            nettyServer.start();
        } catch (Exception e) {
            logger.error("netty server abnormal startup {netty服务启动异常}", e);
        }
    }

    /**
     * 销毁
     *
     * @param sce
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.info("destroy() of netty {}  flag:{}", Calendar.getInstance().getTime(), (null == nettyServer));
        nettyServer.destroy();
    }
}

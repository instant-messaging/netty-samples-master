package com.bxs.day07.protocol2;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 9:37
 */
public final class NettyConstant {
    public static final String REMOTEIP = "127.0.0.1";
    public static final int PORT = 8080;
    public static final int LOCAL_PORT = 12088;
    public static final String LOCALIP = "127.0.0.1";
}

package com.bxs.day07.protocol1.common;

/**
 * 说明：协议消息头
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 17:59
 */
public class ProtocolHeader {
    /**
     * 魔数
     */
    private byte magic;
    /**
     * 消息类型
     */
    private byte type;
    /**
     * 保留字
     */
    private short reserve;
    /**
     * 序列号
     */
    private short sn;
    /**
     * 长度
     */
    private int len;

    public ProtocolHeader() {
    }

    public ProtocolHeader(byte magic, byte messageType, short reserve, short sn, int len) {
        this.magic = magic;
        this.type = messageType;
        this.reserve = reserve;
        this.sn = sn;
        this.len = len;
    }

    public byte getMagic() {
        return magic;
    }

    public ProtocolHeader setMagic(byte magic) {
        this.magic = magic;
        return this;
    }

    public byte getType() {
        return type;
    }

    public ProtocolHeader setType(byte type) {
        this.type = type;
        return this;
    }

    public short getReserve() {
        return reserve;
    }

    public ProtocolHeader setReserve(short reserve) {
        this.reserve = reserve;
        return this;
    }

    public short getSn() {
        return sn;
    }

    public ProtocolHeader setSn(short sn) {
        this.sn = sn;
        return this;
    }

    public int getLen() {
        return len;
    }

    public ProtocolHeader setLen(int len) {
        this.len = len;
        return this;
    }
}

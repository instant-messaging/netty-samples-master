package com.bxs.netty;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/24 17:38
 */
public class OrderResponse implements java.io.Serializable {
    private static final long serialVersionUID = -5003946216600820264L;

    private Integer orderId;
    private String respCode;
    private String desc;

    public Integer getOrderId() {
        return orderId;
    }

    public OrderResponse setOrderId(Integer orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getRespCode() {
        return respCode;
    }

    public OrderResponse setRespCode(String respCode) {
        this.respCode = respCode;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public OrderResponse setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    @Override
    public String toString() {
        return "OrderResponse{" +
                "orderId=" + orderId +
                ", respCode='" + respCode + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}

package com.bxs.netty.im.attribute;

import com.bxs.netty.im.session.Session;
import io.netty.util.AttributeKey;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:56
 */
public interface Attributes {
    AttributeKey<Session> SESSION = AttributeKey.newInstance("session");
}

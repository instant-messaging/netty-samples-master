package com.bxs.netty.im.session;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:57
 */
@Data
@NoArgsConstructor
public class Session {
    /**
     * 用户唯一性标识
     */
    private String userId;
    private String username;

    public Session(String userId, String username) {
        this.userId = userId;
        this.username = username;
    }

    @Override
    public String toString() {
        return userId + ":" + username;
    }
}

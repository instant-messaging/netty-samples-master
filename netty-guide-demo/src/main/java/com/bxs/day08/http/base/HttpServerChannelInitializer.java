package com.bxs.day08.http.base;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;

/**
 * HTTP Server ChannelInitializer.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 14:53
 */
public class HttpServerChannelInitializer extends ChannelInitializer<SocketChannel> {

    public HttpServerChannelInitializer() {
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        //pipeline.addLast(new HttpRequestDecoder());
        //pipeline.addLast(new HttpResponseEncoder());
        pipeline.addLast(new HttpServerCodec());
        pipeline.addLast(new HttpObjectAggregator(256 * 1024));
        pipeline.addLast(new HttpServerHandler());
    }
}

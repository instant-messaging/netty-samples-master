package com.bxs.protocol.custom.common.entity;

import lombok.Builder;

import java.io.Serializable;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 10:42
 */
@Builder
public class Body implements Serializable {
    private Object payload;

    public Body() {
    }

    public Body(Object payload) {
        this.payload = payload;
    }

    public Object getPayload() {
        return payload;
    }

    public Body setPayload(Object payload) {
        this.payload = payload;
        return this;
    }

    @Override
    public String toString() {
        return "Body{" +
                "payload=" + payload +
                '}';
    }
}

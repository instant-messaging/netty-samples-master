package com.bxs.netty.singleton.type;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:04
 */
public final class InitializingOnDemandHolderIdiom {

    private InitializingOnDemandHolderIdiom() {
    }

    public static InitializingOnDemandHolderIdiom getInstance() {
        return HelperHolder.INSTANCE;
    }

    private static class HelperHolder {
        private static final InitializingOnDemandHolderIdiom INSTANCE = new InitializingOnDemandHolderIdiom();
    }
}

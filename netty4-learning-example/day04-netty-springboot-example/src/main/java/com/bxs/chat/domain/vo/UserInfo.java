package com.bxs.chat.domain.vo;

import com.bxs.chat.domain.entity.Group;
import com.bxs.chat.domain.entity.User;
import lombok.*;

import java.util.List;

/**
 * 用户基本信息VO
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 11:33
 */
@AllArgsConstructor
@Builder
@ToString
public class UserInfo {

    /**
     * 用户id
     */
    @Setter
    @Getter
    private Long userId;

    /**
     * 用户名称
     */
    @Setter
    @Getter
    private String username;

    /**
     *  用户头像
     */
    @Setter
    @Getter
    private String avatarUrl;

    /**
     *  好友列表
     */
    @Setter
    @Getter
    private List<User> friendList;

    /**
     *  群列表
     */
    @Setter
    @Getter
    private List<Group> groupList;
}

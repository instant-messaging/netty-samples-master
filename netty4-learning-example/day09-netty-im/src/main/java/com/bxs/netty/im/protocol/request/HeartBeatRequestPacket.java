package com.bxs.netty.im.protocol.request;

import com.bxs.netty.im.protocol.Packet;

import static com.bxs.netty.im.protocol.command.Command.HEARTBEAT_REQUEST;

/**
 * 心跳请求包
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:53
 */
public class HeartBeatRequestPacket extends Packet {
    @Override
    public Byte getCommand() {
        return HEARTBEAT_REQUEST;
    }
}

package com.bxs.netty.iterator.list;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:13
 */
public class Item {

    private ItemType type;
    private String name;

    public Item(ItemType type, String name) {
        this.setType(type);
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public ItemType getType() {
        return type;
    }

    public final void setType(ItemType type) {
        this.type = type;
    }
}

package com.bxs.netty.serialize.impl;

import com.alibaba.fastjson.JSON;
import com.bxs.netty.serialize.Serializer;


/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 11:18
 */
public class JSONSerializer implements Serializer {
    @Override
    public byte[] serialize(Object object) {
        return JSON.toJSONBytes(object);
    }

    @Override
    public <T> T deserialize(Class<T> clazz, byte[] bytes) {
        return JSON.parseObject(bytes, clazz);
    }
}

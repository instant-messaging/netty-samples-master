package com.bxs.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/24 8:50
 */
public class TimeClientHandler implements Runnable {

    private String host;
    private int port;
    private Selector selector;
    private SocketChannel socketChannel;
    private volatile boolean stop;


    public TimeClientHandler(String host, int port) {
        this.host = host == null ? "127.0.0.1" : host;
        this.port = port;
        try {
            // 打开连接
            selector = Selector.open();
            // 打开连接
            socketChannel = SocketChannel.open();
            // TODO 该处存在一个小BUG,第一条数据会丢失
            // TODO 如果该处设为true阻塞,则客户端会报错,应该是本人写法还存在点问题
            socketChannel.configureBlocking(false);
        } catch (IOException e) {
            e.printStackTrace();
            // 异常情况断开连接
            System.exit(1);
        }
    }

    @Override
    public void run() {
        try {
            doConnect();
        } catch (Exception e) {
            e.printStackTrace();
            // 异常情况断开连接
            System.exit(1);
        }
        while (!stop) {
            try {
                if (selector.select() > 0) {
                    Set<SelectionKey> selectionKeys = selector.selectedKeys();
                    Iterator<SelectionKey> iterator = selectionKeys.iterator();
                    while (iterator.hasNext()) {
                        SelectionKey selectionKey = iterator.next();
                        iterator.remove();
                        try {
                            handleInput(selectionKey);
                        } catch (Exception e) {
                            if (selectionKey != null) {
                                selectionKey.cancel();
                                if (selectionKey.channel() != null) {
                                    selectionKey.channel().close();
                                }
                            }
                        }
                    }
                }
            } catch (IOException e) {
                System.exit(1);
            }
        }
        if (selector != null) {
            try {
                selector.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleInput(SelectionKey key) throws IOException {
        // 验证这个key是否有效
        if (key.isValid()) {
            SocketChannel channel = (SocketChannel) key.channel();
            // 判断SocketChannel是否处于连接状态
            if (key.isConnectable()) {
                // 判断 SocketChannel 是否连接成功
                if (channel.finishConnect()) {
                    // 连接成功则注册OP_READ事件到Selector选择器中
                    channel.register(this.selector, SelectionKey.OP_READ);
                    doWrite(channel);
                } else {
                    System.exit(1);
                }
                // 创建读取所需Buffer
                ByteBuffer readBuffer = ByteBuffer.allocate(1024);
                int read = channel.read(readBuffer);
                if (read > 0) {
                    // 反转缓冲区
                    readBuffer.flip();
                    byte[] bytes = new byte[readBuffer.remaining()];
                    readBuffer.get(bytes);
                    String msg = new String(bytes, "UTF-8");
                    System.out.println("TimeClient 接收到的消息:" + msg);
                }
                // 如果接收完毕退出循环
                this.stop = true;
            }
        }
    }

    private void doConnect() throws IOException {
        if (socketChannel.connect(new InetSocketAddress(host, port))) {
            socketChannel.register(this.selector, SelectionKey.OP_READ);
            doWrite(socketChannel);
        } else {
            // 向Reactor线程的Selector注册OP_CONNECT事件
            socketChannel.register(this.selector, SelectionKey.OP_CONNECT);
        }
    }

    private void doWrite(SocketChannel channel) throws IOException {
        byte[] req = "挽歌君帅不帅".getBytes();
        ByteBuffer buffer = ByteBuffer.allocate(req.length);
        // 将字节数组复制到缓冲区
        buffer.put(req);
        // 反转缓冲区
        buffer.flip();
        channel.write(buffer);
        if (!buffer.hasRemaining()) {
            System.out.println("消息发送成功");
        }
    }
}

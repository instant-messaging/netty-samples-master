package com.bxs.day10.live;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/10 13:49
 */
public class LiveServer {
    private static Logger logger = LoggerFactory.getLogger(LiveServer.class);

    private final int port;

    public LiveServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("Usage: " + LiveServer.class.getSimpleName() + " <port>");
            return;
        }
        int port = Integer.parseInt(args[0]);
        new LiveServer(port).run();
    }

    private void run() {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            logger.debug("initChannel ch:" + ch);

                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast(new LiveDecoder());
                            pipeline.addLast(new LiveEncoder());
                            //pipeline.addLast(new HttpObjectAggregator(256 * 1024));
                            pipeline.addLast(new LiveHandler());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, Boolean.TRUE);

            // 绑定并开始接受进入的连接
            ChannelFuture future = bootstrap.bind(port).sync();

            // 等待直到服务器套接字关闭
            // 在本例中，这种情况不会发生，但您可以优雅地完成
            // 关闭服务器
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}

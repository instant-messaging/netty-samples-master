package com.bxs.netty.persistence.service;

import com.bxs.netty.persistence.domain.vo.Device;
import com.bxs.netty.persistence.domain.repository.DeviceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:13
 */
@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DeviceDao deviceDao;

    @Override
    public Device saveDevice(Device device) {
        Device savedDevice = deviceDao.save(device);
        return savedDevice;
    }
}

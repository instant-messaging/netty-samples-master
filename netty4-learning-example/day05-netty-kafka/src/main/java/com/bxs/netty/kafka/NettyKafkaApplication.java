package com.bxs.netty.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 14:58
 */
@SpringBootApplication
public class NettyKafkaApplication {
    public static ConfigurableApplicationContext context;

    public static void main(String[] args) {
        context = SpringApplication.run(NettyKafkaApplication.class, args);
    }
}

package com.bxs.netty.springboot.websocket.config;

import com.bxs.netty.springboot.websocket.WebSocketServerrListener;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 14:44
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {


    @Bean(name = "nettyServerListener")
    public ServletListenerRegistrationBean<WebSocketServerrListener> nettyListenerRegist() {
        ServletListenerRegistrationBean<WebSocketServerrListener> registrationBean = new ServletListenerRegistrationBean<>();
        registrationBean.setListener(new WebSocketServerrListener());
        return registrationBean;
    }
}

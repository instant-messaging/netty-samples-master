package com.bxs.netty.io.pio;

import java.util.concurrent.*;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/9 10:25
 */
public class TimeServerHandlerExecutePool {

    /**
     * 初始化线程池执行器
     */
    private ExecutorService service;

    /**
     * 线程池
     *
     * @param maxPoolSize 最大线程数
     * @param queueSize   队列大小
     */
    public TimeServerHandlerExecutePool(int maxPoolSize, int queueSize) {
        // 队列
        //BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(queueSize);
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(queueSize);

        // 初始化线程池执行器
        ExecutorService executorService = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(), maxPoolSize, 120L, TimeUnit.SECONDS, workQueue);

        this.service = executorService;
    }

    public void execute(Runnable task) {
        service.execute(task);
    }
}

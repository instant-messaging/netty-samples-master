package com.bxs.netty.persistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 16:37
 */
@SpringBootApplication
public class NettyPersistenceApplication {

    public static void main(String[] args) {
        SpringApplication.run(NettyPersistenceApplication.class);
    }
}

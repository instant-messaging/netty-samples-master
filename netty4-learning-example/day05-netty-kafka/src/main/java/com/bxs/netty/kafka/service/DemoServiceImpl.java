package com.bxs.netty.kafka.service;

import com.bxs.netty.kafka.NettyKafkaApplication;
import com.bxs.netty.kafka.kafka.Sender;
import com.bxs.netty.kafka.proto.Callback;
import com.bxs.netty.kafka.proto.KafkaEventOne;
import com.bxs.netty.kafka.proto.KafkaEventTwo;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 14:59
 */
@Service
public class DemoServiceImpl implements DemoService {

    @Override
    public Map<String, Object> run() {
        Map<String, Object> result = new HashMap<>();

        Callback request = Callback.newBuilder()
                .setOneEvent(KafkaEventOne.newBuilder().setAddress("beijing").build())
                .setTwoEvent(KafkaEventTwo.newBuilder().setNumber(123456).build())
                .build();

        Sender sender = NettyKafkaApplication.context.getBean(Sender.class);
        sender.send(request);

        return result;
    }
}

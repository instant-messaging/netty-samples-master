package com.bxs.netty.springboot.web;

import com.bxs.netty.springboot.client.NettyClient;
import com.bxs.netty.springboot.protocol.protobuf.MessageBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 17:04
 */
@RestController
public class ConsumerController {
    @Autowired
    private NettyClient nettyClient;

    @GetMapping("/send")
    public String send() {
        MessageBase.Message message = MessageBase.Message.newBuilder()
                .setCmd(MessageBase.Message.CommandType.NORMAL)
                .setContent("hello server")
                .setRequestId(UUID.randomUUID().toString())
                .build();
        nettyClient.sendMsg(message);
        return "send ok";
    }
}

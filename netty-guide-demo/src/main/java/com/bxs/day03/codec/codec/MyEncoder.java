package com.bxs.day03.codec.codec;

import com.bxs.day03.common.Message;
import com.bxs.day03.common.MessageHeader;
import com.bxs.day03.common.MessageType;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.nio.charset.Charset;

/**
 * My Encoder.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 17:56
 */
public class MyEncoder extends MessageToByteEncoder<Message> {

    @Override
    protected void encode(ChannelHandlerContext ctx, Message message, ByteBuf out) throws Exception {
        if (message == null | message.getMessageHeader() == null) {
            throw new Exception("The encode message is null");
        }

        // 获取消息头
        MessageHeader header = message.getMessageHeader();

        // 获取消息体
        String body = message.getBody();
        byte[] bodyBytes = body.getBytes(Charset.forName("utf-8"));

        // 计算消息体的长度
        int bodySize = bodyBytes.length;

        System.out.printf("MyEncoder header: %s, body: %s", header.getMessageType(), body);

        out.writeByte(MessageType.EMGW_LOGIN_RES.getValue());
        out.writeInt(bodySize);
        out.writeBytes(bodyBytes);
    }
}

package com.bxs.io;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 伪异步方式的I/O创建
 *
 * @Author: Mr.Lu
 * @Date: 2020/8/22 17:40
 */
public class TimeServerPool {

    public static void main(String[] args) {
        int port = 4041;
        System.out.println("start server......" + port);
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            Socket socket = serverSocket.accept();
            //创建I/O任务线程池
            TimeServerHandlerExecutePool executePool = new TimeServerHandlerExecutePool(50, 100);
            executePool.execute(new TimeServerHandler(socket));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

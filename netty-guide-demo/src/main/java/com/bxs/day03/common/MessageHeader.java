package com.bxs.day03.common;

/**
 * Message Header.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 17:56
 */
public class MessageHeader {
    /**
     * 消息类型
     */
    private byte messageType;
    /**
     * 长度
     */
    private int len;

    public MessageHeader() {
    }

    public MessageHeader(byte messageType, int len) {
        this.messageType = messageType;
        this.len = len;
    }

    public byte getMessageType() {
        return messageType;
    }

    public MessageHeader setMessageType(byte messageType) {
        this.messageType = messageType;
        return this;
    }

    public int getLen() {
        return len;
    }

    public MessageHeader setLen(int len) {
        this.len = len;
        return this;
    }
}

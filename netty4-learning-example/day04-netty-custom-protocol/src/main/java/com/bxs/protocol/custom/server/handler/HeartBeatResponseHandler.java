package com.bxs.protocol.custom.server.handler;

import com.bxs.protocol.custom.common.constant.NettyConstant;
import com.bxs.protocol.custom.common.entity.Header;
import com.bxs.protocol.custom.common.entity.Message;
import com.bxs.protocol.custom.common.enums.MessageType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 11:26
 */
public class HeartBeatResponseHandler extends ChannelInboundHandlerAdapter {
    private static final Logger log = LoggerFactory.getLogger(HeartBeatResponseHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ChannelPipeline pipeline = ctx.channel().pipeline();
        ChannelPipeline pipeline1 = ctx.pipeline();
        log.debug("pipeline is equal" + (pipeline == pipeline1) + "   " + (ctx.channel() == ctx.pipeline().channel()));

        Message message = (Message) msg;
        log.debug("HeartBeatRespHandler receive msg: {}", msg.toString());

        if (message != null && message.getHeader().getType() == MessageType.HEARTBEAT_REQUEST.getType().byteValue()) {
            Header header = Header.builder()
                    .sessionId(NettyConstant.LOGIN_SESSION_ID)
                    .priority(NettyConstant.PRIORITY)
                    .code(NettyConstant.CODE)
                    .type(MessageType.HEARTBEAT_RESPONSE.getType().byteValue())
                    .length(NettyConstant.HEADER_LENGTH)
                    .build();
            message.setHeader(header);
            ctx.channel().writeAndFlush(message);
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println(cause);
        ctx.close();
    }
}

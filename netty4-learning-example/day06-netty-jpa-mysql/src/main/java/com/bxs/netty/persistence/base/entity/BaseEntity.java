package com.bxs.netty.persistence.base.entity;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:03
 */
public interface BaseEntity<D> extends ToData<D> {

    Long getId();

    void setId(Long id);
}

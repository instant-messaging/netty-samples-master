package com.bxs.netty.chain.handler;

import com.bxs.netty.chain.quest.Request;
import com.bxs.netty.chain.quest.RequestType;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 14:28
 */
public class ProductManager extends RequestHandler {
    public ProductManager(RequestHandler handler) {
        super(handler);
    }

    @Override
    public void handleRequest(Request req) {
        if (RequestType.PRODUCT_MANGER == req.getRequestType()) {
            printHandling(req);
            req.markHandled();
        } else {
            super.handleRequest(req);
        }
    }

    @Override
    public String toString() {
        return "产品经理";
    }
}

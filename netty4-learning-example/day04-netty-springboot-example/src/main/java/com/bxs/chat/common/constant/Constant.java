package com.bxs.chat.common.constant;

import com.bxs.chat.domain.entity.Group;
import com.bxs.chat.domain.vo.UserInfo;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 常量枚举
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 11:30
 */
public class  Constant {

    public static final String USER_TOKEN = "userId";

    public static final Integer THEAD_SIZE = 10;

    public static Map<String, WebSocketServerHandshaker> webSocketHandshakerMap = new ConcurrentHashMap<>();

    public static Map<String, ChannelHandlerContext> onlineUserMap = new ConcurrentHashMap<>();

    public static Map<String, Group> groupInfoMap = new ConcurrentHashMap<>();

    public static Map<String, UserInfo> userInfoMap = new HashMap<>();
}

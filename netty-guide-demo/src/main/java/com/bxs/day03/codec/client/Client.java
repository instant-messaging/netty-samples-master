package com.bxs.day03.codec.client;

import com.bxs.day03.codec.codec.MyCodec;
import com.bxs.day03.common.Message;
import com.bxs.day03.common.MessageHeader;
import com.bxs.day03.common.MessageType;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.CharsetUtil;

/**
 * My Client.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 18:09
 */
public class Client {

    private String host;
    private int port;

    public Client(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void run() throws InterruptedException {

        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(workerGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast("codec", new MyCodec());
                            ch.pipeline().addLast(new ClientHandler());
                        }
                    });

            // 启动客户端
            ChannelFuture future = bootstrap.connect(host, port).sync();

            while (true) {
                // 发送消息给服务器
                Message message = new Message();
                MessageHeader header = new MessageHeader();
                header.setMessageType(MessageType.EMGW_LOGIN_REQ.getValue());
                String body = "床前明月光，疑是地上霜。举头望明月，低头思故乡。";

                byte[] bytes = body.getBytes(CharsetUtil.UTF_8);
                int size = bytes.length;
                header.setLen(size);
                message.setMessageHeader(header);
                message.setBody(body);

                future.channel().writeAndFlush(message);
                Thread.sleep(3000);
            }
        } finally {
            workerGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new Client("localhost", 8082).run();
    }
}

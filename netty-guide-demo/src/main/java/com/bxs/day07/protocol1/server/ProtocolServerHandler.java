package com.bxs.day07.protocol1.server;

import com.bxs.day07.protocol1.common.ProtocolMessage;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 说明：处理器
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 18:09
 */
public class ProtocolServerHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object obj)
            throws Exception {
        Channel incoming = ctx.channel();

        if(obj instanceof ProtocolMessage) {
            ProtocolMessage message = (ProtocolMessage) obj;
            System.out.println("Client->Server:"+incoming.remoteAddress()+message.getBody());
            incoming.write(obj);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }
}

package com.bxs.netty.chain;

import com.bxs.netty.chain.handler.ProductManager;
import com.bxs.netty.chain.handler.QAEngineer;
import com.bxs.netty.chain.handler.RequestHandler;
import com.bxs.netty.chain.handler.SoftwareEngineer;
import com.bxs.netty.chain.quest.Request;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 14:25
 */
public class ITCompany {
    private RequestHandler chain;

    public ITCompany() {
        buildChain();
    }

    private void buildChain() {
        chain = new ProductManager(new SoftwareEngineer(new QAEngineer(null)));
    }

    public void makeRequest(Request req) {
        chain.handleRequest(req);
    }
}

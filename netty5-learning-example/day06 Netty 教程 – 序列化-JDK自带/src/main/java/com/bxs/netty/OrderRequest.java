package com.bxs.netty;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/24 17:38
 */
public class OrderRequest implements java.io.Serializable {
    private static final long serialVersionUID = 1826067782744144943L;

    private Integer orderId;
    private String userName;
    private String productName;
    private String phoneNumber;
    private String address;

    public Integer getOrderId() {
        return orderId;
    }

    public OrderRequest setOrderId(Integer orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public OrderRequest setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public OrderRequest setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public OrderRequest setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public OrderRequest setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public String toString() {
        return "OrderRequest{" +
                "orderId=" + orderId +
                ", userName='" + userName + '\'' +
                ", productName='" + productName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}

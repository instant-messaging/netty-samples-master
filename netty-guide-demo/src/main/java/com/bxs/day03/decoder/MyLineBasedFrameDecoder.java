package com.bxs.day03.decoder;

import io.netty.handler.codec.LineBasedFrameDecoder;

/**
 * My LineBasedFrameDecoder.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 10:15
 */
public class MyLineBasedFrameDecoder extends LineBasedFrameDecoder {

    /**
     * 帧的最大长度
     */
    private final static int MAX_LENGTH = 1024;

    public MyLineBasedFrameDecoder() {
        super(MAX_LENGTH);
    }
}

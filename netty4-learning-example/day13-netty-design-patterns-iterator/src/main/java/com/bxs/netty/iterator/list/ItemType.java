package com.bxs.netty.iterator.list;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:12
 */
public enum ItemType {

    ANY, WEAPON, RING, POTION

}

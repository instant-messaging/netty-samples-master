package com.bxs.protocol.custom.common.codec;

import com.bxs.protocol.custom.common.entity.Body;
import com.bxs.protocol.custom.common.entity.Message;
import com.bxs.protocol.custom.common.util.ByteUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 11:02
 */
public class MessageEncoder extends MessageToByteEncoder<Message> {
    private static final Logger logger = LoggerFactory.getLogger(MessageEncoder.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, Message msg, ByteBuf out) throws Exception {
        logger.info("编码 msg={}",msg.toString());
        Integer length = msg.getHeader().getLength();
        Integer code = msg.getHeader().getCode();
        Long sessionId = msg.getHeader().getSessionId();
        Byte type = msg.getHeader().getType();
        Byte priority = msg.getHeader().getPriority();
        out.writeInt(length);
        out.writeInt(code);
        out.writeLong(sessionId);
        out.writeByte(type);
        out.writeByte(priority);

        Map<String, Object> attachment = msg.getHeader().getAttachment();
        if (attachment != null && !attachment.isEmpty()) {
            // 用两个字节记录可扩展字段attachment的大小，short是16位，2个字节
            out.writeShort(attachment.size());
            if (attachment.size() > 0) {
                Set<Map.Entry<String, Object>> entries = attachment.entrySet();
                for (Map.Entry<String, Object> entry : entries) {
                    String key = entry.getKey();
                    // 用4个字节记录key长度
                    out.writeInt(key.length());
                    out.writeCharSequence(key, CharsetUtil.UTF_8);
                    Object obj = entry.getValue();
                    byte[] v = ByteUtil.toByteArray(obj);
                    int vlen = v.length;
                    out.writeInt(vlen);
                    out.writeBytes(v);
                }
            }
        }

        Body body = msg.getBody();
        if (body != null) {
            Object payload = msg.getBody().getPayload();
            if (payload != null) {
                byte[] load = ByteUtil.toByteArray(payload);
                out.writeBytes(load);
            }
        }

        logger.info("编码调用结束");
    }
}

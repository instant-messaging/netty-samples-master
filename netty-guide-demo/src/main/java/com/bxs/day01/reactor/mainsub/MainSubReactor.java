package com.bxs.day01.reactor.mainsub;

import java.io.IOException;

/**
 * Main Sub Reactor Demo
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 15:37
 */
public class MainSubReactor {

    public static void main(String[] args) throws IOException {
        new Thread(new Reactor(2333)).start();
    }
}

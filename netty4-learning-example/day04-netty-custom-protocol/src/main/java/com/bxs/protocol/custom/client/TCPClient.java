package com.bxs.protocol.custom.client;

import com.bxs.protocol.custom.client.handler.HeartBeatRequestHandler;
import com.bxs.protocol.custom.client.handler.LoginRequestHandler;
import com.bxs.protocol.custom.common.codec.MessageDecoder;
import com.bxs.protocol.custom.common.codec.MessageEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 客户端
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/25 12:31
 */
@Component
@Order(2)
public class TCPClient implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(TCPClient.class);

    @Value("${netty.host}")
    private String host;
    @Value("${netty.port}")
    private Integer port;

    @Override
    public void run(String... args) throws Exception {
        NioEventLoopGroup client = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(client)
                    .channel(NioSocketChannel.class)
                    .localAddress("127.0.0.1", 8800)
                    .handler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel channel) throws Exception {
                            ChannelPipeline pipeline = channel.pipeline();
                            pipeline.addLast(new MessageDecoder());
                            pipeline.addLast(new MessageEncoder());
                            pipeline.addLast(new HeartBeatRequestHandler());
                            pipeline.addLast(new LoginRequestHandler());
                        }
                    });

            ChannelFuture future = bootstrap.connect(host, port).sync();

            if (future.isSuccess()) {
                logger.info("客户端连接主机:{}，ip:{}成功！", host, port);
            }
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            client.shutdownGracefully();
        }
    }
}

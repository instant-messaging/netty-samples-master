package com.bxs.chat.domain.repository;

import com.bxs.chat.domain.entity.UserChatRelation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * 群组仓储层
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 12:37
 */
public interface UserChatRepository extends JpaRepository<UserChatRelation, Long> {

    /**
     * 通过GroupId 查询群
     *
     * @param groupId 群id
     * @return 返回用户群关系对象
     */
    Optional<List<UserChatRelation>> findAllByGroupId(Long groupId);
}

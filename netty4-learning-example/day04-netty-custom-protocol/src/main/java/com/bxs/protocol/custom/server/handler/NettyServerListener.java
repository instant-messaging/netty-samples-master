package com.bxs.protocol.custom.server.handler;

import com.bxs.protocol.custom.server.TCPService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 14:18
 */
public class NettyServerListener implements ServletContextListener {
    private static final Logger logger = LoggerFactory.getLogger(NettyServerListener.class);

    @Value("${netty.port}")
    private int port;

    private TCPService tcpService;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}

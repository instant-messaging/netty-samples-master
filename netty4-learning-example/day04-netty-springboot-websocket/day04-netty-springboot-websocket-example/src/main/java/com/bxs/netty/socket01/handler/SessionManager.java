package com.bxs.netty.socket01.handler;

import com.bxs.netty.utils.NettyAttrUtil;
import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 通道组池，管理所有websocket连接
 *
 * @Author: Mr.Lu
 * @Date: 2020/10/10 15:12
 */
public class SessionManager {

    /**
     * 定义一个channel组，管理所有的channel
     * GlobalEventExecutor.INSTANCE 是全局的事件执行器，是一个单例
     */
    private static ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /**
     * 存放用户与Chanel的对应信息，用于给指定用户发送消息
     */
    private static Map<String, Channel> userChannelMap = new ConcurrentHashMap<>();

    private SessionManager() {
    }

    public static ChannelGroup getChannelGroup() {
        return channelGroup;
    }

    /**
     * 获取用户channel map
     * @return
     */
    public static Map<String, Channel> getUserChannelMap(){
        return userChannelMap;
    }

    /**
     * 新增一个客户端通道
     *
     * @param channel
     */
    public static void addChannel(Channel channel) {
        getChannelGroup().add(channel);
    }

    /**
     * 添加用户id与channel的关联
     * @param userId
     * @param channel
     */
    public static void addChannel(String userId, Channel channel) {
        getUserChannelMap().put(userId, channel);
    }

    /**
     * 移除一个客户端连接通道
     *
     * @param channel
     */
    public static void removeChannel(Channel channel) {
        String userId = NettyAttrUtil.getUserId(channel);
        // 清除会话信息
        getChannelGroup().remove(channel);
        getUserChannelMap().remove(userId);
    }

    /**
     * 根据好友id获取对应的通道
     * @param userId 接收人编号
     * @return Netty通道
     */
    public static Channel getChannel(String userId) {
        return getUserChannelMap().get(userId);
    }

    /**
     * 打印所有的用户与通道的关联数据
     */
    public static void print() {
        for (String userId : userChannelMap.keySet()) {
            System.out.println("用户id:" + userId + " 通道:" + userChannelMap.get(userId).id());
        }
    }

    public static void clear(){
        channelGroup.clear();
        userChannelMap.clear();
    }
}

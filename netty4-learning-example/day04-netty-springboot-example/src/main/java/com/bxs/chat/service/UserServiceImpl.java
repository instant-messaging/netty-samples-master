package com.bxs.chat.service;

import com.bxs.chat.common.constant.Constant;
import com.bxs.chat.domain.vo.UserInfo;
import com.bxs.chat.domain.entity.Group;
import com.bxs.chat.domain.entity.User;
import com.bxs.chat.common.exception.Result;
import com.bxs.chat.domain.repository.GroupsRepository;
import com.bxs.chat.domain.repository.UserRepository;
import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/2 14:42
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupsRepository groupsRepository;

    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     * @return 登录信息-token
     */
    @Override
    public Result login(String username, String password, HttpServletRequest request) {
        Preconditions.checkNotNull(username,"用户名为空");
        Preconditions.checkArgument(password.length() > 0, "密码长度必须大于0");
        User user = userRepository.findByPasswordAndUsername(password, username);
        if (Objects.isNull(user)) {
            return Result.failure().setMessage("请登录");
        }
        // 通过HttpSession 存入用户id
        request.getSession().setAttribute(Constant.USER_TOKEN, user.getId());
        return Result.success(HttpStatus.OK.value()).setMessage("登录成功");
    }

    /**
     * 通过用户id获取用户信息
     * @param id 用户id
     * @return User 实例
     */
    @Override
    public User getById(Long id) {
        Preconditions.checkNotNull(id,"用户id为空");
        return userRepository.findById(id).get();
    }

    /**
     * 通过用户id查找用户相关信息 包括群消息 好友列表
     * @param userId 用户id
     * @return 用户基本信息和群信息
     */
    @Override
    public Result getUserInfoById(Long userId) {
        Preconditions.checkNotNull(userId,"用Id为空");
        User user = userRepository.findById(userId).get();
        List<Group> groups = groupsRepository.findAllByUserId(userId);
        List<User> friends = userRepository.findAllByUserId(userId);
        UserInfo userInfo = UserInfo.builder()
                .avatarUrl(user.getAvatarUrl())
                .friendList(friends)
                .groupList(groups)
                .userId(userId)
                .username(user.getUsername())
                .build();
        return Result.success(userInfo);
    }
}

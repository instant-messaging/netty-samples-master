package com.bxs.netty.singleton.type;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 15:04
 */
public final class MouseDriver {

    private MouseDriver () {
    }

    private static final MouseDriver INSTANCE = new MouseDriver();


    public static MouseDriver getInstance() {
        return INSTANCE;
    }
}

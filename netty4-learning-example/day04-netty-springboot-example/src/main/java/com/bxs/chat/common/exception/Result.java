package com.bxs.chat.common.exception;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/16 14:46
 */
@Data
@Accessors(chain = true)
public class Result implements Serializable {

    private Integer code;

    private String message;

    private Object data;

    public Result() {
    }

    public Result(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 返回成功
     *
     * @return
     */
    public static Result success() {
        return success(null);
    }

    /**
     * 返回成功
     *
     * @param data
     * @return
     */
    public static Result success(Object data) {
        return new Result(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data);
    }

    /**
     * 返回失败
     */
    public static Result failure() {
        return failure(ResultCode.ERROR);
    }

    /**
     * 返回失败
     */
    public static Result failure(String message) {
        return failure(ResultCode.ERROR).setMessage(message);
    }

    /**
     * 返回失败
     *
     * @param resultCode
     * @return
     */
    public static Result failure(ResultCode resultCode) {
        return failure(resultCode.getCode(), resultCode.getMessage());
    }

    /**
     * 返回失败
     *
     * @param code
     * @param message
     * @return
     */
    public static Result failure(Integer code, String message) {
        return new Result(code, message, null);
    }
}


package com.bxs.netty.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Date: 2020/10/17 16:02
 */
public interface DiscardService {
    Logger LOGGER = LoggerFactory.getLogger(DiscardService.class);

    default void discard(String message){
        LOGGER.info("丢弃消息:{}", message);
    }
}

# Netty demos. （Netty 案例大全）

[Netty 4.x 用户指南](../docs/Netty%204.x%20用户指南.pdf)

[Netty 权威指南 第2版](../docs/Netty权威指南%20第2版.pdf)

涉及的相关技术及版本如下。

* Netty 4.1.51.Final
* Jackson 2.10.1
* JUnit 5.5.2

包含示例如下：
1. [BIO、NIO、AIO 基础](/src/main/java/com/bxs/day01)
    1. [Java 标准 I/O 实现 Echo 服务器、客户端](src/main/java/com/bxs/day01/bio)
    2. [Java 线程池示例](src/main/java/com/bxs/day01/pio)
    3. [Java NIO 实现 UDP 服务器、客户端](src/main/java/com/bxs/day01/nio/UDP)
    4. [Java NIO 实现 Echo 服务器、客户端](src/main/java/com/bxs/day01/nio)
    5. [Java AIO 实现 Echo 服务器、客户端](src/main/java/com/bxs/day01/aio)
    6. [Java Reactor 示例](src/main/java/com/bxs/day01/reactor)
2. [Netty 入门](/src/main/java/com/bxs/day02)
    1. [Java  ByteBuffer 使用案例](src/main/java/com/bxs/day01/nio/buffer)
    2. [Netty ByteBuf 使用案例](src/main/java/com/bxs/day02/buffer)
    3. [Netty ByteBuf 的三种使用模式](src/main/java/com/bxs/day02/buffer)
    4. [Netty 实现丢弃服务器](src/main/java/com/bxs/day02/discard)
    5. [Netty 实现无连接协议 Echo 服务器、客户端](src/main/java/com/bxs/day02/echo)
    6. [Netty 实现 Echo 服务器、客户端](src/main/java/com/bxs/day02/echo)
    7. [Netty 实现时间服务器](src/main/java/com/bxs/day02/time)

3. [自定义编码/解码](src/main/java/com/bxs/day03)
    1. [自定义基于换行的解码器](src/main/java/com/bxs/day03/decoder)
    2. [自定义编码器](src/main/java/com/bxs/day03/encoder)
    3. [自定义编解码器](src/main/java/com/bxs/day03/codec)

4. [Netty开箱即用的解码器](src/main/java/com/bxs/day04)
    1. [对于二进制协议请看](src/main/java/com/bxs/day04/factorial)
    2. [对于基于文本协议请看](src/main/java/com/bxs/day04/telnet)

5. [消息序列化](src/main/java/com/bxs/day05)
    1. [基于 Netty 的对象序列化](src/main/java/com/bxs/day05/serialization)
    2. [基于 Jackson 的 JSON 序列化](src/main/java/com/bxs/day05/jackcon)
    3. [用POJO代替ByteBuf](src/main/java/com/bxs/day05/pojo)

6. [进阶学习](src/main/java/com/bxs/day06)
    1. [实现心跳机制](src/main/java/com/bxs/day06/heartbeat)
    2. [基于 SSL/TSL 的双向认证 Echo 服务器和客户端](src/main/java/com/bxs/day06/secureecho)

7. [自定义协议](src/main/java/com/bxs/day07)
    1. [自定义协议](src/main/java/com/bxs/day07/protocol1)
    2. [自定义协议](src/main/java/com/bxs/day07/protocol2)

8. [Netty开箱即用的协议](src/main/java/com/bxs/day08)
    1. [文件服务器](src/main/java/com/bxs/day08/file)
    2. [基于 HTTP 的 Web 服务器](src/main/java/com/bxs/day08/http)
    3. [基于 HTTP/2 的 Web 服务器和客户端](src/main/java/com/bxs/day08/http2)
    4. [基于 WebSocket 的聊天室](src/main/java/com/bxs/day08/websocket)

3. [TCP客户端](src/main/java/com/bxs/util/TCPClient.java)
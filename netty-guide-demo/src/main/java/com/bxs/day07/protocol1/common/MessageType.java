package com.bxs.day07.protocol1.common;

/**
 * 说明：消息类型
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 18:02
 */
public enum MessageType {
    EMGW_LOGIN_REQ((byte) 0x00),
    EMGW_LOGIN_RES((byte) 0x01);

    private byte value;

    public byte getValue() {
        return value;
    }

    private MessageType(byte value) {
        this.value = value;
    }
}

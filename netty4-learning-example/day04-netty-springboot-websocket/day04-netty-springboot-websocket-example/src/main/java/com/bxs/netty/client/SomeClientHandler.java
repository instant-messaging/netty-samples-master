package com.bxs.netty.client;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.concurrent.GenericFutureListener;
import io.netty.util.concurrent.ScheduledFuture;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Date: 2020/10/22 9:22
 */
public class SomeClientHandler extends ChannelInboundHandlerAdapter {
    private ScheduledFuture schedule;
    private GenericFutureListener listener;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // 发送心跳
        sendHeartbeat(ctx.channel());
    }


    private void sendHeartbeat(Channel channel) {
        // 生成一个[1，8)的随机数作为心跳发送的时间间隔
        int interval = new Random().nextInt(7) + 1;
        System.out.println(interval + "秒后会向Server发送心跳");

        //channel.eventLoop().schedule方法可以生成一个定时任务
        //该方法有三个参数：需要执行的任务，多长时间以后执行，时间单位
        schedule = channel.eventLoop().schedule(() -> {
            if (channel.isActive()) {
                System.out.println("向Server发送心跳");
                channel.writeAndFlush("~PING~");
            } else {
                System.out.println("与Server间的连接已经关闭");
            }
        }, interval, TimeUnit.SECONDS);
        //该异步定时任务只会执行一次，我们可以添加监听器监听异步任务操作，
        //一但异步任务完成马上触发监听器方法，通过监听器可以拿到异步任务的操作结果
        //我们可以让监听器一但监听到任务执行完毕，马上重新执行该任务，达到循环的效果

        //定义一个监听器，一旦操作完成会触发监听器执行
        listener = (future) -> {
            // 再次发送心跳
            // sendHeartBeat方法执行完了出栈了，才会
            // 触发监听的sendHeartBeat，不会栈溢出
            sendHeartbeat(channel);
        };

        // 为定时任务添加监听器，一旦该异步任务结束就会触发监听器逻辑
        schedule.addListener(listener);
    }
    //为了安全起见，重写一个channelInactive方法，当通道被关闭了，
    // 就把定时任务的监听器取消掉，就不会再递归调用了，以免通道关闭
    // 了,心跳还再一直发
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        // 一旦连接被关闭，则将监听器移除，这样就不会再发生心跳方法的递归调用了，以防止栈溢出
        schedule.removeListener(listener);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}

package com.bxs.day07.protocol1.client;

import com.bxs.day07.protocol1.common.ProtocolMessage;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 说明：处理器
 * ProtocolClient
 * @Author: Mr.Lu
 * @Date: 2020/9/5 18:11
 */
public class ProtocolClientHandler extends SimpleChannelInboundHandler<Object> {


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object obj)
            throws Exception {
        Channel incoming = ctx.channel();
        System.out.println("Server->Client:"+incoming.remoteAddress()+obj.toString());

        if(obj instanceof ProtocolMessage) {
            ProtocolMessage message = (ProtocolMessage)obj;
            System.out.println("Server->Client:"+incoming.remoteAddress() + message.getBody());
        }
    }
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        //ctx.flush();
    }

}

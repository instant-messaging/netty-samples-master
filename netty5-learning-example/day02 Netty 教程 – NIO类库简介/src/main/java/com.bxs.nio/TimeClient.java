package com.bxs.nio;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/24 8:49
 */
public class TimeClient {

    public static void main(String[] args) {
        int port = 4040;
        new Thread(new TimeClientHandler("127.0.0.1", port),"NIO-MultiplexerTimeServer-1").start();
    }
}

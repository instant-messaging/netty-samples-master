package com.bxs.netty;

/**
 * @Author: Mr.Lu
 * @Date: 2020/8/26 9:48
 */
public class ApplicationTests {

    private static int count = 0;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10000; i++) {
            new Thread(ApplicationTests::ince).start();
        }
        Thread.sleep(5000);
        System.out.println("运行结果：" + count);
    }

    private synchronized static void ince() {
        count ++;
    }
}

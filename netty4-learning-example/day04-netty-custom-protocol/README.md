### 自定义协议主要采用长度进行粘包处理

工程分三个部分

1. common：主要包含实体，枚举，一些公用的编解码常量等
2. service：服务端
3. client：客户端

##### 上面的心跳是自定义的，也可以用netty自带的心跳并进行自定义如以下，服务端采用
```java_holder_method_tree
.addLast(new IdleStateHandler(3,5,7, TimeUnit.SECONDS))
.addLast(new HeartBeatBuz())
```
```java
public class HeartBeatBiz extends ChannelInboundHandlerAdapter {
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if(evt instanceof IdleStateHandler){
            IdleStateEvent event = (IdleStateEvent) evt;
            String eventType = null;
            switch (event.state()){
                case READER_IDLE:
                    eventType="读空闲";
                    break;
                case WRITER_IDLE:
                    eventType="写空闲";
                    break;
                case ALL_IDLE:
                    eventType="读写空闲";
                    break;
            }
            System.out.println(ctx.channel().remoteAddress()+"---超时时间--"+eventType);
            System.out.println("处理相应业务");
        }
    }
}
```
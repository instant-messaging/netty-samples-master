package com.bxs.day01.aio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 异步非阻塞的IO 的 应答服务器
 *
 * Async Echo Server.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 13:45
 */
public class AsyncEchoServer {
    public static int DEFAULT_PORT = 7;

    public static void main(String[] args) {
        int port;

        try {
            port = Integer.parseInt(args[0]);
        } catch (RuntimeException ex) {
            port = DEFAULT_PORT;
        }

        AsynchronousServerSocketChannel serverSocketChannel;
        try {
            serverSocketChannel = AsynchronousServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(port));

            // 设置阐述
            serverSocketChannel.setOption(StandardSocketOptions.SO_RCVBUF, 4 * 1024);
            serverSocketChannel.setOption(StandardSocketOptions.SO_REUSEADDR, true);

            System.out.println("AsyncEchoServer已启动，端口：" + port);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        while (true) {

            // 可连接
            Future<AsynchronousSocketChannel> future = serverSocketChannel.accept();
            AsynchronousSocketChannel socketChannel = null;
            try {
                socketChannel = future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                System.out.println("AsyncEchoServer异常!" + e.getMessage());
            }

            System.out.println("AsyncEchoServer接受客户端的连接：" + socketChannel);

            // 分配缓存区
            ByteBuffer buffer = ByteBuffer.allocate(100);


            try {
                while (socketChannel.read(buffer).get() != -1) {
                    buffer.flip();
                    socketChannel.write(buffer).get();

                    System.out.println("AsyncEchoServer  -> " + socketChannel.getRemoteAddress() + "：" + buffer.toString());

                    if (buffer.hasRemaining()) {
                        buffer.compact();
                    } else {
                        buffer.clear();
                    }
                }
            } catch (InterruptedException | ExecutionException | IOException e) {
                e.printStackTrace();
                System.out.println("AsyncEchoServer异常!" + e.getMessage());
            }

        }
    }
}

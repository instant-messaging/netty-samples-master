package com.bxs.chat.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 群相关信息  说明一下 之前使用了group作为表名称 直接执行代码 一直提示sql有问题，后来才发现是使用了关键词group 建表时慎用
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/2 11:31
 */
@Entity
@Table(name = "groups")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Group {

    /**
     * 主键自增id 群组id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    /**
     * 群名称
     */
    @Setter
    @Getter
    @Column(name = "name",columnDefinition = "varchar(64) DEFAULT NULL COMMENT '群名称'")
    private String name;

    /**
     * 群公告
     */
    @Setter
    @Getter
    @Column(name = "note",columnDefinition = "varchar(64) DEFAULT NULL COMMENT '群公告'")
    private String note;

    /**
     * 群头像
     */
    @Setter
    @Getter
    @Column(name = "avatar_url",columnDefinition = "varchar(64) DEFAULT NULL COMMENT '群头像'")
    private String avatarUrl;

    /**
     * 群创建人
     */
    @Setter
    @Getter
    @Column(name = "create_user",columnDefinition = "varchar(64) DEFAULT NULL COMMENT '群创建人'")
    private String createUser;

    /**
     * 群创建时间
     */
    @Setter
    @Getter
    @Column(name = "create_date", columnDefinition = "datetime DEFAULT NULL COMMENT '创建时间'")
    private LocalDateTime createDate;

    /**
     * 修改时间
     */
    @Setter
    @Getter
    @Column(name = "last_modify_date", columnDefinition = "datetime DEFAULT NULL COMMENT '修改时间'")
    private LocalDateTime lastModifyDate;


    /**
     * 状态
     */
    @Setter
    @Getter
    @Column(name = "state", columnDefinition = "int(4) DEFAULT NULL COMMENT '删除标志位'")
    private Integer state;
}

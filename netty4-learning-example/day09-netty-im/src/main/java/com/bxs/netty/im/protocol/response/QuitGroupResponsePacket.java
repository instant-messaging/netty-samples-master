package com.bxs.netty.im.protocol.response;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;

import static com.bxs.netty.im.protocol.command.Command.QUIT_GROUP_RESPONSE;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 9:03
 */
@Data
public class QuitGroupResponsePacket extends Packet {

    private String groupId;

    private boolean success;

    private String reason;

    @Override
    public Byte getCommand() {
        return QUIT_GROUP_RESPONSE;
    }
}

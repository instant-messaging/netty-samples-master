package com.bxs.netty.kafka.kafka;

import com.bxs.netty.kafka.proto.Callback;
import com.google.protobuf.InvalidProtocolBufferException;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Deserializer;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 15:12
 */
@Slf4j
public class CallbackDeserializer extends Adapter implements Deserializer<Callback> {

    @Override
    public Callback deserialize(final String topic, byte[] data) {
        try {
            return Callback.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            log.info("Received un-parse message exception and skip.");
            return null;
        }
    }
}

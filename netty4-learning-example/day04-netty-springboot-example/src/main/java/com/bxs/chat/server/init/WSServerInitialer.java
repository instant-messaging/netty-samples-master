package com.bxs.chat.server.init;

import com.bxs.chat.server.codec.ProtocolDecoder;
import com.bxs.chat.server.handler.ChatHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;

/**
 * 绑定客户端连接时候触发操作
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/3 17:14
 */
@Slf4j
public class WSServerInitialer extends ChannelInitializer<SocketChannel> {

    @Resource
    private ChatHandler chatHandler;

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        log.info("收到新的客户端连接: {}", ch.toString());

        ChannelPipeline pipeline = ch.pipeline();

        //15 秒客户端没有向服务器发送心跳则关闭连接
        pipeline.addLast(new IdleStateHandler(15, 0, 0));

        // 协议解码处理器，判断是什么协议（WebSocket还是TcpSocket）,然后动态修改编解码器
        pipeline.addLast(new ProtocolDecoder());

        // 自定义的handler
        pipeline.addLast(chatHandler);
    }
}

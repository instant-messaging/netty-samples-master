package com.bxs.netty.socket00.handler;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;

/**
 * Heartbeat
 * HeartbeatHandler 心跳处理
 *
 * @Author: Mr.Lu
 * @Date: 2020/10/10 14:14
 */
public class HeartBeatServerHandler extends ChannelInboundHandlerAdapter {

    private static final ByteBuf HEARTBEAT_SEQUENCE = Unpooled.unreleasableBuffer(Unpooled.copiedBuffer("HEARTBEAT", CharsetUtil.UTF_8));

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
            switch (idleStateEvent.state()) {
                case READER_IDLE:
                    System.out.println("读空闲事件触发...");
                    break;
                case WRITER_IDLE:
                    System.out.println("写空闲事件触发...");
                    break;
                case ALL_IDLE:
                    System.out.println("---------------");
                    System.out.println("读写空闲事件触发");
                    System.out.println("关闭通道资源");
                    // 关闭连接
                    //ctx.channel().close();
                    //ctx.writeAndFlush(HEARTBEAT_SEQUENCE.duplicate()).addListener(ChannelFutureListener.CLOSE_ON_FAILURE);
                    break;
            }
        } else {
            // 传递给下一个处理程序
            super.userEventTriggered(ctx, evt);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}

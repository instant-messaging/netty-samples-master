package com.bxs.chat.common.util.codec;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5 加密编码工具类
 *
 * @Author: Mr.Lu
 * @Date: 2020/8/28 12:53
 */
public final class MD5Util {

    private MD5Util() {
    }

    /**
     * 将输入的字符串进行MD5加密（编码）
     *
     * @param inputString
     * @return
     */
    public static String createMD5String(String inputString) {
        return encodeByMD5(inputString);
    }

    /**
     * 验证MD5密码是否正确
     *
     * @param md5
     * @param inputString
     * @return
     */
    public static boolean authMD5String(String md5, String inputString) {
        if (md5.equals(encodeByMD5(inputString))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 对字符串进行MD5编码
     *
     * @param originStr
     * @return
     */
    public static String encodeByMD5(String originStr) {
        if (originStr != null) {
            try {
                // 创建具有指定算法名称的信息摘要
                MessageDigest md = MessageDigest.getInstance("MD5");
                // 使用指定的字节数组对摘要进行最后的更新，然后完成摘要计算
                char[] _charStr = originStr.toCharArray();
                byte[] _byteStr = new byte[_charStr.length];
                for (int i = 0; i < _charStr.length; i++) {
                    _byteStr[i] = (byte)_charStr[i];
                }
                byte[] _results = md.digest(_byteStr);
                StringBuilder _hexValue = new StringBuilder();
                for (byte _result : _results) {
                    int _val = _result & 0xff;
                    if (_val < 16) {
                        _hexValue.append('0');
                    }
                    _hexValue.append(Integer.toHexString(_val));
                }
                return _hexValue.toString();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    //=================================================================================================

    /**
     * 16进制字符
     */
    static String[] chars = {"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"};

    /**
     * 将普通字符串用md5加密，并转化为16进制字符串
     * @param str
     * @return
     */
    public static String StringInMd5(String str) {

        // 消息签名（摘要）
        MessageDigest md5 = null;
        try {
            // 参数代表的是算法名称
            md5 = MessageDigest.getInstance("md5");
            byte[] result = md5.digest(str.getBytes());

            StringBuilder sb = new StringBuilder(32);
            // 将结果转为16进制字符  0~9 A~F
            for (int i = 0; i < result.length; i++) {
                // 一个字节对应两个字符
                byte x = result[i];
                // 取得高位
                int h = 0x0f & (x >>> 4);
                // 取得低位
                int l = 0x0f & x;
                sb.append(chars[h]).append(chars[l]);
            }
            return sb.toString();

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}

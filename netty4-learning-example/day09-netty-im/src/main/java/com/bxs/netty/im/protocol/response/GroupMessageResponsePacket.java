package com.bxs.netty.im.protocol.response;

import com.bxs.netty.im.protocol.Packet;
import com.bxs.netty.im.session.Session;
import lombok.Data;

import static com.bxs.netty.im.protocol.command.Command.GROUP_MESSAGE_RESPONSE;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:58
 */
@Data
public class GroupMessageResponsePacket extends Packet {

    private String fromGroupId;

    private Session fromUser;

    private String message;

    @Override
    public Byte getCommand() {
        return GROUP_MESSAGE_RESPONSE;
    }
}

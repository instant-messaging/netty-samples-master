package com.bxs.protocol.custom.server.handler;

import com.bxs.protocol.custom.common.entity.Message;
import com.bxs.protocol.custom.common.enums.MessageType;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 11:45
 */
public class MessageHandler extends ChannelInboundHandlerAdapter {
    private static final Logger log = LoggerFactory.getLogger(MessageHandler.class);

    private String writeList = "192.168.25.92:8800,/127.0.0.1:8800";

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Message message = (Message) msg;
        log.debug(msg.toString());

        if (message != null && message.getHeader().getType() == MessageType.HANDSHAKE_REQUEST.getType().byteValue()) {
            Channel channel = ctx.channel();
            String remoteIp = channel.remoteAddress().toString();
            InetSocketAddress remoteAddress = (InetSocketAddress) channel.remoteAddress();
            log.debug(remoteAddress.toString());
            log.debug(remoteIp + "\t" + writeList.contains(remoteIp));
            if (writeList.contains(remoteIp)) {
                message.getHeader().setType(MessageType.HANDSHAKE_RESPONSE.getType().byteValue());
                log.debug("message 发送出去~~~~~~{}", message.toString());
                ctx.writeAndFlush(message);
            }
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println(cause);
        Channel channel = ctx.channel();
        if (channel.isActive()) {
            ctx.close();
        }
    }
}

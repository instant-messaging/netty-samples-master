package com.bxs.netty.im.protocol.request;

import com.bxs.netty.im.protocol.Packet;
import lombok.Data;

import java.util.List;

import static com.bxs.netty.im.protocol.command.Command.CREATE_GROUP_REQUEST;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 8:46
 */
@Data
public class CreateGroupRequestPacket extends Packet {

    private List<String> userIdList;

    @Override
    public Byte getCommand() {
        return CREATE_GROUP_REQUEST;
    }
}

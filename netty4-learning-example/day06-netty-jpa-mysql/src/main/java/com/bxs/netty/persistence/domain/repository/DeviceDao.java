package com.bxs.netty.persistence.domain.repository;

import com.bxs.netty.persistence.base.repository.Dao;
import com.bxs.netty.persistence.domain.vo.Device;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/7 17:12
 */
public interface DeviceDao extends Dao<Device> {
    /**
     * Save or update device object
     *
     * @param device the device object
     * @return saved device object
     */
    Device save(Device device);
}

package com.bxs.day05.serialization;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * SerializationClient Handler.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 11:11
 */
public class SerializationClientHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object obj) throws Exception {
        if (obj instanceof SerializationBean) {
            SerializationBean user = (SerializationBean) obj;
            System.out.println("Server -> Client: " + user);
        }
    }
}

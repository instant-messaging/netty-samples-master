package com.bxs.netty.chain.handler;

import com.bxs.netty.chain.quest.Request;
import com.bxs.netty.chain.quest.RequestType;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/8 14:29
 */
public class QAEngineer extends RequestHandler {

    public QAEngineer(RequestHandler handler) {
        super(handler);
    }

    @Override
    public void handleRequest(Request req) {
        if (RequestType.QA_ENGINNER == req.getRequestType()) {
            printHandling(req);
            req.markHandled();
        } else {
            super.handleRequest(req);
        }
    }


    @Override
    public String toString() {
        return "测试工程师";
    }
}

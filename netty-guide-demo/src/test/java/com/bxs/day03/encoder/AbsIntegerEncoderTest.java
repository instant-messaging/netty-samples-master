package com.bxs.day03.encoder;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * AbsIntegerEncoder Test.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 10:58
 */
public class AbsIntegerEncoderTest {
    @Test
    void testEncoded() {
        ByteBuf buf = Unpooled.buffer();
        for (int i = 1; i < 10; i++) {
            buf.writeInt(i * -1); //（1）
        }

        EmbeddedChannel channel = new EmbeddedChannel(new AbsIntegerEncoder()); //（2）
        assertTrue(channel.writeOutbound(buf)); //（3）
        assertTrue(channel.finish()); //（4）

        // 读字节
        for (int i = 1; i < 10; i++) {
            assertEquals(Integer.valueOf(i+""), channel.readOutbound()); //（5）
        }
        assertNull(channel.readOutbound());
    }
}

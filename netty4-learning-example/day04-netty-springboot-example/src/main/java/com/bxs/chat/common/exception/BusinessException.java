package com.bxs.chat.common.exception;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/18 10:26
 */
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    /**
     * 自定义错误码
     */
    private Integer code;

    private BusinessException() {
    }

    public BusinessException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public BusinessException setCode(Integer code) {
        this.code = code;
        return this;
    }
}

package com.bxs.day01.reactor.basic;

import java.io.IOException;

/**
 * Basic Reactor Demo
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 14:55
 */
public class BasicReactor {

    public static void main(String[] args) throws IOException {
        new Thread(new Reactor(2333)).start();
    }
}

package com.bxs.day05.jackcon;

import com.bxs.day05.jackcon.codec.JacksonMapper;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * JacksonClient Handler.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 11:33
 */
public class JacksonClientHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object obj) throws Exception {
        String jsonString = "";
        if (obj instanceof JacksonBean) {
            JacksonBean user = (JacksonBean) obj;
            // 对象转为json字符串
            jsonString = JacksonMapper.getInstance().writeValueAsString(user);
            System.out.println("Server -> Client: " + jsonString);
        }
    }

}
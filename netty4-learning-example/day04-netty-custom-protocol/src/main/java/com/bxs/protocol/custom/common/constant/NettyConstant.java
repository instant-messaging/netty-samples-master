package com.bxs.protocol.custom.common.constant;

/**
 * 一些常量
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/25 10:53
 */
public class NettyConstant {
    public static final Long LOGIN_SESSION_ID = 0L;
    public static final Integer CODE= 0xABEF;
    public static final Byte PRIORITY = 1;
    public static final Integer HEADER_LENGTH =18;
}

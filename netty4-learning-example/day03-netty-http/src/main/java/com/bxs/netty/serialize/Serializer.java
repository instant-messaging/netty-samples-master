package com.bxs.netty.serialize;

/**
 * 序列化接口类
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/7 11:17
 */
public interface Serializer {
    /**
     * java 对象转换成二进制
     */
    byte[] serialize(Object object);

    /**
     * 二进制转换成 java 对象
     */
    <T> T deserialize(Class<T> clazz, byte[] bytes);
}

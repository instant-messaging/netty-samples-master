package com.bxs.day01.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * 同步非组赛IO 的 应答服务器
 *
 * Non Bloking Echo Server.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/4 14:02
 */
public class NonBlokingEchoServer {
    public static int DEFAULT_PORT = 7;

    public static void main(String[] args) throws IOException {
        int port;

        try {
            port = Integer.parseInt(args[0]);
        } catch (RuntimeException ex) {
            port = DEFAULT_PORT;
        }

        new NonBlokingEchoServer().run(port);
    }

    private void run(int port) throws IOException {
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        // 1.绑定服务器到制定端口
        serverChannel.socket().bind(new InetSocketAddress(port));
        // 2.打开 selector 处理 channel
        Selector selector = Selector.open();
        // 3.注册 ServerSocket 到 ServerSocket ，并指定这是专门意接受 连接。
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);

        System.out.println("NonBlokingEchoServer已启动，端口：" + port);

        final ByteBuffer message = ByteBuffer.wrap("Hi!\r\n".getBytes());

        for (; ; ) {
            try {
                // 4.等待新的事件来处理。这将阻塞，直到一个事件是传入。
                selector.select();
            } catch (IOException e) {
                System.out.println("NonBlockingEchoServer异常!" + e.getMessage());
                e.printStackTrace();
                break;
            }
            // 5.从收到的所有事件中 获取 SelectionKey 实例。
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = keys.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();
                try {

                    // 可连接
                    // 6.检查该事件是一个新的连接准备好接受。
                    if (key.isAcceptable()) {
                        ServerSocketChannel server = (ServerSocketChannel) key.channel();
                        SocketChannel socketChannel = server.accept();

                        System.out.println("Accepted connection from " + socketChannel);

                        // 设置非阻塞
                        socketChannel.configureBlocking(false);

                        // 7.接受客户端，并用 selector 进行注册。
                        SelectionKey clientKey = socketChannel.register(selector, SelectionKey.OP_WRITE | SelectionKey.OP_READ, message.duplicate());

                        // 分配缓存区
                        ByteBuffer buffer = ByteBuffer.allocate(100);
                        clientKey.attach(buffer);
                    }

                    // 可读
                    // 8.检查 socket 是否准备好读数据。
                    if (key.isReadable()) {
                        SocketChannel client = (SocketChannel) key.channel();
                        ByteBuffer output = (ByteBuffer) key.attachment();
                        int read = client.read(output);

                        System.out.println(client.getRemoteAddress() + " -> NonBlokingEchoServer：" + output.toString());
                        if (read > 0) {
                            // 反转缓冲区
                            output.flip();
                            byte[] bytes = new byte[output.remaining()];
                            output.get(bytes);
                            String msg = new String(bytes, "UTF-8");
                            System.out.println(client.getRemoteAddress() + " 的消息 :" + msg);
                        }

                        key.interestOps(SelectionKey.OP_WRITE);
                    }

                    // 可写
                    // 9.检查 socket 是否准备好写数据。
                    if (key.isWritable()) {
                        SocketChannel client = (SocketChannel) key.channel();
                        ByteBuffer output = (ByteBuffer) key.attachment();
                        output.flip();
                        while (output.hasRemaining()) {
                            //9.将数据写入到所连接的客户端。如果网络饱和，连接是可写的，那么这个循环将写入数据，直到该缓冲区是空的。
                            if (client.write(output) == 0) {
                                break;
                            }
                        }

                        System.out.println("NonBlokingEchoServer  -> " + client.getRemoteAddress() + "：" + output.toString());

                        output.compact();

                        key.interestOps(SelectionKey.OP_READ);

                        //10.关闭连接。
                        client.close();
                    }
                } catch (IOException ex) {
                    key.cancel();
                    try {
                        key.channel().close();
                    } catch (IOException cex) {
                        // 在关闭时忽略
                        System.out.println("NonBlockingEchoServer异常!" + cex.getMessage());
                    }
                }
            }
        }
    }
}
package com.bxs.netty.springboot.client;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 17:14
 */
public class NettyClientHandlerInitilizer extends ChannelInitializer<Channel> {
    @Override
    protected void initChannel(Channel ch) throws Exception {

    }
}

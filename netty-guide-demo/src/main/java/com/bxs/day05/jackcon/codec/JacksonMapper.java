package com.bxs.day05.jackcon.codec;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * ObjectMapper instance.
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 11:22
 */
public class JacksonMapper {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static ObjectMapper getInstance() {
        return MAPPER;
    }
}

package com.bxs.chat.domain.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.time.LocalDateTime;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/16 16:12
 */
public class Message {

    /**
     * 发送者name
     */
    public String from;
    /**
     * 接收者name
     */
    public String to;
    /**
     * 接收者name
     */
    private String type;
    /**
     * 发送的文本
     */
    public String text;
    /**
     * 发送时间
     */
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    public LocalDateTime date;

    public String getFrom() {
        return from;
    }

    public Message setFrom(String from) {
        this.from = from;
        return this;
    }

    public String getTo() {
        return to;
    }

    public Message setTo(String to) {
        this.to = to;
        return this;
    }

    public String getType() {
        return type;
    }

    public Message setType(String type) {
        this.type = type;
        return this;
    }

    public String getText() {
        return text;
    }

    public Message setText(String text) {
        this.text = text;
        return this;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Message setDate(LocalDateTime date) {
        this.date = date;
        return this;
    }
}

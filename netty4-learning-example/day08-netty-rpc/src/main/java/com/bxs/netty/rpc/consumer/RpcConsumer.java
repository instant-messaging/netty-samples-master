package com.bxs.netty.rpc.consumer;

import com.bxs.netty.rpc.api.IRpcHelloService;
import com.bxs.netty.rpc.api.IRpcService;
import com.bxs.netty.rpc.consumer.proxy.RpcProxy;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/9 11:58
 */
public class RpcConsumer {

    public static void main(String[] args) {
        IRpcHelloService rpcHello = RpcProxy.create(IRpcHelloService.class);

        System.out.println(rpcHello.hello("Tom老师"));

        IRpcService service = RpcProxy.create(IRpcService.class);

        System.out.println("8 + 2 = " + service.add(8, 2));
        System.out.println("8 - 2 = " + service.sub(8, 2));
        System.out.println("8 * 2 = " + service.mult(8, 2));
        System.out.println("8 / 2 = " + service.div(8, 2));
    }
}

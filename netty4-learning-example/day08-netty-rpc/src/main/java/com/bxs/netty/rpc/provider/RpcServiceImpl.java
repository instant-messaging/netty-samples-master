package com.bxs.netty.rpc.provider;

import com.bxs.netty.rpc.api.IRpcService;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/9 11:56
 */
public class RpcServiceImpl implements IRpcService {

    @Override
    public int add(int a, int b) {
        return 0;
    }

    @Override
    public int sub(int a, int b) {
        return 0;
    }

    @Override
    public int mult(int a, int b) {
        return 0;
    }

    @Override
    public int div(int a, int b) {
        return 0;
    }
}

package com.bxs.day03.decoder;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;

/**
 * My LineBasedFrameDecoder ChannelInitializer。
 *
 * @Author: Mr.Lu
 * @Date: 2020/9/5 10:14
 */
public class MyLineBasedFrameDecoderChannelInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();
        // 基于换行符号
        pipeline.addLast(new MyLineBasedFrameDecoder());

        // 解码转String
        pipeline.addLast(new StringDecoder());

        // 自定义ChannelHandler
        pipeline.addLast(new MyLineBasedFrameDecoderServerHandler());
    }
}

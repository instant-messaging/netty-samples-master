package com.bxs.netty.springboot.websocket.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/25 17:56
 */
@Service
public class DiscardService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiscardService.class);

    public void discard (String message) {
        LOGGER.info("丢弃消息:{}", message);
    }
}
